package rpg.resource;

import java.lang.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.io.*;
import javax.imageio.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import org.w3c.dom.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import rpg.resource.*;
import rpg.draw.*;
import rpg.character.*;
import rpg.map.*;
import rpg.control.*;
import rpg.item.*;
/**
 * A class that implements an Episode editor
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class EpisodeEditor extends JPanel implements ActionListener, ListSelectionListener{

	private static final long serialVersionUID = 1L;
	
	private DocumentBuilderFactory dbf;
	private DocumentBuilder db;
	private Document inputDoc;
	private Document outputDoc;
	
	private int width;
	private int height;
	
	private ArrayList<Episode> episodeList = ResourceHandler.getEpisodes();
	private ArrayList<NPC> npcList = ResourceHandler.getNPCs();
	private ArrayList<Item> itemList = ResourceHandler.getItems();
	private ArrayList<RoleInit> roleList = ResourceHandler.getRoles();
	
	private DefaultListModel<BufferedEpisode> curEpisodeListModel = new DefaultListModel<BufferedEpisode>();
	private JList<BufferedEpisode> curEpisodeList = new JList<BufferedEpisode>(curEpisodeListModel);
	private DefaultListModel<Episode> curSubEpisodeListModel = new DefaultListModel<Episode>();
	private JList<Episode> curSubEpisodeList = new JList<Episode>(curSubEpisodeListModel);
	private DefaultListModel<Item> needItemListModel = new DefaultListModel<Item>();
	private JList<Item> needItemList = new JList<Item>(needItemListModel);
	private DefaultListModel<Item> getItemListModel = new DefaultListModel<Item>();
	private JList<Item> getItemList = new JList<Item>(getItemListModel);
	private DefaultListModel<Item> loseItemListModel = new DefaultListModel<Item>();
	private JList<Item> loseItemList = new JList<Item>(loseItemListModel);
	private DefaultListModel<RoleInit> getRoleListModel = new DefaultListModel<RoleInit>();
	private JList<RoleInit> getRoleList = new JList<RoleInit>(getRoleListModel);
	private DefaultListModel<RoleInit> loseRoleListModel = new DefaultListModel<RoleInit>();
	private JList<RoleInit> loseRoleList = new JList<RoleInit>(loseRoleListModel);
	
	private JComboBox<Object> needItemBox = new JComboBox<Object>(itemList.toArray());
	private JButton needItemInButton = new JButton("Insert");
	private JButton needItemOutButton = new JButton("Delete");
	private JTextField needItemNum = new JTextField();
	private JComboBox<Object> getItemBox = new JComboBox<Object>(itemList.toArray());
	private JButton getItemInButton = new JButton("Insert");
	private JButton getItemOutButton = new JButton("Delete");
	private JTextField getItemNum = new JTextField();
	private JComboBox<Object> loseItemBox = new JComboBox<Object>(itemList.toArray());
	private JButton loseItemInButton = new JButton("Insert");
	private JButton loseItemOutButton = new JButton("Delete");
	private JTextField loseItemNum = new JTextField();
	private JComboBox<Object> getRoleBox = new JComboBox<Object>(roleList.toArray());
	private JButton getRoleInButton = new JButton("Insert");
	private JButton getRoleOutButton = new JButton("Delete");
	private JComboBox<Object> loseRoleBox = new JComboBox<Object>(roleList.toArray());
	private JButton loseRoleInButton = new JButton("Insert");
	private JButton loseRoleOutButton = new JButton("Delete");
	
	private JPanel infoPanel = new JPanel();
	private JPanel subInfoPanel = new JPanel();
	private JScrollPane episodeListPanel = new JScrollPane(curEpisodeList);
	private JScrollPane subEpisodeListPanel = new JScrollPane(curSubEpisodeList);
	private JPanel episodeControlPanel = new JPanel();
	private JPanel allListPanel = new JPanel();
	
	private String[] infoNameList = {"id", "name", "subNum"};
	private String[] subInfoNameList = {"type", "name", "targetName", "itemEvent", "roleEvent", "areaBound", "boundWords", "area"};
	
	private ArrayList<JLabel> infoLabelList = new ArrayList<JLabel>();
	private ArrayList<JTextField> infoInputList = new ArrayList<JTextField>();
	
	private ArrayList<JLabel> subInfoLabelList = new ArrayList<JLabel>();
	private ArrayList<JComponent> subInfoInputList = new ArrayList<JComponent>();
	
	private JButton saveEpisodeButton = new JButton("Save Episode");
	private JButton deleteEpisodeButton = new JButton("Delete Episode");
	private JButton insertButton = new JButton("Insert");
	private JButton deleteButton = new JButton("Delete");
	private JButton createButton = new JButton("Create episode");
	private JButton createSubButton = new JButton("Create Subepisode");
	
	private JButton saveAllButton = new JButton("Save All change");
		
	private ResourceEditor caller;
	
	public EpisodeEditor(ResourceEditor caller){
		this.caller = caller;
		initialLayout();
	}
	
	private void initialLayout(){
		width = caller.getWidth();
		height = caller.getHeight();
		this.setLayout(null);
		setAllPanel();
		initialInfoPanel();
		initialSubInfoPanel();
		initialEpisodeListPanel();
		initialSubEpisodeListPanel();
		initialEpisodeControlPanel();
		initialAllListPanel();
	}

	
	private void setAllPanel(){
		infoPanel.setBounds(75, 70, 200, 75);
		this.add(infoPanel);
		
		subInfoPanel.setBounds(width/4, 40, 200, 250);
		this.add(subInfoPanel);
		
		episodeListPanel.setBounds(width*4/10, height/2 - 50, width/5, height/3 + 50);
		this.add(episodeListPanel);
		
		subEpisodeListPanel.setBounds(70, height/2 - 50, width/5, height/3 + 50);
		this.add(subEpisodeListPanel);
		
		episodeControlPanel.setBounds(width*3/10 - 30, height*2/3 - 30, 140, 100);
		this.add(episodeControlPanel);
		
		allListPanel.setBounds(width*6/10 + 50, 50, width* 3/10 + 50, height * 8/10);
		this.add(allListPanel);
		
		saveAllButton.setBounds(40, 200, 150, 80);
		saveAllButton.addActionListener(this);
		this.add(saveAllButton);
	}
	
	private void initialInfoPanel(){
		infoPanel.setLayout(new GridLayout(infoNameList.length + 1, 2));
		
		for(int i=0; i<infoNameList.length; i++){
			JLabel label = new JLabel(infoNameList[i]);
			infoLabelList.add(label);
			infoPanel.add(label);
			
			JTextField textField = new JTextField();
			if(infoNameList[i].equals("subNum"))
				textField.setEditable(false);
			infoInputList.add(textField);
			infoPanel.add(textField);
		}
	}
	
	private void initialSubInfoPanel(){
		subInfoPanel.setLayout(new GridLayout(subInfoNameList.length + 1, 2));
		
		for(int i=0; i<subInfoNameList.length; i++){
			JLabel label = new JLabel(subInfoNameList[i]);
			subInfoLabelList.add(label);
			subInfoPanel.add(label);
			
			JComponent component;
			if(label.getText().equals("targetName")){
				NPC[] tmpArray = new NPC[npcList.size()];
				for(int j=0; j<npcList.size(); j++)
					tmpArray[j] = npcList.get(j);
				component = new JComboBox<NPC>(tmpArray);
			}
			else if(label.getText().equals("type")){
				Integer[] tmpArray = {1, 2, 3};
				component = new JComboBox<Integer>(tmpArray);
			}
			else if(label.getText().equals("itemEvent")
				||	label.getText().equals("roleEvent")
				||	label.getText().equals("areaBound"))
				component = new JCheckBox(label.getText());
			else
				component = new JTextField();
			subInfoInputList.add(component);
			subInfoPanel.add(component);
		}
	}
	
	private void initialEpisodeListPanel(){
		parseXml();
		refreshByEpisode(curEpisodeListModel.get(0));
		curEpisodeList.addListSelectionListener(this);
	}
	
	private void initialSubEpisodeListPanel(){
		curSubEpisodeList.addListSelectionListener(this);
	}
	
	private void initialEpisodeControlPanel(){
		episodeControlPanel.setLayout(new GridLayout(6, 1));
		episodeControlPanel.add(insertButton);
		episodeControlPanel.add(deleteButton);
		episodeControlPanel.add(createButton);
		episodeControlPanel.add(createSubButton);
		episodeControlPanel.add(saveEpisodeButton);
		episodeControlPanel.add(deleteEpisodeButton);
		insertButton.addActionListener(this);
		deleteButton.addActionListener(this);
		createButton.addActionListener(this);
		createSubButton.addActionListener(this);
		saveEpisodeButton.addActionListener(this);
		deleteEpisodeButton.addActionListener(this);
	}
	
	private void initialAllListPanel(){
		allListPanel.setLayout(new GridLayout(5, 1));
		
		JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayout(1, 2));
		JPanel panel1_small = new JPanel();
		panel1_small.setLayout(new GridLayout(4, 1));
		panel1_small.setBounds(0, 0, width/9, height*8/50);
		panel1.add(panel1_small);
		
		JScrollPane panel1_scroll = new JScrollPane(needItemList);
		panel1_scroll.setBounds(width/9, 0, width*2/9, height*8/50);
		panel1.add(panel1_scroll);
		
		panel1_small.add(needItemBox);
		panel1_small.add(needItemNum);
		panel1_small.add(needItemInButton);
		panel1_small.add(needItemOutButton);
		
		JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayout(1, 2));
		JPanel panel2_small = new JPanel();
		panel2_small.setLayout(new GridLayout(4, 1));
		panel2_small.setBounds(0, 0, width/9, height*8/50);
		panel2.add(panel2_small);
		
		JScrollPane panel2_scroll = new JScrollPane(getItemList);
		panel2_scroll.setBounds(width/9, 0, width*2/9, height*8/50);
		panel2.add(panel2_scroll);
		
		panel2_small.add(getItemBox);
		panel2_small.add(getItemNum);
		panel2_small.add(getItemInButton);
		panel2_small.add(getItemOutButton);
		
		JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayout(1, 2));
		JPanel panel3_small = new JPanel();
		panel3_small.setLayout(new GridLayout(4, 1));
		panel3_small.setBounds(0, 0, width/9, height*8/50);
		panel3.add(panel3_small);
		
		JScrollPane panel3_scroll = new JScrollPane(loseItemList);
		panel3_scroll.setBounds(width/9, 0, width*2/9, height*8/50);
		panel3.add(panel3_scroll);
		
		panel3_small.add(loseItemBox);
		panel3_small.add(loseItemNum);
		panel3_small.add(loseItemInButton);
		panel3_small.add(loseItemOutButton);
		
		JPanel panel4 = new JPanel();
		panel4.setLayout(new GridLayout(1, 2));
		JPanel panel4_small = new JPanel();
		panel4_small.setLayout(new GridLayout(3, 1));
		panel4_small.setBounds(0, 0, width/9, height*8/50);
		panel4.add(panel4_small);
		
		JScrollPane panel4_scroll = new JScrollPane(getRoleList);
		panel4_scroll.setBounds(width/9, 0, width*2/9, height*8/50);
		panel4.add(panel4_scroll);
		
		panel4_small.add(getRoleBox);
		panel4_small.add(getRoleInButton);
		panel4_small.add(getRoleOutButton);
		
		JPanel panel5 = new JPanel();
		panel5.setLayout(new GridLayout(1, 2));
		JPanel panel5_small = new JPanel();
		panel5_small.setLayout(new GridLayout(3, 1));
		panel5_small.setBounds(0, 0, width/9, height*8/50);
		panel5.add(panel5_small);
		
		JScrollPane panel5_scroll = new JScrollPane(loseRoleList);
		panel5_scroll.setBounds(width/9, 0, width*2/9, height*8/50);
		panel5.add(panel5_scroll);
		
		panel5_small.add(loseRoleBox);
		panel5_small.add(loseRoleInButton);
		panel5_small.add(loseRoleOutButton);
		
		allListPanel.add(panel1);
		allListPanel.add(panel2);
		allListPanel.add(panel3);
		allListPanel.add(panel4);
		allListPanel.add(panel5);
		
		needItemInButton.addActionListener(this);
		needItemOutButton.addActionListener(this);
		getItemInButton.addActionListener(this);
		getItemOutButton.addActionListener(this);
		loseItemInButton.addActionListener(this);
		loseItemOutButton.addActionListener(this);
		getRoleInButton.addActionListener(this);
		getRoleOutButton.addActionListener(this);
		loseRoleInButton.addActionListener(this);
		loseRoleOutButton.addActionListener(this);
		
	}
	
	private void setInfo(String target, String value){
		for(int i=0; i<infoLabelList.size(); i++)
			if(infoLabelList.get(i).getText().equals(target))
				infoInputList.get(i).setText(value);
	}
	
	private void setSubInfo(String target, String value){
		for(int i=0; i<subInfoLabelList.size(); i++)
			if(subInfoLabelList.get(i).getText().equals(target)){
				if(subInfoInputList.get(i) instanceof JTextField)
					((JTextField)subInfoInputList.get(i)).setText(value);
				else if(subInfoInputList.get(i) instanceof JComboBox){
					for(int j=0; j<((JComboBox)subInfoInputList.get(i)).getItemCount(); j++)
						if(((JComboBox)subInfoInputList.get(i)).getItemAt(j).toString().equals(value))
							((JComboBox)subInfoInputList.get(i)).setSelectedIndex(j);
				}
				else if(subInfoInputList.get(i) instanceof JCheckBox){
					if(value.equals("true"))
						((JCheckBox)subInfoInputList.get(i)).setSelected(true);
					else
						((JCheckBox)subInfoInputList.get(i)).setSelected(false);
				}
			}
	}
	
	private void refreshByEpisode(BufferedEpisode episode){
		curSubEpisodeListModel.clear();
		for(int i=0; i<episode.getSubList().size(); i++)
			curSubEpisodeListModel.addElement(episode.getSubList().get(i));
		
		setInfo("id", episode.getId() + "");
		setInfo("name", episode.getName());
		setInfo("subNum", episode.getSubNum() + "");
	}
	
	private void refreshBySub(Episode subEpisode){
		setSubInfo("type", subEpisode.getType() + "");
		setSubInfo("name", subEpisode.getName());
		setSubInfo("targetName", subEpisode.getTargetName());
		needItemListModel.clear();
		getItemListModel.clear();
		loseItemListModel.clear();
		if(subEpisode.getGetItemList() != null
		|| subEpisode.getLoseItemList() != null
		|| subEpisode.getNeedItemList() != null){
			setSubInfo("itemEvent", "true");
			if(subEpisode.getNeedItemList() != null)
				for(int i=0; i<subEpisode.getNeedItemList().size(); i++)
					needItemListModel.addElement(subEpisode.getNeedItemList().get(i));
			if(subEpisode.getGetItemList() != null)
				for(int i=0; i<subEpisode.getGetItemList().size(); i++)
					getItemListModel.addElement(subEpisode.getGetItemList().get(i));
			if(subEpisode.getLoseItemList() != null)
				for(int i=0; i<subEpisode.getLoseItemList().size(); i++)
					loseItemListModel.addElement(subEpisode.getLoseItemList().get(i));
		}
		else
			setSubInfo("itemEvent", "false");
		getRoleListModel.clear();
		loseRoleListModel.clear();
		if(subEpisode.getGetRoleList() != null
		|| subEpisode.getLoseRoleList() != null){
			setSubInfo("roleEvent", "true");
			// if(subEpisode.getGetRoleList() != null)
				// for(int i=0; i<subEpisode.getGetRoleList().size(); i++)
					// getRoleListModel.addElement(subEpisode.getGetRoleList().get(i));
			// if(subEpisode.getLoseRoleList() != null)
				// for(int i=0; i<subEpisode.getLoseRoleList().size(); i++)
					// loseRoleListModel.addElement(subEpisode.getLoseRoleList().get(i));
		}
		else
			setSubInfo("roleEvent", "false");
		if(subEpisode.getBoundMapList() != null)
			setSubInfo("areaBound", "true");
		else
			setSubInfo("areaBound", "false");
		setSubInfo("boundWords", subEpisode.getBoundWords());
		ArrayList<Integer> boundList = subEpisode.getBoundMapList();
		StringBuffer sb = new StringBuffer("");
		if(boundList != null){
			sb.append(boundList.get(0) + ",");
			for(int i=1; i<boundList.size(); i++)
				sb.append(boundList.get(i) + ",");
		}
		setSubInfo("area", sb.toString());
	}
	
	private void initialParser(){
		try{
			File file = new File("xml/episode.xml");
			dbf = DocumentBuilderFactory.newInstance();
			db = dbf.newDocumentBuilder();
			inputDoc = db.parse(file);
			inputDoc.getDocumentElement().normalize();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void parseXml(){
		initialParser();
		Element root = inputDoc.getDocumentElement();
		NodeList list = root.getElementsByTagName("episode");
		
		for(int i=0; i<list.getLength(); i++){
			Element element = (Element)list.item(i);
			BufferedEpisode episode = new BufferedEpisode();
			episode.setId(Integer.parseInt(element.getAttribute("id")));
			episode.setName(element.getElementsByTagName("name").item(0).getTextContent());
			episode.setSubNum(Integer.parseInt(element.getElementsByTagName("subNum").item(0).getTextContent()));
			
			LinkedList<Episode> tmpEpisodeList = new LinkedList<Episode>();
			for(int j=0; j<episodeList.size(); j++)
				if(episodeList.get(j).getId() == episode.getId())
					tmpEpisodeList.add(episodeList.get(j));
			episode.setSubList(tmpEpisodeList);
			curEpisodeListModel.addElement(episode);
		}
	}
	
	// private void createNewAnimation(){
		// try{
			// JFileChooser fileChooser = new JFileChooser();
			// int returnValue = fileChooser.showOpenDialog(this);
			// if(returnValue == JFileChooser.APPROVE_OPTION){
				// BufferedAnimation animation = makeAnimation();
				// animation.setFrameFile(getRelativePath(fileChooser.getSelectedFile(), new File(System.getProperty("user.dir"))));
				// animation.setFrameNum(0);
				// animation.setFrameList(new LinkedList<BufferedFrame>());
				
				// for(int i=0; i<animationListModel.getSize(); i++){
					// if(i == animationListModel.getSize()-1){
						// animationListModel.addElement(animation);
						// break;
					// }
					// if(animationListModel.get(i).getType().equals(animation.getType())
					// && !animationListModel.get(i+1).getType().equals(animation.getType())){
						// animationListModel.insertElementAt(animation, i+1);
						// break;
					// }
				// }
			// }
		// }catch(Exception ex){
			// ex.printStackTrace();
		// }
	// }
	
	// private BufferedAnimation makeAnimation(){
		// BufferedAnimation animation = new BufferedAnimation();
		// animation.setType(animationTypeList[animationTypeBox.getSelectedIndex()]);
		
		// for(int i=0; i<infoLabelList.size(); i++){
			// if(infoLabelList.get(i).getText().equals("name"))
				// animation.setName(infoInputList.get(i).getText());
			// else if(infoLabelList.get(i).getText().equals("frameFile"))
				// animation.setFrameFile(infoInputList.get(i).getText());
			// else if(infoLabelList.get(i).getText().equals("frameNum"))
				// animation.setFrameNum(Integer.parseInt(infoInputList.get(i).getText()));
			// else if(infoLabelList.get(i).getText().equals("rowSize"))
				// animation.setRowSize(Integer.parseInt(infoInputList.get(i).getText()));
			// else if(infoLabelList.get(i).getText().equals("colSize"))
				// animation.setColSize(Integer.parseInt(infoInputList.get(i).getText()));
			// else if(infoLabelList.get(i).getText().equals("scaleRate"))
				// animation.setScaleRate(Double.parseDouble(infoInputList.get(i).getText()));
		// }
		
		// LinkedList<BufferedFrame> tmpFrameList = new LinkedList<BufferedFrame>();
		// for(int i=0; i<frameListModel.getSize(); i++)
			// tmpFrameList.add(frameListModel.get(i));
		// animation.setFrameList(tmpFrameList);
		// return animation;
	// }
	
	// private String getFromInfo(String target){
		// for(int i=0; i<infoLabelList.size(); i++)
			// if(infoLabelList.get(i).getText().equals(target))
				// return infoInputList.get(i).getText();
		// return null;
	// }
	
	// private void setToInfo(String target, String value){
		// for(int i=0; i<infoLabelList.size(); i++)
			// if(infoLabelList.get(i).getText().equals(target))
				// infoInputList.get(i).setText(value);
	// }
	
	// private void insertNewFrame(){
		// BufferedFrame frame = new BufferedFrame();
		// frame.setRow(Integer.parseInt(frameInputList.get(0).getText()));
		// frame.setCol(Integer.parseInt(frameInputList.get(1).getText()));
		// String fileName = animationListModel.get(animationList.getSelectedIndex()).getFrameFile();
		// try{
			// BufferedImage image = ImageIO.read(new FileInputStream(new File(fileName)));			
			// int rowSize = Integer.parseInt(getFromInfo("rowSize"));
			// int colSize = Integer.parseInt(getFromInfo("colSize"));
			// System.out.println("Image: " + image.getWidth() + ", " + image.getHeight());
			// System.out.println("Subimg: " + (frame.getCol()-1) * colSize + ", " + (frame.getRow()-1) * rowSize + ", " +  rowSize + ", " + colSize);
			// frame.setImage(image.getSubimage((frame.getCol()-1) * colSize, (frame.getRow()-1) * rowSize, colSize, rowSize));
			// if(frameList.getSelectedIndex() < 0)
				// frameListModel.addElement(frame);
			// else
				// frameListModel.insertElementAt(frame, frameList.getSelectedIndex());
			// animationListModel.get(animationList.getSelectedIndex()).setFrameNum(animationListModel.get(animationList.getSelectedIndex()).getFrameNum()+1);
			// setToInfo("frameNum", (Integer.parseInt(getFromInfo("frameNum")) + 1) + "");
		// }catch(Exception ex){
			// ex.printStackTrace();
		// }
	// }
	
	// private void smartInsertNewFrame(){
		// int rowSize = Integer.parseInt(getFromInfo("rowSize"));
		// int colSize = Integer.parseInt(getFromInfo("colSize"));
		// int rowNum = Integer.parseInt(smartInputList.get(1).getText()) / rowSize;
		// int colNum = Integer.parseInt(smartInputList.get(0).getText()) / colSize;
		// String fileName = animationListModel.get(animationList.getSelectedIndex()).getFrameFile();
		// try{
			// BufferedImage image = ImageIO.read(new FileInputStream(new File(fileName)));
			// System.out.println("ROWNUM: " + rowNum + "COLNUM" + colNum);
			// if(directionBox.isSelected()){
				// for(int i=0; i<rowNum; i++){
					// for(int j=0; j<colNum; j++){
						// BufferedFrame frame = new BufferedFrame();
						// frame.setRow(i+1);
						// frame.setCol(j+1);
						// System.out.println("Image: " + image.getWidth() + ", " + image.getHeight());
						// System.out.println("Subimg: " + (frame.getCol()-1) * colSize + ", " + (frame.getRow()-1) * rowSize + ", " +  rowSize + ", " + colSize);
						// frame.setImage(image.getSubimage((frame.getCol()-1) * colSize, (frame.getRow()-1) * rowSize, colSize, rowSize));
						// frameListModel.addElement(frame);
						// animationListModel.get(animationList.getSelectedIndex()).setFrameNum(animationListModel.get(animationList.getSelectedIndex()).getFrameNum()+1);
						// setToInfo("frameNum", (Integer.parseInt(getFromInfo("frameNum")) + 1) + "");
					// }
				// }
			// }
			// else{
				// for(int j=0; j<colNum; j++){
					// for(int i=0; i<rowNum; i++){
						// BufferedFrame frame = new BufferedFrame();
						// frame.setRow(i+1);
						// frame.setCol(j+1);
						// frame.setImage(image.getSubimage((frame.getCol()-1) * colSize, (frame.getRow()-1) * rowSize, colSize, rowSize));
						// frameListModel.addElement(frame);
						// animationListModel.get(animationList.getSelectedIndex()).setFrameNum(animationListModel.get(animationList.getSelectedIndex()).getFrameNum()+1);
						// setToInfo("frameNum", (Integer.parseInt(getFromInfo("frameNum")) + 1) + "");
					// }
				// }
			// }
		// }catch(Exception ex){
			// ex.printStackTrace();
		// }
	// }
	
	
	// private void saveAll(){
		// outputDoc = db.newDocument();
		// Element root = outputDoc.createElement("root");
		// outputDoc.appendChild(root);
		// ArrayList<Element> typeList = new ArrayList<Element>();
		// for(int i=0; i<animationTypeList.length; i++){
			// Element type = outputDoc.createElement(animationTypeList[i]);
			// root.appendChild(type);
			// typeList.add(type);
		// }
		
		// for(int i=0; i<animationListModel.getSize(); i++){
			// Element typeElement = typeList.get(0);
			// BufferedAnimation animation = animationListModel.get(i);
			
			// for(int j=0; j<animationTypeList.length; j++)
				// if(animation.getType().equals(animationTypeList[j]))
					// typeElement = typeList.get(j);
			
			// Element animationElement = outputDoc.createElement("animation");
			// animationElement.setAttribute("id", animation.getType() + i);
			// typeElement.appendChild(animationElement);
			
			// Element nameElement = outputDoc.createElement("name");
			// Text tName = outputDoc.createTextNode(animation.getName());
			// nameElement.appendChild(tName);
			// animationElement.appendChild(nameElement);
			
			// Element frameNumElement = outputDoc.createElement("frameNum");
			// Text tFrameNum = outputDoc.createTextNode(animation.getFrameNum() + "");
			// frameNumElement.appendChild(tFrameNum);
			// animationElement.appendChild(frameNumElement);
			
			// Element frameFileElement = outputDoc.createElement("frameFile");
			// Text tFrameFile = outputDoc.createTextNode(animation.getFrameFile());
			// frameFileElement.appendChild(tFrameFile);
			// animationElement.appendChild(frameFileElement);
			
			// Element rowSizeElement = outputDoc.createElement("rowSize");
			// Text tRowSize = outputDoc.createTextNode(animation.getRowSize() + "");
			// rowSizeElement.appendChild(tRowSize);
			// animationElement.appendChild(rowSizeElement);
			
			// Element colSizeElement = outputDoc.createElement("colSize");
			// Text tColSize = outputDoc.createTextNode(animation.getColSize() + "");
			// colSizeElement.appendChild(tColSize);
			// animationElement.appendChild(colSizeElement);
			
			// Element scaleRateElement = outputDoc.createElement("scale");
			// Text tScaleRate = outputDoc.createTextNode((int)(animation.getScaleRate()) + "");
			// scaleRateElement.appendChild(tScaleRate);
			// animationElement.appendChild(scaleRateElement);
			
			// for(int j=0; j<animation.getFrameList().size(); j++){
				// BufferedFrame frame = animation.getFrameList().get(j);
				// Element frameElement = outputDoc.createElement("frame");
				// frameElement.setAttribute("id", j + "");
				// animationElement.appendChild(frameElement);
				
				// Element rowElement = outputDoc.createElement("row");
				// Text tRow = outputDoc.createTextNode(frame.getRow() + "");
				// rowElement.appendChild(tRow);
				// frameElement.appendChild(rowElement);
				
				// Element colElement = outputDoc.createElement("col");
				// Text tCol = outputDoc.createTextNode(frame.getCol() + "");
				// colElement.appendChild(tCol);
				// frameElement.appendChild(colElement);
			// }
		// }
		
		// try{
			// TransformerFactory transformerFactory = TransformerFactory.newInstance();
			// Transformer transformer = transformerFactory.newTransformer();
			// transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			// transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			// DOMSource source = new DOMSource(outputDoc);
			// StreamResult result = new StreamResult(new File("xml/animation.xml"));
			// transformer.transform(source, result);
			// System.out.println("Hi");
		// }catch(Exception ex){
			// ex.printStackTrace();
		// }
	// }
	
	public void valueChanged(ListSelectionEvent e){
		if(e.getValueIsAdjusting())
			return;
		int index = ((JList)e.getSource()).getSelectedIndex();
		if(index < 0)
			return;
		if(e.getSource() == curEpisodeList){
			if(index < 0 || index >= curEpisodeListModel.getSize())
				return;
			BufferedEpisode episode = curEpisodeListModel.get(index);
			refreshByEpisode(episode);
			curSubEpisodeList.setSelectedIndex(0);
		}
		else if(e.getSource() == curSubEpisodeList){
			if(index < 0 || index >= curSubEpisodeListModel.getSize())
				return;
			Episode subEpisode = curSubEpisodeListModel.get(index);
			refreshBySub(subEpisode);
		}
	}
	
	public void actionPerformed(ActionEvent e){
		//if(e.getSource() == insertButton)
	}
}

class BufferedEpisode{
	private int id;
	private String name;
	private int subNum;
	private LinkedList<Episode> subList;
	
	public BufferedEpisode(){
	}
	
	public BufferedEpisode(int id, String name, int subNum, LinkedList<Episode> subList){
		this.id = id;
		this.name = name;
		this.subNum = subNum;
		this.subList = subList;
	}
	
	public int getId(){
		return id;
	}
	
	public String getName(){
		return name;
	}
	
	public int getSubNum(){
		return subNum;
	}
	
	public LinkedList<Episode> getSubList(){
		return subList;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setSubNum(int subNum){
		this.subNum = subNum;
	}
	
	public void setSubList(LinkedList<Episode> subList){
		this.subList = subList;
	}
	
	public String toString(){
		return new String("Episose " + id + " (" + subNum + ")");
	}
}