package rpg.resource;

import java.lang.*;
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.io.*;
import javax.imageio.*;

import rpg.map.*;
import rpg.draw.*;
import rpg.character.*;

/**
 * A class that implements all resource editor
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class ResourceEditor extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	private int width;
	private int height;
	private JTabbedPane tabPanel = new JTabbedPane();
	private JPanel mainPanel = new JPanel();
	
	public ResourceEditor(){
		super();
		setFullScreen();
		initialTabbedPane();
		
		this.setVisible(true);
	}
	
	private void setFullScreen(){
		this.setUndecorated(true);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		width = (int)screenSize.getWidth();
		height = (int)screenSize.getHeight();
		Rectangle bounds = new Rectangle(screenSize);
		this.setBounds(bounds);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
	}
	
	private void initialTabbedPane(){
		tabPanel.setBounds(0, 0, width, height);
		
		initialMainPanel();
		tabPanel.addTab("Main", mainPanel);
		
		AnimationEditor ae = new AnimationEditor(this);
		tabPanel.addTab("Animation", ae);
		
		EpisodeEditor ee = new EpisodeEditor(this);
		tabPanel.addTab("Episode", ee);
		
		tabPanel.addTab("Image", new JPanel());
		
		tabPanel.addTab("Item", new JPanel());
		
		MapEditor me = new MapEditor(this);
		tabPanel.addTab("Map", me);
		
		tabPanel.addTab("Monster", new JPanel());
		
		tabPanel.addTab("Music", new JPanel());
		
		//NPCEditor npce = new NPCEditor(this);
		tabPanel.addTab("NPC", new JPanel());
		
		tabPanel.addTab("Role", new JPanel());
		
		tabPanel.addTab("Skill", new JPanel());
		
		tabPanel.addTab("Sound", new JPanel());
		
		tabPanel.addTab("Treasure", new JPanel());
		
		this.getContentPane().add(tabPanel, 0);
	}
	
	private void initialMainPanel(){
		mainPanel.setLayout(null);
		mainPanel.setBounds(0, 0, width, height);
		JLabel infoLabel = new JLabel("ResourceEditor Version 1.0");
		infoLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 26));
		infoLabel.setBounds(width/2 - 200, height/2 - 80, 400, 150);
		mainPanel.add(infoLabel);
	}
	
	public void actionPerformed(ActionEvent e){
		
	}
	
	public static void main(String[] args){
		ResourceEditor rs = new ResourceEditor();
	}
}