package rpg.resource;

import java.lang.*;
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.io.*;
import javax.imageio.*;

import rpg.resource.*;
import rpg.draw.*;
import rpg.character.*;
import rpg.map.*;

/**
 * A class that implements a map editor
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class MapEditor extends JPanel implements ActionListener, Runnable{
	
	private static final long serialVersionUID = 1L;
	private static final int MAX_MAP_SIZE = 500;
	private static final int MAX_ICON_NUM = 20000;
	private static final int BLOCK_SIZE = 16;
	private static final int EDIT_SIZE = 20;
	private static final int ELEMENT_SIZE = 32;
	private JPanel editPanel;
	private JPanel elementPanel;
	private JPanel propertyPanel;
	private JPanel controlPanel;
	private JPanel filePanel;
	private JPanel monsterPanel;
	private JPanel usagePanel;
	
	private String[] mapTypeList = {"Village", "BigMap", "Maze"};
	private JComboBox<Monster> monsterChooser = new JComboBox<Monster>();
	private JComboBox<String> typeBox = new JComboBox<String>(mapTypeList);
	
	private JCheckBox isObstacleBox = new JCheckBox("is Obstacle");
	private JCheckBox isExitBox = new JCheckBox("is Exit");
	private JCheckBox eraserBox = new JCheckBox("Eraser");
	private JCheckBox isMergeBox = new JCheckBox("is Merge");
	private JCheckBox isAnimationBox = new JCheckBox("is Animation");
	private JCheckBox isScaleBox = new JCheckBox("is Scale");
	private JCheckBox chooseRangeBox = new JCheckBox("choose Range");
	
	private JTextField meetRateField = new JTextField();
	private JTextField mapIdField = new JTextField();
	private JTextField newRowField = new JTextField();
	private JTextField newColField = new JTextField();
	
	private JLabel curPosLabel = new JLabel();
	private JLabel minPosLabel = new JLabel();
	private JLabel maxPosLabel = new JLabel();
	private JLabel monsterPortrait = new JLabel();
	private JLabel totalRate = new JLabel(" Total: " + 0);
	private JLabel selectedMapLabel;
	
	private JFileChooser fileChooser = new JFileChooser();
	private JScrollPane scroller;
	private JButton[][] buttonArray;
	private ButtonInfo[][] buttonInfo;
	private ImageIcon[] choiceIcon;
	private boolean[][] isCreate;
	
	private boolean isFirst = true;
	
	private BufferedImage defaultImg;
	
	private double[] meetRateList;
	
	private DefaultListModel<ImageIcon> iconListModel = new DefaultListModel<ImageIcon>();
	private JList<ImageIcon> iconList = new JList<ImageIcon>(iconListModel);
	
	private BufferedImage[] imageList = ResourceHandler.getImageListByName("mapElementList");
	private String defaultImgPath = ResourceHandler.getResourcePath("image", "defaultMapImage");
	private BufferedImage obstacleImg = ResourceHandler.getImageByName("other", "obstacleMapImage");
	private BufferedImage exitImg = ResourceHandler.getImageByName("other", "exitMapImage");
	
	private ImageIcon tmpSaveIcon;
	
	private int choiceIndex;
	private int curRow = MAX_MAP_SIZE/2 - EDIT_SIZE/2;
	private int curCol = MAX_MAP_SIZE/2 - EDIT_SIZE/2;
	private int minRow = curRow;
	private int maxRow = curRow;
	private int minCol = curCol;
	private int maxCol = curCol;
	private int width;
	private int height;
	
	private boolean rangeOpen;
	private int startIndex;
	private int endIndex;
	
	private boolean isStop;
	private boolean isStart;
	
	private int startingRow;
	private int startingCol;
	
	private ResourceEditor caller;
	
	public MapEditor(ResourceEditor caller){
		super();
		this.caller = caller;
		initial();
		setElementArea();
		setEditArea();
		setPropertyArea();
		setControlArea();
		setFileArea();
		setMonsterArea();
		setUsageArea();
		this.setVisible(true);
	}
	
	private void initial(){
		width = caller.getWidth();
		height = caller.getHeight();
		this.setBounds(0, 0, width, height);
		this.setLayout(null);
		try{
			defaultImg = ImageIO.read(new FileInputStream(new File(defaultImgPath)));
		}catch(Exception e){
			e.printStackTrace();
		}
		buttonArray = new JButton[EDIT_SIZE][EDIT_SIZE];
		for(int i=0; i<EDIT_SIZE; i++)
			buttonArray[i] = new JButton[EDIT_SIZE];
		buttonInfo = new ButtonInfo[MAX_MAP_SIZE][MAX_MAP_SIZE];
		isCreate = new boolean[MAX_MAP_SIZE][MAX_MAP_SIZE];
		for(int i=0; i<MAX_MAP_SIZE; i++){
			buttonInfo[i] = new ButtonInfo[MAX_MAP_SIZE];
			isCreate[i] = new boolean[MAX_MAP_SIZE];
		}
		choiceIcon = new ImageIcon[MAX_ICON_NUM];
	}
	
	private void setElementArea(){
		elementPanel = new JPanel(new GridLayout(0, 32));
		int elementCount = 0;
		for(int i=0; i<imageList.length; i++){
			try{
				BufferedImage srcImg = imageList[i];
				int srcWidth = srcImg.getWidth();
				int srcHeight = srcImg.getHeight();
				int colNum = srcWidth / BLOCK_SIZE;
				int rowNum = srcHeight / BLOCK_SIZE;
				for(int row=0; row<rowNum; row++){
					for(int col=0; col<colNum; col++){
						BufferedImage tmpImg = srcImg.getSubimage(col * BLOCK_SIZE, row * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
						choiceIcon[elementCount] = new ImageIcon(tmpImg);
						JButton button = new JButton(choiceIcon[elementCount]);
						button.setPreferredSize(new Dimension(BLOCK_SIZE, BLOCK_SIZE));
						button.setActionCommand("Element-" + elementCount);
						button.addActionListener(this);
						elementPanel.add(button);
						elementCount++;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		scroller = new JScrollPane(elementPanel);
		scroller.setBounds(10, 10, (int)(width * 0.4), (int)(height * 0.6));
		scroller.setVerticalScrollBar(scroller.createVerticalScrollBar());
		this.add(scroller);
	}
	
	private void setEditArea(){
		editPanel = new JPanel(new GridLayout(EDIT_SIZE, EDIT_SIZE));
		for(int i=0; i<EDIT_SIZE; i++){
			for(int j=0; j<EDIT_SIZE; j++){
				buttonArray[i][j] = new JButton();
				buttonArray[i][j].setPreferredSize(new Dimension(BLOCK_SIZE,BLOCK_SIZE));
				buttonArray[i][j].setIcon(new ImageIcon(defaultImg));
				buttonArray[i][j].addActionListener(this);
				buttonArray[i][j].setActionCommand("Edit-" + (EDIT_SIZE*i+j));
				buttonArray[i][j].addMouseListener(new MouseAdapter(){
					public ImageIcon savedImage;
					public ImageIcon redImage;
					
					public void mouseEntered(MouseEvent evt){
						savedImage = (ImageIcon)((JButton)evt.getSource()).getIcon();
						redImage = new ImageIcon(addShadow("RED", Drawer.imageToBufferedImage(savedImage.getImage())));
						((JButton)evt.getSource()).setIcon(redImage);
					}
					
					public void mouseExited(MouseEvent evt){
						if((ImageIcon)((JButton)evt.getSource()).getIcon() == redImage)
							((JButton)evt.getSource()).setIcon(savedImage);
					}
					});
				editPanel.add(buttonArray[i][j]);
			}
		}
		editPanel.setBounds((int)(width*0.5) - 10, 20, width/2, height-50);
		this.add(editPanel);
	}
	
	private void setPropertyArea(){
		try{
			propertyPanel = new JPanel(new GridLayout(7, 2));
			propertyPanel.setPreferredSize(new Dimension(width/4, height/4));
			selectedMapLabel = new JLabel(new ImageIcon(defaultImg));
			propertyPanel.add(selectedMapLabel);
			propertyPanel.add(typeBox);
			propertyPanel.add(isObstacleBox);
			isObstacleBox.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if(isObstacleBox.isSelected())
						isExitBox.setSelected(false);
				}
			});
			propertyPanel.add(isExitBox);
			isExitBox.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if(isExitBox.isSelected()){
						isObstacleBox.setSelected(false);
						mapIdField.setEditable(true);
						newRowField.setEditable(true);
						newColField.setEditable(true);
					}
					else{
						mapIdField.setEditable(false);
						newRowField.setEditable(false);
						newColField.setEditable(false);
					}
				}
			});
			propertyPanel.add(new JLabel("Map id:"));
			propertyPanel.add(mapIdField);
			propertyPanel.add(new JLabel("Row pos:"));
			propertyPanel.add(newRowField);
			propertyPanel.add(new JLabel("Col pos:"));
			propertyPanel.add(newColField);
			mapIdField.setEditable(false);
			newRowField.setEditable(false);
			newColField.setEditable(false);
			propertyPanel.setBounds(20 , 50 + (int)(height * 0.64), (int)(width * 0.15), (int)(height * 0.3));
			this.add(propertyPanel);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void setControlArea(){
		controlPanel = new JPanel(new BorderLayout());
		JButton upButton = new JButton("^");
		upButton.setActionCommand("UP");
		upButton.addActionListener(this);
		controlPanel.add(upButton, BorderLayout.NORTH);
		JButton downButton = new JButton("v");
		downButton.setActionCommand("DOWN");
		downButton.addActionListener(this);
		controlPanel.add(downButton, BorderLayout.SOUTH);
		JButton leftButton = new JButton("<");
		leftButton.setActionCommand("LEFT");
		leftButton.addActionListener(this);
		controlPanel.add(leftButton, BorderLayout.WEST);
		JButton rightButton = new JButton(">");
		rightButton.setActionCommand("RIGHT");
		rightButton.addActionListener(this);
		controlPanel.add(rightButton, BorderLayout.EAST);
		controlPanel.setBounds((int)(width * 0.3) + 30, height * 8 / 10, width / 10 - 30, (int)(height * 0.1) );
		this.add(controlPanel);
	}
	
	private void setFileArea(){
		filePanel = new JPanel(new GridLayout(10, 1));
		filePanel.add(curPosLabel);
		filePanel.add(minPosLabel);
		filePanel.add(maxPosLabel);
		updateCurPosLabel();
		filePanel.add(eraserBox);
		CreateButton("Save");
		CreateButton("Load");
		CreateButton("Clean");
		CreateButton("Play");
		CreateButton("Stop");
		filePanel.setBounds((int)(width * 0.2), (int)(height *  0.7), width / 10 - 30, (int)(height * 0.25) );
		this.add(filePanel);
	}
	
	private void setMonsterArea(){
		monsterPanel = new JPanel(new GridLayout(1, 3));
		Monster[] monsterList = ResourceHandler.getAllMonsters();
		meetRateList = new double[monsterList.length];
		for(int i=0; i<monsterList.length; i++)
			monsterChooser.addItem(monsterList[i]);
		monsterChooser.setActionCommand("Monster");
		monsterChooser.addActionListener(this);
		monsterPanel.add(monsterChooser);
		monsterPortrait.setIcon(monsterList[0].getImage());
		monsterPanel.add(monsterPortrait);
		meetRateField.setText(meetRateList[0] + "");
		meetRateField.getDocument().addDocumentListener(
			new DocumentListener(){
				public void changedUpdate(DocumentEvent e){
				}
				public void removeUpdate(DocumentEvent e){
					try{
						meetRateList[monsterChooser.getSelectedIndex()] = Double.parseDouble(meetRateField.getText());
						refreshTotalRate();
					}catch(Exception ex){
					}
				}
				public void insertUpdate(DocumentEvent e){
					try{
						meetRateList[monsterChooser.getSelectedIndex()] = Double.parseDouble(meetRateField.getText());
						refreshTotalRate();
					}catch(Exception ex){
					}
				}
			}
		);
		monsterPanel.add(meetRateField);
		monsterPanel.add(totalRate);
		monsterPanel.setBounds(20, 20 + (int)(height * 0.6), (int)(width * 0.4), (int)(height * 0.06));
		this.add(monsterPanel);
	}
	
	private void setUsageArea(){
		usagePanel = new JPanel(new GridLayout(5, 1));
		usagePanel.setBounds((int)(width * 0.3) + 30, (int)(height * 0.69), (int)(width * 0.14), (int)(height * 0.1));
		isMergeBox.setSelected(true);
		isMergeBox.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if(isMergeBox.isSelected())
						isAnimationBox.setSelected(false);
					else
						isAnimationBox.setSelected(true);
				}
			});
		isAnimationBox.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if(isAnimationBox.isSelected())
						isMergeBox.setSelected(false);
					else
						isMergeBox.setSelected(true);
				}
			});
		usagePanel.add(isMergeBox);
		usagePanel.add(isAnimationBox);
		usagePanel.add(isScaleBox);
		usagePanel.add(chooseRangeBox);
		this.add(usagePanel);
	}
	
	private void refreshTotalRate(){
		double rate = 0;
		for(int i=0; i<monsterChooser.getItemCount(); i++)
			rate += meetRateList[i];
		totalRate.setText(" Total: " + rate);
	}
	
	private void CreateButton(String str){
		JButton button = new JButton(str);
		button.addActionListener(this);
		button.setActionCommand(str);
		filePanel.add(button);
	}
	
	public void actionPerformed(ActionEvent e){
		String cmd = e.getActionCommand();
		if(cmd.startsWith("Element")){
			choiceIndex = Integer.parseInt(cmd.substring(8));
			if(chooseRangeBox.isSelected()){
				if(!rangeOpen){
					startIndex = choiceIndex;
					rangeOpen = true;
				}
				else{
					endIndex = choiceIndex;
					rangeOpen = false;
				}
			}
			
			if(!isScaleBox.isSelected())
				selectedMapLabel.setIcon(new ImageIcon(getMergeImg(choiceIndex)));
			else{
				BufferedImage img = Drawer.imageToBufferedImage(choiceIcon[choiceIndex].getImage());
				selectedMapLabel.setIcon(new ImageIcon(getMergeImg2(choiceIndex)));
			}
		}
		else if(cmd.startsWith("Edit")){
			int originRow = Integer.parseInt(cmd.substring(5)) / EDIT_SIZE;
			int originCol = Integer.parseInt(cmd.substring(5)) % EDIT_SIZE;
			
			if(isFirst){
				minRow = curRow + originRow;
				minCol = curCol + originCol;
				maxRow = curRow + originRow;
				maxCol = curCol + originCol;
				isFirst = false;
			}
			
			if(chooseRangeBox.isSelected()){
				if(rangeOpen)
					return;
					
				int leftLimit = startIndex % ELEMENT_SIZE;
				int rightLimit = endIndex % ELEMENT_SIZE + 1;
				int upLimit = startIndex / ELEMENT_SIZE;
				int downLimit = endIndex / ELEMENT_SIZE + 1;
				
				System.out.printf("Start: %d, EndL %d\n", startIndex, endIndex);
				
				for(int row=upLimit; row<downLimit; row = row+2){
					for(int col=leftLimit; col<rightLimit; col = col+2){
						BufferedImage img = getMergeImg(row * ELEMENT_SIZE + col);
						if(!isCreate[curRow + originRow + (row-upLimit)/2][curCol + originCol + (col-leftLimit)/2]){
							buttonInfo[curRow + originRow + (row-upLimit)/2][curCol + originCol + (col-leftLimit)/2] = new ButtonInfo();
							updateInfo(img, buttonInfo[curRow + originRow + (row-upLimit)/2][curCol + originCol + (col-leftLimit)/2]);
							updateBounds(curRow + originRow + (row-upLimit)/2, curCol + originCol + (col-leftLimit)/2);
							isCreate[curRow + originRow + (row-upLimit)/2][curCol + originCol + (col-leftLimit)/2] = true;
							if(isObstacleBox.isSelected())
								buttonArray[originRow + (row-upLimit)/2][originCol + (col-leftLimit)/2].setIcon(new ImageIcon(combineImg(img, obstacleImg)));
							else if(isExitBox.isSelected())
								buttonArray[originRow + (row-upLimit)/2][originCol + (col-leftLimit)/2].setIcon(new ImageIcon(combineImg(img, exitImg)));
							else
								buttonArray[originRow + (row-upLimit)/2][originCol + (col-leftLimit)/2].setIcon(new ImageIcon(img));
						}
						else{							
							System.out.printf("%d %d\n", curRow + originRow + (row-upLimit)/2, curCol + originCol + (col-leftLimit)/2);
							BufferedImage topImg = combineImg(Drawer.imageToBufferedImage(buttonInfo[curRow + originRow + (row-upLimit)/2][curCol + originCol + (col-leftLimit)/2].mapElement.getImage().getImage()), img);
							updateInfo(topImg, buttonInfo[curRow + originRow + (row-upLimit)/2][curCol + originCol + (col-leftLimit)/2]);
							if(isObstacleBox.isSelected())
								buttonArray[originRow + (row-upLimit)/2][originCol + (col-leftLimit)/2].setIcon(new ImageIcon(combineImg(topImg, obstacleImg)));
							else if(isExitBox.isSelected())
								buttonArray[originRow + (row-upLimit)/2][originCol + (col-leftLimit)/2].setIcon(new ImageIcon(combineImg(topImg, exitImg)));
							else
								buttonArray[originRow + (row-upLimit)/2][originCol + (col-leftLimit)/2].setIcon(new ImageIcon(topImg));						
						}
					}
				}
				return;
			}
			
			if(isStart){
				int beginRow = originRow > startingRow ? startingRow : originRow;
				int beginCol = originCol > startingCol ? startingCol : originCol;
				int endRow = originRow > startingRow ? originRow : startingRow;
				int endCol = originCol > startingCol ? originCol : startingCol;
				
				for(int row=beginRow; row <= endRow; row++){
					for(int col=beginCol; col <= endCol; col++){
						if(!eraserBox.isSelected()){
							BufferedImage img = Drawer.imageToBufferedImage(((ImageIcon)selectedMapLabel.getIcon()).getImage());
							if(!isCreate[curRow + row][curCol + col]){
								buttonInfo[curRow + row][curCol + col] = new ButtonInfo();
								updateInfo(img, buttonInfo[curRow + row][curCol + col]);
								updateBounds(curRow + row, curCol + col);
								isCreate[curRow + row][curCol + col] = true;
								if(isObstacleBox.isSelected())
									buttonArray[row][col].setIcon(new ImageIcon(combineImg(img, obstacleImg)));
								else if(isExitBox.isSelected())
									buttonArray[row][col].setIcon(new ImageIcon(combineImg(img, exitImg)));
								else
									buttonArray[row][col].setIcon(new ImageIcon(img));
							}
							else{
								if(isAnimationBox.isSelected()){
									if(row == startingRow && col == startingCol)
										buttonArray[row][col].setIcon(tmpSaveIcon);
									buttonInfo[curRow + row][curCol + col].mapElement.addImage(new ImageIcon(img));
								}
								else{
									if(row == startingRow && col == startingCol)
										buttonArray[row][col].setIcon(tmpSaveIcon);
									BufferedImage topImg = combineImg(Drawer.imageToBufferedImage(buttonInfo[curRow + row][curCol + col].mapElement.getImage().getImage()), img);
									updateInfo(topImg, buttonInfo[curRow + row][curCol + col]);
									if(isObstacleBox.isSelected())
										buttonArray[row][col].setIcon(new ImageIcon(combineImg(topImg, obstacleImg)));
									else if(isExitBox.isSelected())
										buttonArray[row][col].setIcon(new ImageIcon(combineImg(topImg, exitImg)));
									else
										buttonArray[row][col].setIcon(new ImageIcon(topImg));
								}
							}
						}
						else{
							buttonArray[row][col].setIcon(new ImageIcon(defaultImg));
							isCreate[curRow + row][curCol + col] = false;
						}
					}
				}
				isStart = false;
			}
			else{
				startingRow = originRow;
				startingCol = originCol;
				tmpSaveIcon = (ImageIcon)buttonArray[originRow][originCol].getIcon();
				ImageIcon newIcon = new ImageIcon(addShadow("RED", Drawer.imageToBufferedImage( ((ImageIcon)buttonArray[originRow][originCol].getIcon()).getImage() )));
				buttonArray[originRow][originCol].setIcon(newIcon);
				isStart = true;
			}
		}
		else if(cmd.equals("UP")){
			for(int row=EDIT_SIZE-1; row>0; row--)
				for(int col=0; col<EDIT_SIZE; col++)
					buttonArray[row][col].setIcon(buttonArray[row-1][col].getIcon());
			
			for(int col=0; col<EDIT_SIZE; col++){
				if(isCreate[curRow - 1][curCol + col]){
					buttonArray[0][col].setIcon(new ImageIcon(buttonInfo[curRow - 1][curCol + col].mapElement.getImage().getImage()));
					if(buttonInfo[curRow-1][curCol+col].mapElement.isObstacle())
						buttonArray[0][col].setIcon(new ImageIcon(combineImg(((ImageIcon)buttonArray[0][col].getIcon()).getImage(), obstacleImg)));
					else if(buttonInfo[curRow-1][curCol+col].mapElement.isExit())
						buttonArray[0][col].setIcon(new ImageIcon(combineImg(((ImageIcon)buttonArray[0][col].getIcon()).getImage(), exitImg)));
				}
				else
					buttonArray[0][col].setIcon(new ImageIcon(defaultImg));
			}			
			curRow -= 1;
			updateCurPosLabel();
		}
		else if(cmd.equals("DOWN")){
			for(int row=0; row<EDIT_SIZE-1; row++)
				for(int col=0; col<EDIT_SIZE; col++)
					buttonArray[row][col].setIcon(buttonArray[row+1][col].getIcon());
			
			for(int col=0; col<EDIT_SIZE; col++){
				if(isCreate[curRow + EDIT_SIZE][curCol + col]){
					buttonArray[EDIT_SIZE-1][col].setIcon(new ImageIcon(buttonInfo[curRow + EDIT_SIZE][curCol + col].mapElement.getImage().getImage()));
					if(buttonInfo[curRow + EDIT_SIZE][curCol+col].mapElement.isObstacle())
						buttonArray[EDIT_SIZE-1][col].setIcon(new ImageIcon(combineImg(((ImageIcon)buttonArray[EDIT_SIZE-1][col].getIcon()).getImage(), obstacleImg)));
					else if(buttonInfo[curRow + EDIT_SIZE][curCol + col].mapElement.isExit())
						buttonArray[EDIT_SIZE-1][col].setIcon(new ImageIcon(combineImg(((ImageIcon)buttonArray[EDIT_SIZE-1][col].getIcon()).getImage(), exitImg)));
				}
				else
					buttonArray[EDIT_SIZE-1][col].setIcon(new ImageIcon(defaultImg));
			}
			curRow += 1;
			updateCurPosLabel();
		}
		else if(cmd.equals("LEFT")){
			for(int col=EDIT_SIZE-1; col>0; col--)
				for(int row=0; row<EDIT_SIZE; row++)
					buttonArray[row][col].setIcon(buttonArray[row][col-1].getIcon());
			
			for(int row=0; row<EDIT_SIZE; row++){
				if(isCreate[curRow + row][curCol - 1]){
					buttonArray[row][0].setIcon(new ImageIcon(buttonInfo[curRow + row][curCol -1].mapElement.getImage().getImage()));
					if(buttonInfo[curRow + row][curCol -1].mapElement.isObstacle())
						buttonArray[row][0].setIcon(new ImageIcon(combineImg(((ImageIcon)buttonArray[row][0].getIcon()).getImage(), obstacleImg)));
					else if(buttonInfo[curRow + row][curCol -1].mapElement.isExit())
						buttonArray[row][0].setIcon(new ImageIcon(combineImg(((ImageIcon)buttonArray[row][0].getIcon()).getImage(), exitImg)));
				}
				else
					buttonArray[row][0].setIcon(new ImageIcon(defaultImg));
			}			
			curCol -= 1;
			updateCurPosLabel();
		}
		else if(cmd.equals("RIGHT")){
			for(int col=0; col<EDIT_SIZE-1; col++)
				for(int row=0; row<EDIT_SIZE; row++)
					buttonArray[row][col].setIcon(buttonArray[row][col+1].getIcon());
			
			for(int row=0; row<EDIT_SIZE; row++){
				if(isCreate[curRow + row][curCol + EDIT_SIZE]){
					buttonArray[row][EDIT_SIZE-1].setIcon(new ImageIcon(buttonInfo[curRow + row][curCol + EDIT_SIZE].mapElement.getImage().getImage()));				
					if(buttonInfo[curRow + row][curCol + EDIT_SIZE].mapElement.isObstacle())
						buttonArray[row][EDIT_SIZE-1].setIcon(new ImageIcon(combineImg(((ImageIcon)buttonArray[row][EDIT_SIZE-1].getIcon()).getImage(), obstacleImg)));
					else if(buttonInfo[curRow + row][curCol + EDIT_SIZE].mapElement.isExit())
						buttonArray[row][EDIT_SIZE-1].setIcon(new ImageIcon(combineImg(((ImageIcon)buttonArray[row][EDIT_SIZE-1].getIcon()).getImage(), exitImg)));
				}
				else
					buttonArray[row][EDIT_SIZE-1].setIcon(new ImageIcon(defaultImg));
			}
			curCol += 1;
			updateCurPosLabel();
		}
		else if(cmd.equals("Save"))
			SaveMap();
		else if(cmd.equals("Load"))
			LoadMap();
		else if(cmd.equals("Clean")){
			int returnValue = JOptionPane.showConfirmDialog(this, "Are you sure to clean?");
			if(returnValue == JOptionPane.OK_OPTION)
				cleanAll();
		}
		else if(cmd.equals("Play")){
			isStop = false;
			Thread thread = new Thread(this);
			thread.start();
		}
		else if(cmd.equals("Stop"))
			isStop = true;
		else if(cmd.equals("Monster")){
			monsterPortrait.setIcon(((Monster)(((JComboBox)e.getSource()).getSelectedItem())).getImage());
			meetRateField.setText(meetRateList[((JComboBox)e.getSource()).getSelectedIndex()] + "");
		}
	}
	
	private void updateCurPosLabel(){
		curPosLabel.setText("cur: " + curRow + ", " + curCol);
		minPosLabel.setText("min: " + minRow + ", " + minCol);
		maxPosLabel.setText("max: " + maxRow + ", " + maxCol);
	}
	
	public void run(){
		while(!isStop){
			for(int row=0; row<EDIT_SIZE; row++){
				for(int col=0; col<EDIT_SIZE; col++){
					if(isCreate[curRow + row][curCol + col])
						buttonArray[row][col].setIcon(buttonInfo[curRow + row][curCol + col].mapElement.getNextFrame());
				}
			}
			try{
				Thread.sleep(200);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}
	
	private void SaveMap(){
		int returnValue = fileChooser.showSaveDialog(this);
		if(returnValue == JFileChooser.APPROVE_OPTION){
			File file = fileChooser.getSelectedFile();
			int mapId = Integer.parseInt(fileChooser.getName(file).substring(0, 3));
			String mapName = fileChooser.getName(file).substring(3);
			String mapType = (String)typeBox.getSelectedItem();
			int mapWidth = maxCol - minCol + 1;
			int mapHeight = maxRow - minRow + 1;
			Map map = new Map(mapWidth, mapHeight, mapId, mapName, mapType);
			
			for(int row=0; row<mapHeight; row++)
				for(int col=0; col<mapWidth; col++){
					if(isCreate[minRow + row][minCol + col])
						map.setElement(row, col, buttonInfo[minRow + row][minCol + col].mapElement);
					else
						map.setElement(row, col, null);
				}
			
			if(mapType.equals("Maze") || mapType.equals("BigMap")){
				int monsterCount = 0;
				for(int i=0; i<monsterChooser.getItemCount(); i++)
					if(meetRateList[i] != 0)
						monsterCount++;
						
				Monster[] mapMonsterList = new Monster[monsterCount];
				double[] mapMeetRateList = new double[monsterCount];
				monsterCount = 0;
				for(int i=0; i<monsterChooser.getItemCount(); i++){
					if(meetRateList[i] != 0){
						mapMonsterList[monsterCount] = new Monster(monsterChooser.getItemAt(i));
						mapMeetRateList[monsterCount] = meetRateList[i];
						monsterCount++;
					}
				}
				map.setMonsters(mapMonsterList, mapMeetRateList);
			}
			
			try{
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file, false));
				oos.writeObject(map);
				oos.close();
				JOptionPane.showMessageDialog(this, "Save successfully!");
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}		
	}
	
	private void LoadMap(){
		int returnValue = fileChooser.showOpenDialog(this);
		if(returnValue == JFileChooser.APPROVE_OPTION){
			File file = fileChooser.getSelectedFile();
			try{
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
				Map map = (Map)ois.readObject();
				ois.close();
				
				cleanAll();
				
				curRow = MAX_MAP_SIZE/2 - map.getHeight()/2;
				curCol = MAX_MAP_SIZE/2 - map.getWidth()/2;
				minRow = curRow;
				minCol = curCol;
				
				for(int row=0; row<map.getHeight(); row++){
					for(int col=0; col<map.getWidth(); col++){
						buttonInfo[curRow + row][curCol + col] = new ButtonInfo();
						buttonInfo[curRow + row][curCol + col].mapElement = map.getElement(row, col);
						if(buttonInfo[curRow + row][curCol + col].mapElement == null)
							continue;
						isCreate[curRow + row][curCol + col] = true;
						if(row < EDIT_SIZE && col < EDIT_SIZE){
							BufferedImage targetImg = Drawer.imageToBufferedImage(buttonInfo[curRow + row][curCol + col].mapElement.getImage().getImage());
							if(buttonInfo[curRow + row][curCol + col].mapElement.isObstacle())
								buttonArray[row][col].setIcon(new ImageIcon(combineImg(targetImg, obstacleImg)));
							else if(buttonInfo[curRow + row][curCol + col].mapElement.isExit())
								buttonArray[row][col].setIcon(new ImageIcon(combineImg(targetImg, exitImg)));
							else
								buttonArray[row][col].setIcon(buttonInfo[curRow + row][curCol + col].mapElement.getImage());
						}
					}
				}
				
				maxRow = minRow + map.getHeight();
				maxCol = minCol + map.getWidth();
				
				typeBox.setSelectedItem(map.getType());
				
				if(map.getType().equals("Maze") || map.getType().equals("BigMap")){
					int monsterCount = 0;
					for(int i=0; i<map.getMonsterCount(); i++){
						Monster monster = map.getMonster(i);
						for(int j=0; j<monsterChooser.getItemCount(); j++){
							if(monster.getName().equals(monsterChooser.getItemAt(j).getName()))
								meetRateList[j] = map.getRate(i);
							else
								meetRateList[j] = 0;
						}
					}
					monsterChooser.setSelectedIndex(0);
					
					monsterPortrait.setIcon(((Monster)(monsterChooser.getSelectedItem())).getImage());
					meetRateField.setText(meetRateList[0] + "");
				}
				
				updateCurPosLabel();
				
				JOptionPane.showMessageDialog(this, "Load successfully!");
			}catch(Exception ex){
				ex.printStackTrace();
			}
			isFirst = false;
		}
	}
	
	private void cleanAll(){
		for(int row=0; row<MAX_MAP_SIZE; row++)
			for(int col=0; col<MAX_MAP_SIZE; col++){
				isCreate[row][col] = false;
			}
		
		for(int row=0; row<EDIT_SIZE; row++)
			for(int col=0; col<EDIT_SIZE; col++){
				buttonArray[row][col].setIcon(new ImageIcon(defaultImg));
			}
			
		curRow = MAX_MAP_SIZE/2 - EDIT_SIZE/2;
		curCol = MAX_MAP_SIZE/2 - EDIT_SIZE/2;
		minRow = curRow;
		maxRow = curRow;
		minCol = curCol;
		maxCol = curCol;
		isFirst = true;
		
		updateCurPosLabel();
	}
	
	private BufferedImage combineImg(BufferedImage src, BufferedImage dest){
		BufferedImage combined = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics g = combined.getGraphics();
		g.drawImage(src, 0, 0, null);
		g.drawImage(dest, 0, 0, null);
		return combined;
	}
	
	private BufferedImage combineImg(Image _src, BufferedImage dest){
		BufferedImage src = Drawer.imageToBufferedImage(_src);
		BufferedImage combined = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics g = combined.getGraphics();
		g.drawImage(src, 0, 0, null);
		g.drawImage(dest, 0, 0, null);
		return combined;
	}
	
	private BufferedImage addShadow(String color, BufferedImage img){
		int colorCode;
		if(color.equals("RED"))
			colorCode = 0xff0000;
		else
			colorCode = 0x00ff00;
		BufferedImage destImg = Drawer.deepCopyBI(img);
		for(int i=0; i<destImg.getWidth(); i++)
			for(int j=0; j<destImg.getHeight(); j++){
				int rgb = destImg.getRGB(i, j);
				rgb |= colorCode;
				destImg.setRGB(i, j, rgb);
			}
		return destImg;
	}
	
	private BufferedImage getMergeImg(int choiceIndex){
		BufferedImage img = new BufferedImage(BLOCK_SIZE * 2, BLOCK_SIZE * 2, BufferedImage.TYPE_INT_ARGB);
		img.createGraphics().drawImage(choiceIcon[choiceIndex].getImage(), 0, 0, null);
		img.createGraphics().drawImage(choiceIcon[choiceIndex+1].getImage(), BLOCK_SIZE, 0, null);
		img.createGraphics().drawImage(choiceIcon[choiceIndex+ELEMENT_SIZE].getImage(), 0, BLOCK_SIZE, null);
		img.createGraphics().drawImage(choiceIcon[choiceIndex+ELEMENT_SIZE+1].getImage(), BLOCK_SIZE, BLOCK_SIZE, null);
		return img;
	}
	
	private BufferedImage getMergeImg2(int choiceIndex){
		BufferedImage img = new BufferedImage(BLOCK_SIZE * 2, BLOCK_SIZE * 2, BufferedImage.TYPE_INT_ARGB);
		img.createGraphics().drawImage(choiceIcon[choiceIndex].getImage(), 0, 0, null);
		img.createGraphics().drawImage(choiceIcon[choiceIndex].getImage(), BLOCK_SIZE, 0, null);
		img.createGraphics().drawImage(choiceIcon[choiceIndex].getImage(), 0, BLOCK_SIZE, null);
		img.createGraphics().drawImage(choiceIcon[choiceIndex].getImage(), BLOCK_SIZE, BLOCK_SIZE, null);
		return img;
	}
	
	private void updateBounds(int row, int col){
		if(row < minRow)
			minRow = row;
		if(row > maxRow)
			maxRow = row;
		if(col < minCol)
			minCol = col;
		if(col > maxCol)
			maxCol = col;
		updateCurPosLabel();
	}
	
	private void updateInfo(BufferedImage img, ButtonInfo buttonInfo){
		try{
			buttonInfo.mapElement.setImage(new ImageIcon(img));
			buttonInfo.mapElement.setObstacle(isObstacleBox.isSelected());
			buttonInfo.mapElement.setExit(isExitBox.isSelected());	
			
			if(isExitBox.isSelected()){
				buttonInfo.mapElement.setNextMapId(Integer.parseInt(mapIdField.getText()));
				buttonInfo.mapElement.setNextPosRow(Integer.parseInt(newRowField.getText()));
				buttonInfo.mapElement.setNextPosCol(Integer.parseInt(newColField.getText()));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}

class ButtonInfo extends JButton{
	
	private static final long serialVersionUID = 1L;
	private BufferedImage defaultImg;
	
	public MapElement mapElement;
	
	public ButtonInfo(){
		try{
			defaultImg = ResourceHandler.getImageByName("other", "defaultMapImage");
			mapElement = new MapElement(new ImageIcon(defaultImg), false, false, 0, 0, 0, 0);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}