package rpg.resource;

import java.lang.*;
import java.util.*;
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.io.*;
import javax.imageio.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import org.w3c.dom.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import rpg.resource.*;
import rpg.draw.*;
import rpg.character.*;
import rpg.map.*;

/**
 * A class that implements an animation editor
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class AnimationEditor extends JPanel implements ActionListener, ListSelectionListener, Runnable{

	private static final long serialVersionUID = 1L;
	private static final int DISPLAY_SIZE = 200;
	
	private DocumentBuilderFactory dbf;
	private DocumentBuilder db;
	private Document inputDoc;
	private Document outputDoc;
	
	private int width;
	private int height;
	
	private DefaultListModel<BufferedAnimation> animationListModel = new DefaultListModel<BufferedAnimation>();
	private JList<BufferedAnimation> animationList = new JList<BufferedAnimation>(animationListModel);
	private DefaultListModel<BufferedFrame> frameListModel = new DefaultListModel<BufferedFrame>();
	private JList<BufferedFrame> frameList = new JList<BufferedFrame>(frameListModel);
	
	private String[] infoNameList = {"name", "frameNum", "frameFile", "rowSize", "colSize", "scaleRate"};
	private String[] animationTypeList = {"role", "monster", "skill"};
	private String[] frameInputNameList = {"row: ", "col: "};
	private String[] smartInputNameList = {"Image W: ", "Image H: "};
	
	private JPanel displayPanel = Drawer.drawBackGround(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), DISPLAY_SIZE, DISPLAY_SIZE));
	private JPanel displayControlPanel = new JPanel();
	private JScrollPane animationListPanel = new JScrollPane(animationList);
	private JScrollPane frameListPanel = new JScrollPane(frameList);
	private JPanel frameControlPanel = new JPanel();
	private JPanel infoPanel = new JPanel();
	private JPanel imagePanel = Drawer.drawBackGround(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), DISPLAY_SIZE * 2, DISPLAY_SIZE * 3));
	private JPanel frameInputPanel = new JPanel();
	private JPanel smartPanel = new JPanel();
	
	private JLabel displayLabel = new JLabel();
	private JLabel imageLabel = new JLabel();
	
	private JButton playButton = new JButton("Play");
	private JButton stopButton = new JButton("Stop");
	private JButton saveAnimationButton = new JButton("Save Animation");
	private JButton deleteAnimationButton = new JButton("Delete Animaion");
	private JButton insertButton = new JButton("Insert");
	private JButton deleteButton = new JButton("Delete");
	private JButton createButton = new JButton("Create animation");
	private JButton smartButton = new JButton("Smart Insert");
	
	private JButton saveAllButton = new JButton("Save All change");
	
	private JComboBox<String> animationTypeBox = new JComboBox<String>(animationTypeList);
	private JCheckBox directionBox = new JCheckBox("dir<==>");
	
	private ArrayList<JLabel> infoLabelList = new ArrayList<JLabel>();
	private ArrayList<JTextField> infoInputList = new ArrayList<JTextField>();
	
	private ArrayList<JLabel> frameLabelList = new ArrayList<JLabel>();
	private ArrayList<JTextField> frameInputList = new ArrayList<JTextField>();
	
	private ArrayList<JLabel> smartLabelList = new ArrayList<JLabel>();
	private ArrayList<JTextField> smartInputList = new ArrayList<JTextField>();
		
	private boolean isKilled;
	private int frameIndex;
		
	private ResourceEditor caller;
	
	public AnimationEditor(ResourceEditor caller){
		this.caller = caller;
		initialLayout();
	}
	
	private void initialLayout(){
		width = caller.getWidth();
		height = caller.getHeight();
		this.setLayout(null);
		setAllPanel();
		initialDisplayPanel();
		initialDisplayControlPanel();
		initialFrameControlPanel();
		initialInfoPanel();
		initialImagePanel();
		initialFrameInputPanel();
		initialSmartPanel();
		initialAnimationListPanel();
		initialFrameListPanel();
	}

	private void setAllPanel(){
		displayPanel.setBounds(75, 70, DISPLAY_SIZE, DISPLAY_SIZE);
		this.add(displayPanel);
		
		displayControlPanel.setBounds(width/4, 80, 120, 50);
		this.add(displayControlPanel);
		
		animationListPanel.setBounds(width*4/10, height/2 - 50, width/5, height/3 + 50);
		this.add(animationListPanel);
		
		frameListPanel.setBounds(70, height/2 - 50, width/5, height/3 + 50);
		this.add(frameListPanel);
		
		frameControlPanel.setBounds(width*3/10 - 30, height*2/3 - 30, 140, 100);
		this.add(frameControlPanel);
		
		infoPanel.setBounds(width*4/10, 50, width * 2/10 -10, 200);
		this.add(infoPanel);
		
		imagePanel.setBounds(width*2/3, 100, DISPLAY_SIZE * 2, DISPLAY_SIZE * 3);
		this.add(imagePanel);
		
		frameInputPanel.setBounds(width*3/10 - 20, height*2/3 - 100, 130, 50);
		this.add(frameInputPanel);
		
		smartPanel.setBounds(width*3/10 - 30, height*2/3 - 200, 150, 100);
		this.add(smartPanel);
		
		saveAllButton.setBounds(width/4 - 20, 200, 150, 80);
		saveAllButton.addActionListener(this);
		this.add(saveAllButton);
	}
	
	private void initialDisplayPanel(){
		displayPanel.setLayout(null);
		displayLabel.setBounds(10, 10, DISPLAY_SIZE - 20, DISPLAY_SIZE - 20);
		displayPanel.add(displayLabel);
	}
	
	private void initialDisplayControlPanel(){
		displayControlPanel.setLayout(new GridLayout(2, 1));
		displayControlPanel.add(playButton);
		displayControlPanel.add(stopButton);
		playButton.addActionListener(this);
		stopButton.addActionListener(this);
	}
	
	private void initialAnimationListPanel(){
		parseXml();
		refreshByAnimation(animationListModel.get(0));
		animationList.addListSelectionListener(this);
	}
	
	private void initialFrameListPanel(){
		frameList.addListSelectionListener(this);
	}
	
	private void initialFrameControlPanel(){
		frameControlPanel.setLayout(new GridLayout(6, 1));
		frameControlPanel.add(insertButton);
		frameControlPanel.add(deleteButton);
		frameControlPanel.add(createButton);
		frameControlPanel.add(saveAnimationButton);
		frameControlPanel.add(deleteAnimationButton);
		frameControlPanel.add(smartButton);
		insertButton.addActionListener(this);
		deleteButton.addActionListener(this);
		createButton.addActionListener(this);
		saveAnimationButton.addActionListener(this);
		deleteAnimationButton.addActionListener(this);
		smartButton.addActionListener(this);
	}
	
	private void initialInfoPanel(){
		infoPanel.setLayout(new GridLayout(infoNameList.length + 1, 2));
		
		for(int i=0; i<infoNameList.length; i++){
			JLabel label = new JLabel(infoNameList[i]);
			infoLabelList.add(label);
			infoPanel.add(label);
			
			JTextField textField = new JTextField();
			if(infoNameList[i].equals("frameNum") || infoNameList[i].equals("frameFile"))
				textField.setEditable(false);
			infoInputList.add(textField);
			infoPanel.add(textField);
		}
		infoPanel.add(animationTypeBox);
	}
	
	private void initialImagePanel(){
		imagePanel.setLayout(null);
		imageLabel.setBounds(25, 50, DISPLAY_SIZE * 2 -50, DISPLAY_SIZE * 3 - 100);
		imagePanel.add(imageLabel);
	}
	
	private void initialFrameInputPanel(){
		frameInputPanel.setLayout(new GridLayout(2, 2));
		
		for(int i=0; i<frameInputNameList.length; i++){
			JLabel label = new JLabel(frameInputNameList[i]);
			frameLabelList.add(label);
			frameInputPanel.add(label);
			
			JTextField textField = new JTextField();
			frameInputList.add(textField);
			frameInputPanel.add(textField);
		}
	}
	
	private void initialSmartPanel(){
		smartPanel.setLayout(new GridLayout(3, 2));
		
		for(int i=0; i<smartInputNameList.length; i++){
			JLabel label = new JLabel(smartInputNameList[i]);
			smartLabelList.add(label);
			smartPanel.add(label);
			
			JTextField textField = new JTextField();
			textField.setEditable(false);
			smartInputList.add(textField);
			smartPanel.add(textField);
		}
		smartPanel.add(directionBox);
	}
	
	private void initialParser(){
		try{
			File file = new File("xml/animation.xml");
			dbf = DocumentBuilderFactory.newInstance();
			db = dbf.newDocumentBuilder();
			inputDoc = db.parse(file);
			inputDoc.getDocumentElement().normalize();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void parseXml(){
		initialParser();
		Element root = inputDoc.getDocumentElement();
		
		for(int i=0; i<animationTypeList.length; i++){
			NodeList list = ((Element)root.getElementsByTagName(animationTypeList[i]).item(0)).getElementsByTagName("animation");
			String type = animationTypeList[i];
			for(int j=0; j<list.getLength(); j++){
				String name = ((Element)list.item(j)).getElementsByTagName("name").item(0).getTextContent();
				int frameNum = Integer.parseInt(((Element)list.item(j)).getElementsByTagName("frameNum").item(0).getTextContent());
				String frameFile = ((Element)list.item(j)).getElementsByTagName("frameFile").item(0).getTextContent();
				int rowSize = Integer.parseInt(((Element)list.item(j)).getElementsByTagName("rowSize").item(0).getTextContent());
				int colSize = Integer.parseInt(((Element)list.item(j)).getElementsByTagName("colSize").item(0).getTextContent());
				double scaleRate = Double.parseDouble(((Element)list.item(j)).getElementsByTagName("scale").item(0).getTextContent());
				LinkedList<BufferedFrame> tmpFrameList = new LinkedList<BufferedFrame>();
				try{
					BufferedImage image = ImageIO.read(new FileInputStream(new File(frameFile)));
					for(int k=0; k<frameNum; k++){
						Element element = (Element)((Element)list.item(j)).getElementsByTagName("frame").item(k);
						int row = Integer.parseInt(element.getElementsByTagName("row").item(0).getTextContent());
						int col = Integer.parseInt(element.getElementsByTagName("col").item(0).getTextContent());
						Image subImage = image.getSubimage(colSize * (col-1), rowSize * (row-1), colSize, rowSize);
						BufferedFrame frame = new BufferedFrame(subImage, row, col);
						tmpFrameList.add(frame);
					}
					BufferedAnimation animation;
					animation = new BufferedAnimation(type, name, frameNum, rowSize, colSize, scaleRate, frameFile, tmpFrameList);
					animationListModel.addElement(animation);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}	
		}
		//NodeList nodeList = doc.getElementsByTagName("role");
		//doc.appendChild(nodeList.item(0));
		
		
		// try{
			// TransformerFactory transformerFactory = TransformerFactory.newInstance();
			// Transformer transformer = transformerFactory.newTransformer();
			// DOMSource source = new DOMSource(doc);
			// StreamResult result = new StreamResult(System.out);
			// transformer.transform(source, result);
			// System.out.println("Hi");
		// }catch(Exception ex){
			// ex.printStackTrace();
		// }
		
	}
	
	private void refreshByAnimation(BufferedAnimation animation){
		frameListModel.clear();
		
		for(int i=0; i<animation.getFrameList().size(); i++)
			frameListModel.addElement(animation.getFrameList().get(i));
			
		for(int i=0; i<infoLabelList.size(); i++){
			if(infoLabelList.get(i).getText().equals("name"))
				infoInputList.get(i).setText(animation.getName());
			else if(infoLabelList.get(i).getText().equals("frameNum"))
				infoInputList.get(i).setText(animation.getFrameNum() + "");
			else if(infoLabelList.get(i).getText().equals("frameFile"))
				infoInputList.get(i).setText(animation.getFrameFile());
			else if(infoLabelList.get(i).getText().equals("rowSize"))
				infoInputList.get(i).setText(animation.getRowSize() + "");
			else if(infoLabelList.get(i).getText().equals("colSize"))
				infoInputList.get(i).setText(animation.getColSize() + "");
			else if(infoLabelList.get(i).getText().equals("scaleRate"))
				infoInputList.get(i).setText(animation.getScaleRate() + "");
		}
		for(int i=0; i<animationTypeList.length; i++)
			if(animation.getType().equals(animationTypeList[i]))
				animationTypeBox.setSelectedIndex(i);
		
		try{
			BufferedImage img = ImageIO.read(new FileInputStream(new File(animation.getFrameFile())));
			smartInputList.get(0).setText(img.getWidth() + "");
			smartInputList.get(1).setText(img.getHeight() + "");
			double rate = (((double)DISPLAY_SIZE * 2 - 50) / (double)img.getWidth()) < (((double)DISPLAY_SIZE * 3 - 100) / (double)img.getHeight()) ? (((double)DISPLAY_SIZE * 2 - 50) / (double)img.getWidth()) : (((double)DISPLAY_SIZE * 3 - 100) / (double)img.getHeight());
			imageLabel.setIcon(new ImageIcon(Drawer.getResizedImg(img, (int)(img.getWidth() * rate), (int)(img.getHeight() * rate))));
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private void refreshByFrame(BufferedFrame frame){
		displayLabel.setIcon(new ImageIcon(Drawer.getResizedImg(frame.getImage(), DISPLAY_SIZE - 20, DISPLAY_SIZE - 20)));
	}
	
	public String getRelativePath(File file, File folder){
		String filePath = file.getAbsolutePath();
		String folderPath = folder.getAbsolutePath();
		if(filePath.startsWith(folderPath))
			return filePath.substring(folderPath.length() + 1);
		else
			return null;
	}
	
	private void createNewAnimation(){
		try{
			JFileChooser fileChooser = new JFileChooser();
			int returnValue = fileChooser.showOpenDialog(this);
			if(returnValue == JFileChooser.APPROVE_OPTION){
				BufferedAnimation animation = makeAnimation();
				animation.setFrameFile(getRelativePath(fileChooser.getSelectedFile(), new File(System.getProperty("user.dir"))));
				animation.setFrameNum(0);
				animation.setFrameList(new LinkedList<BufferedFrame>());
				
				for(int i=0; i<animationListModel.getSize(); i++){
					if(i == animationListModel.getSize()-1){
						animationListModel.addElement(animation);
						animationList.setSelectedIndex(animationListModel.getSize()-1);
						break;
					}
					if(animationListModel.get(i).getType().equals(animation.getType())
					&& !animationListModel.get(i+1).getType().equals(animation.getType())){
						animationListModel.insertElementAt(animation, i+1);
						animationList.setSelectedIndex(i+1);
						break;
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private BufferedAnimation makeAnimation(){
		BufferedAnimation animation = new BufferedAnimation();
		animation.setType(animationTypeList[animationTypeBox.getSelectedIndex()]);
		
		for(int i=0; i<infoLabelList.size(); i++){
			if(infoLabelList.get(i).getText().equals("name"))
				animation.setName(infoInputList.get(i).getText());
			else if(infoLabelList.get(i).getText().equals("frameFile"))
				animation.setFrameFile(infoInputList.get(i).getText());
			else if(infoLabelList.get(i).getText().equals("frameNum"))
				animation.setFrameNum(Integer.parseInt(infoInputList.get(i).getText()));
			else if(infoLabelList.get(i).getText().equals("rowSize"))
				animation.setRowSize(Integer.parseInt(infoInputList.get(i).getText()));
			else if(infoLabelList.get(i).getText().equals("colSize"))
				animation.setColSize(Integer.parseInt(infoInputList.get(i).getText()));
			else if(infoLabelList.get(i).getText().equals("scaleRate"))
				animation.setScaleRate(Double.parseDouble(infoInputList.get(i).getText()));
		}
		
		LinkedList<BufferedFrame> tmpFrameList = new LinkedList<BufferedFrame>();
		for(int i=0; i<frameListModel.getSize(); i++)
			tmpFrameList.add(frameListModel.get(i));
		animation.setFrameList(tmpFrameList);
		return animation;
	}
	
	private String getFromInfo(String target){
		for(int i=0; i<infoLabelList.size(); i++)
			if(infoLabelList.get(i).getText().equals(target))
				return infoInputList.get(i).getText();
		return null;
	}
	
	private void setToInfo(String target, String value){
		for(int i=0; i<infoLabelList.size(); i++)
			if(infoLabelList.get(i).getText().equals(target))
				infoInputList.get(i).setText(value);
	}
	
	private void insertNewFrame(){
		int index = frameList.getSelectedIndex();
		BufferedFrame frame = new BufferedFrame();
		frame.setRow(Integer.parseInt(frameInputList.get(0).getText()));
		frame.setCol(Integer.parseInt(frameInputList.get(1).getText()));
		String fileName = animationListModel.get(animationList.getSelectedIndex()).getFrameFile();
		try{
			BufferedImage image = ImageIO.read(new FileInputStream(new File(fileName)));			
			int rowSize = Integer.parseInt(getFromInfo("rowSize"));
			int colSize = Integer.parseInt(getFromInfo("colSize"));
			System.out.println("Image: " + image.getWidth() + ", " + image.getHeight());
			System.out.println("Subimg: " + (frame.getCol()-1) * colSize + ", " + (frame.getRow()-1) * rowSize + ", " +  rowSize + ", " + colSize);
			frame.setImage(image.getSubimage((frame.getCol()-1) * colSize, (frame.getRow()-1) * rowSize, colSize, rowSize));
			if(frameList.getSelectedIndex() < 0)
				frameListModel.addElement(frame);
			else
				frameListModel.insertElementAt(frame, frameList.getSelectedIndex());
			animationListModel.get(animationList.getSelectedIndex()).setFrameNum(animationListModel.get(animationList.getSelectedIndex()).getFrameNum()+1);
			setToInfo("frameNum", (Integer.parseInt(getFromInfo("frameNum")) + 1) + "");
			frameList.setSelectedIndex(index);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private void smartInsertNewFrame(){
		int rowSize = Integer.parseInt(getFromInfo("rowSize"));
		int colSize = Integer.parseInt(getFromInfo("colSize"));
		int rowNum = Integer.parseInt(smartInputList.get(1).getText()) / rowSize;
		int colNum = Integer.parseInt(smartInputList.get(0).getText()) / colSize;
		String fileName = animationListModel.get(animationList.getSelectedIndex()).getFrameFile();
		try{
			BufferedImage image = ImageIO.read(new FileInputStream(new File(fileName)));
			System.out.println("ROWNUM: " + rowNum + "COLNUM" + colNum);
			if(directionBox.isSelected()){
				for(int i=0; i<rowNum; i++){
					for(int j=0; j<colNum; j++){
						BufferedFrame frame = new BufferedFrame();
						frame.setRow(i+1);
						frame.setCol(j+1);
						System.out.println("Image: " + image.getWidth() + ", " + image.getHeight());
						System.out.println("Subimg: " + (frame.getCol()-1) * colSize + ", " + (frame.getRow()-1) * rowSize + ", " +  rowSize + ", " + colSize);
						frame.setImage(image.getSubimage((frame.getCol()-1) * colSize, (frame.getRow()-1) * rowSize, colSize, rowSize));
						frameListModel.addElement(frame);
						animationListModel.get(animationList.getSelectedIndex()).setFrameNum(animationListModel.get(animationList.getSelectedIndex()).getFrameNum()+1);
						setToInfo("frameNum", (Integer.parseInt(getFromInfo("frameNum")) + 1) + "");
					}
				}
			}
			else{
				for(int j=0; j<colNum; j++){
					for(int i=0; i<rowNum; i++){
						BufferedFrame frame = new BufferedFrame();
						frame.setRow(i+1);
						frame.setCol(j+1);
						frame.setImage(image.getSubimage((frame.getCol()-1) * colSize, (frame.getRow()-1) * rowSize, colSize, rowSize));
						frameListModel.addElement(frame);
						animationListModel.get(animationList.getSelectedIndex()).setFrameNum(animationListModel.get(animationList.getSelectedIndex()).getFrameNum()+1);
						setToInfo("frameNum", (Integer.parseInt(getFromInfo("frameNum")) + 1) + "");
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private void deleteFrame(){
		if(frameList.getSelectedIndex() < 0)
			return;
		boolean isLast;
		int index = frameList.getSelectedIndex();
		if(index == frameListModel.getSize()-1)
			isLast = true;
		else
			isLast = false;
		frameListModel.removeElementAt(index);
		animationListModel.get(animationList.getSelectedIndex()).setFrameNum(animationListModel.get(animationList.getSelectedIndex()).getFrameNum()-1);
		for(int i=0; i<infoLabelList.size(); i++)
			if(infoLabelList.get(i).getText().equals("frameNum"))
				infoInputList.get(i).setText(animationListModel.get(animationList.getSelectedIndex()).getFrameNum() + "");
		if(isLast)
			frameList.setSelectedIndex(index-1);
		else
			frameList.setSelectedIndex(index);
	}
	
	private void saveAnimation(){
		if(animationList.getSelectedIndex() < 0)
			return;
		BufferedAnimation animation = makeAnimation();
		animationListModel.setElementAt(animation, animationList.getSelectedIndex());
	}
	
	private void deleteAnimation(){
		if(animationList.getSelectedIndex() < 0)
			return;
		boolean isLast;
		int index = animationList.getSelectedIndex();
		if(index == animationListModel.getSize()-1)
			isLast = true;
		else
			isLast = false;
		animationListModel.removeElementAt(animationList.getSelectedIndex());
		if(isLast)
			animationList.setSelectedIndex(index-1);
		else
			animationList.setSelectedIndex(index);
	}
	
	private void saveAll(){
		outputDoc = db.newDocument();
		Element root = outputDoc.createElement("root");
		outputDoc.appendChild(root);
		ArrayList<Element> typeList = new ArrayList<Element>();
		for(int i=0; i<animationTypeList.length; i++){
			Element type = outputDoc.createElement(animationTypeList[i]);
			root.appendChild(type);
			typeList.add(type);
		}
		
		for(int i=0; i<animationListModel.getSize(); i++){
			Element typeElement = typeList.get(0);
			BufferedAnimation animation = animationListModel.get(i);
			
			for(int j=0; j<animationTypeList.length; j++)
				if(animation.getType().equals(animationTypeList[j]))
					typeElement = typeList.get(j);
			
			Element animationElement = outputDoc.createElement("animation");
			animationElement.setAttribute("id", animation.getType() + i);
			typeElement.appendChild(animationElement);
			
			Element nameElement = outputDoc.createElement("name");
			Text tName = outputDoc.createTextNode(animation.getName());
			nameElement.appendChild(tName);
			animationElement.appendChild(nameElement);
			
			Element frameNumElement = outputDoc.createElement("frameNum");
			Text tFrameNum = outputDoc.createTextNode(animation.getFrameNum() + "");
			frameNumElement.appendChild(tFrameNum);
			animationElement.appendChild(frameNumElement);
			
			Element frameFileElement = outputDoc.createElement("frameFile");
			Text tFrameFile = outputDoc.createTextNode(animation.getFrameFile());
			frameFileElement.appendChild(tFrameFile);
			animationElement.appendChild(frameFileElement);
			
			Element rowSizeElement = outputDoc.createElement("rowSize");
			Text tRowSize = outputDoc.createTextNode(animation.getRowSize() + "");
			rowSizeElement.appendChild(tRowSize);
			animationElement.appendChild(rowSizeElement);
			
			Element colSizeElement = outputDoc.createElement("colSize");
			Text tColSize = outputDoc.createTextNode(animation.getColSize() + "");
			colSizeElement.appendChild(tColSize);
			animationElement.appendChild(colSizeElement);
			
			Element scaleRateElement = outputDoc.createElement("scale");
			Text tScaleRate = outputDoc.createTextNode((int)(animation.getScaleRate()) + "");
			scaleRateElement.appendChild(tScaleRate);
			animationElement.appendChild(scaleRateElement);
			
			for(int j=0; j<animation.getFrameList().size(); j++){
				BufferedFrame frame = animation.getFrameList().get(j);
				Element frameElement = outputDoc.createElement("frame");
				frameElement.setAttribute("id", j + "");
				animationElement.appendChild(frameElement);
				
				Element rowElement = outputDoc.createElement("row");
				Text tRow = outputDoc.createTextNode(frame.getRow() + "");
				rowElement.appendChild(tRow);
				frameElement.appendChild(rowElement);
				
				Element colElement = outputDoc.createElement("col");
				Text tCol = outputDoc.createTextNode(frame.getCol() + "");
				colElement.appendChild(tCol);
				frameElement.appendChild(colElement);
			}
		}
		
		try{
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			DOMSource source = new DOMSource(outputDoc);
			StreamResult result = new StreamResult(new File("xml/animation.xml"));
			transformer.transform(source, result);
			System.out.println("Hi");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void valueChanged(ListSelectionEvent e){
		if(e.getValueIsAdjusting())
			return;
		int index = ((JList)e.getSource()).getSelectedIndex();
		if(index < 0)
			return;
		if(e.getSource() == animationList){
			BufferedAnimation animation = animationListModel.get(index);
			refreshByAnimation(animation);
			frameList.setSelectedIndex(0);
		}
		else if(e.getSource() == frameList){
			if(index < 0 || index >= frameListModel.getSize())
				return;
			BufferedFrame frame = frameListModel.get(index);
			refreshByFrame(frame);
		}
	}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == playButton){
			System.out.println(123);
			if(animationList.getSelectedIndex() < 0)
				return;
			Thread thr = new Thread(this);
			thr.start();
		}
		else if(e.getSource() == stopButton)
			stop();
		else if(e.getSource() == createButton)
			createNewAnimation();
		else if(e.getSource() == insertButton)
			insertNewFrame();
		else if(e.getSource() == deleteButton)
			deleteFrame();
		else if(e.getSource() == saveAnimationButton)
			saveAnimation();
		else if(e.getSource() == deleteAnimationButton)
			deleteAnimation();
		else if(e.getSource() == smartButton)
			smartInsertNewFrame();
		else if(e.getSource() == saveAllButton)
			saveAll();
	}
	
	public void run(){
		frameIndex = 0;
		BufferedAnimation animation = animationListModel.get(animationList.getSelectedIndex());
		
		isKilled = false;
		while(!isKilled){
			displayLabel.setIcon(new ImageIcon(Drawer.getResizedImg(frameListModel.get(frameIndex).getImage(), DISPLAY_SIZE - 20, DISPLAY_SIZE - 20)));
			frameIndex = (frameIndex + 1) % frameListModel.getSize();
			
			try{
				Thread.sleep(100);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		frameIndex = 0;
	}
	
	public void stop(){
		isKilled = true;
	}
}

class BufferedFrame{
	private Image image;
	private int row;
	private int col;
	
	public BufferedFrame(){
	}
	
	public BufferedFrame(Image image, int row, int col){
		this.image = image;
		this.row = row;
		this.col = col;
	}
	
	public Image getImage(){
		return image;
	}
	
	public int getRow(){
		return row;
	}
	
	public int getCol(){
		return col;
	}
	
	public void setImage(Image image){
		this.image = image;
	}
	
	public void setRow(int row){
		this.row = row;
	}
	
	public void setCol(int col){
		this.col = col;
	}
	
	public String toString(){
		return new String("<row: " + row + "> <col: " + col + ">");
	}
}

class BufferedAnimation{
	private String type;
	private String name;
	private int frameNum;
	private int rowSize;
	private int colSize;
	private double scaleRate;
	private String frameFile;
	private LinkedList<BufferedFrame> frameList;
	
	public BufferedAnimation(){
	}
	
	public BufferedAnimation(String type, String name, int frameNum, int rowSize, int colSize, double scaleRate, String frameFile, LinkedList<BufferedFrame> frameList){
		this.type = type;
		this.name = name;
		this.frameNum = frameNum;
		this.rowSize = rowSize;
		this.colSize = colSize;
		this.scaleRate = scaleRate;
		this.frameFile = frameFile;
		this.frameList = frameList;
	}
	
	public String getType(){
		return type;
	}
	
	public String getName(){
		return name;
	}
	
	public int getFrameNum(){
		return frameNum;
	}
	
	public int getRowSize(){
		return rowSize;
	}
	
	public int getColSize(){
		return colSize;
	}
	
	public double getScaleRate(){
		return scaleRate;
	}
	
	public String getFrameFile(){
		return frameFile;
	}
	
	public void setType(String type){
		this.type = type;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setFrameNum(int frameNum){
		this.frameNum = frameNum;
	}
	
	public void setRowSize(int rowSize){
		this.rowSize = rowSize;
	}
	
	public void setColSize(int colSize){
		this.colSize = colSize;
	}
	
	public void setScaleRate(double scaleRate){
		this.scaleRate = scaleRate;
	}
	
	public void setFrameFile(String frameFile){
		this.frameFile = frameFile;
	}
	
	public void setFrameList(LinkedList<BufferedFrame> frameList){
		this.frameList = frameList;
	}
	
	public LinkedList<BufferedFrame> getFrameList(){
		return frameList;
	}
	
	public String toString(){
		return new String("<" + type+ "> " + name + "(" + frameNum + ")");
	}
}