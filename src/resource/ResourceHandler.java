package rpg.resource;

import java.lang.*;
import java.util.ArrayList;
import java.io.*;
import java.awt.image.*;
import javax.imageio.*;
import javax.xml.parsers.*;
import javax.swing.*;
import org.w3c.dom.*;
import rpg.draw.*;

import rpg.map.*;
import rpg.character.*;
import rpg.control.*;
import rpg.item.*;
/**
 * A class that maps the resource_name(e.g. music, image) to the real file names using xml
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class ResourceHandler{
	private static String[] typeNameList = {"music", "sound", "image", "map", "npc", "episode", "item", "role", "skill", "monster", "animation"};
	private static String[] fileNameList = {"xml/music.xml", "xml/sound.xml", 
											"xml/image.xml", "xml/map.xml", 
											"xml/npc.xml", "xml/episode.xml", 
											"xml/item.xml", "xml/role.xml",
											"xml/skill.xml", "xml/monster.xml",
											"xml/animation.xml"};
	private static Document[] docList = new Document[typeNameList.length];
	private static final int FRAME_WIDTH = 32;
	private static final int FRAME_HEIGHT = 48;
	
	static{
		for(int i=0; i<typeNameList.length; i++)
			initialDoc(i);
	}
	
	public static void initialDoc(int index){
		try{
			File file = new File(fileNameList[index]);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			docList[index] = db.parse(file);
			docList[index].getDocumentElement().normalize();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private static int getTypeIndex(String type){
		for(int i=0; i<typeNameList.length; i++)
			if(typeNameList[i].equals(type))
				return i;
		return -1;
	}
	
	public static String getResourcePath(String resType, String resName){
		int index = getTypeIndex(resType);
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName(resType);
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			Element element = (Element)node;
			if(element.getElementsByTagName("srcName").item(0).getTextContent().equals(resName))
				return element.getElementsByTagName("filePath").item(0).getTextContent();
		}
		return null;
	}
	
	public static BufferedImage[] getImageListByName(String resName){
		int index = getTypeIndex("image");
		if(index < 0)
			return null;
		NodeList nodeList = ((Element)docList[index].getElementsByTagName("list").
								item(0)).getElementsByTagName("image");
		try{
			for(int i=0; i<nodeList.getLength(); i++){
				Node node = nodeList.item(i);
				Element element = (Element)node;
				if(element.getElementsByTagName("srcName").item(0).getTextContent().equals(resName)){
					int len = element.getElementsByTagName("filePath").getLength();
					BufferedImage[] imageList = new BufferedImage[len];
					for(int j=0; j<len; j++){
						String fileName = element.getElementsByTagName("filePath").item(j).getTextContent();
						imageList[j] = ImageIO.read(new FileInputStream(new File(fileName)));
					}
					return imageList;
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public static ImageIcon[] getIconListByName(String resName){
		BufferedImage[] imageList = getImageListByName(resName);
		if(imageList == null)
			return null;
		ImageIcon[] iconList = new ImageIcon[imageList.length];
		for(int i=0; i<imageList.length; i++)
			iconList[i] = new ImageIcon(imageList[i]);
		return iconList;
	}
	
	public static BufferedImage getImageByName(String group, String resName){
		int index = getTypeIndex("image");
		if(index < 0)
			return null;
		NodeList nodeList = ((Element)docList[index].getElementsByTagName(group).
								item(0)).getElementsByTagName("image");
		try{
			for(int i=0; i<nodeList.getLength(); i++){
				Node node = nodeList.item(i);
				Element element = (Element)node;
				if(element.getElementsByTagName("srcName").item(0).getTextContent().equals(resName)){
					String fileName = element.getElementsByTagName("filePath").item(0).getTextContent();
					BufferedImage srcImage = ImageIO.read(new FileInputStream(new File(fileName)));
					if(element.getElementsByTagName("isSub").item(0).getTextContent().equals("false"))
						return srcImage;
					else{
						int rowInt = Integer.parseInt(element.getElementsByTagName("rowInt").item(0).getTextContent());
						int colInt = Integer.parseInt(element.getElementsByTagName("colInt").item(0).getTextContent());
						int row = Integer.parseInt(element.getElementsByTagName("row").item(0).getTextContent());
						int col = Integer.parseInt(element.getElementsByTagName("col").item(0).getTextContent());
						return srcImage.getSubimage(colInt * (col-1), rowInt * (row-1), colInt, rowInt);
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public static ImageIcon getIconByName(String group, String resName){
		BufferedImage img = getImageByName(group, resName);
		if(img == null)
			return null;
		return new ImageIcon(img);
	}
	
	public static ArrayList<ImageIcon> getNPCIconById(String npcId){
		ArrayList<ImageIcon> iconList = new ArrayList<ImageIcon>();
		int index = getTypeIndex("image");
		if(index < 0)
			return null;
		NodeList nodeList = ((Element)docList[index].getElementsByTagName("npc").item(0))
							.getElementsByTagName("image");
		try{
			for(int i=0; i<nodeList.getLength(); i++){
				Node node = nodeList.item(i);
				Element element = (Element)node;
				if(element.getAttribute("id").equals(npcId)){
					String fileName = element.getElementsByTagName("filePath").item(0).getTextContent();
					BufferedImage srcImg = ImageIO.read(new FileInputStream(new File(fileName)));
					if(element.getElementsByTagName("isSub").item(0).getTextContent().equals("false"))
						iconList.add(new ImageIcon(srcImg));
					else{
						int rowInt = Integer.parseInt(element.getElementsByTagName("rowInt").item(0).getTextContent());
						int colInt = Integer.parseInt(element.getElementsByTagName("colInt").item(0).getTextContent());
						int row = Integer.parseInt(element.getElementsByTagName("row").item(0).getTextContent());
						int col = Integer.parseInt(element.getElementsByTagName("col").item(0).getTextContent());
						srcImg = srcImg.getSubimage(colInt * (col-1), rowInt * (row-1), colInt, rowInt);
					}
					iconList.add(new ImageIcon(srcImg));
				}
			}
			return iconList;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public static Map getMapById(String resId){
		int index = getTypeIndex("map");
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName("map");
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			Element element = (Element)node;
			if(element.getAttribute("id").equals(resId)){
				try{
					File file = new File(element.getElementsByTagName("filePath").item(0).getTextContent());
					ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
					Map map = (Map)ois.readObject();
					ois.close();
					return map;
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}
		return null;
	}
	
	public static Map getMapByName(String resName){
		int index = getTypeIndex("map");
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName("map");
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			Element element = (Element)node;
			if(element.getElementsByTagName("srcName").item(0).getTextContent().equals(resName)){
				try{
					File file = new File(element.getElementsByTagName("filePath").item(0).getTextContent());
					ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
					Map map = (Map)ois.readObject();
					ois.close();
					return map;
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}
		return null;
	}
	
	public static ArrayList<Map> getMaps(){
		int index = getTypeIndex("map");
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName("map");
		
		ArrayList<Map> returnList = new ArrayList<Map>();
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			Element element = (Element)node;
			try{
				File file = new File(element.getElementsByTagName("filePath").item(0).getTextContent());
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
				Map map = (Map)ois.readObject();
				ois.close();
				returnList.add(map);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		return returnList;
	}

	public static int getResourceAmount(String resType){
		int index = getTypeIndex(resType);
		if(index < 0)
			return -1;
		NodeList nodeList = docList[index].getElementsByTagName(resType);
		return nodeList.getLength();
	}
	
	public static int[] getMapIdList(){
		int index = getTypeIndex("map");
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName("map");
		int[] returnList = new int[nodeList.getLength()];
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			Element element = (Element)node;
			returnList[i] = Integer.parseInt(element.getAttribute("id"));	
		}
		return returnList;
	}
	
	public static int getNPCAmountByMapId(String mapId){
		int index = getTypeIndex("npc");
		if(index < 0)
			return -1;
		NodeList nodeList = docList[index].getElementsByTagName("map");
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			Element element = (Element)node;
			if(element.getAttribute("id").equals(mapId))
				return element.getElementsByTagName("npc").getLength();
		}
		return -1;
	}
	
	public static NPC getNPC(String mapId, String npcId){
		int index = getTypeIndex("npc");
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName("map");
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element)node;
				if(element.getAttribute("id").equals(mapId)){
					NodeList npcList = element.getElementsByTagName("npc");
					for(int j=0; j<npcList.getLength(); j++){
						Node npc = npcList.item(j);
						if(npc.getNodeType() == Node.ELEMENT_NODE){
							Element npcElement = (Element)npc;
							if(npcElement.getAttribute("id").equals(npcId)){
								String name = npcElement.getElementsByTagName("name").item(0).getTextContent();
								ArrayList<ImageIcon> imageList = getNPCIconById(npcElement.getElementsByTagName("imageId").item(0).getTextContent());
								ImageIcon portrait = imageList.get(4);
								ArrayList<String> sellList = null;
								if(npcElement.getElementsByTagName("isMerchant").item(0).getTextContent().equals("true")){
									sellList = new ArrayList<String>();
									NodeList list = npcElement.getElementsByTagName("sell");
									for(int k=0; k<list.getLength(); k++)
										sellList.add(list.item(k).getTextContent());
								}
								boolean isBoss;
								if(npcElement.getElementsByTagName("isBoss").item(0).getTextContent().equals("true"))
									isBoss = true;
								else
									isBoss = false;
								int row = Integer.parseInt(npcElement.getElementsByTagName("row").item(0).getTextContent());
								int col = Integer.parseInt(npcElement.getElementsByTagName("col").item(0).getTextContent());
								int range = Integer.parseInt(npcElement.getElementsByTagName("range").item(0).getTextContent());
								int dialogueNum = npcElement.getElementsByTagName("dialogue").getLength();
								Dialogue[] dialogueList = new Dialogue[dialogueNum];
								for(int k=0; k<dialogueNum; k++){
									Node innerNode = npcElement.getElementsByTagName("dialogue").item(k);
									if(innerNode.getNodeType() == Node.ELEMENT_NODE){
										Element innerElement = (Element)innerNode;
										int startEpisode = Integer.parseInt(innerElement.getAttribute("start").split("-")[0]);
										int startSubEpisode = Integer.parseInt(innerElement.getAttribute("start").split("-")[1]);
										int endEpisode = Integer.parseInt(innerElement.getAttribute("end").split("-")[0]);
										int endSubEpisode = Integer.parseInt(innerElement.getAttribute("end").split("-")[1]);
										String content = npcElement.getElementsByTagName("dialogue").item(k).getTextContent();
										Dialogue dialogue = new Dialogue(startEpisode, startSubEpisode, endEpisode, endSubEpisode, content);
										dialogueList[k] = dialogue;
									}									
								}
								NPC returnNPC = new NPC(name, imageList, portrait, row, col, dialogueList, range, sellList,isBoss);
								if(isBoss){
									int battleId = Integer.parseInt(npcElement.getElementsByTagName("battleEvent").item(0).getTextContent());
									int battleSubId = Integer.parseInt(npcElement.getElementsByTagName("battleEventSub").item(0).getTextContent());
									returnNPC.setBattleEvent(battleId, battleSubId);
								}
								return returnNPC;
							}
						}
					}
					
				}
			}
		}
		return null;
	}
	
	public static ArrayList<NPC> getNPCs(){
		int index = getTypeIndex("npc");
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName("map");
		
		ArrayList<NPC> returnList = new ArrayList<NPC>();
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			Element element = (Element)node;
			NodeList npcList = element.getElementsByTagName("npc");
			for(int j=0; j<npcList.getLength(); j++){
				Node npc = npcList.item(j);
				Element npcElement = (Element)npc;
				String name = npcElement.getElementsByTagName("name").item(0).getTextContent();
				ArrayList<ImageIcon> imageList = getNPCIconById(npcElement.getElementsByTagName("imageId").item(0).getTextContent());
				ImageIcon portrait = imageList.get(4);
				ArrayList<String> sellList = null;
				if(npcElement.getElementsByTagName("isMerchant").item(0).getTextContent().equals("true")){
					sellList = new ArrayList<String>();
					NodeList list = npcElement.getElementsByTagName("sell");
					for(int k=0; k<list.getLength(); k++)
						sellList.add(list.item(k).getTextContent());
				}
				boolean isBoss;
				if(npcElement.getElementsByTagName("isBoss").item(0).getTextContent().equals("true"))
					isBoss = true;
				else
					isBoss = false;
				int row = Integer.parseInt(npcElement.getElementsByTagName("row").item(0).getTextContent());
				int col = Integer.parseInt(npcElement.getElementsByTagName("col").item(0).getTextContent());
				int range = Integer.parseInt(npcElement.getElementsByTagName("range").item(0).getTextContent());
				int dialogueNum = npcElement.getElementsByTagName("dialogue").getLength();
				Dialogue[] dialogueList = new Dialogue[dialogueNum];
				for(int k=0; k<dialogueNum; k++){
					Node innerNode = npcElement.getElementsByTagName("dialogue").item(k);
					if(innerNode.getNodeType() == Node.ELEMENT_NODE){
						Element innerElement = (Element)innerNode;
						int startEpisode = Integer.parseInt(innerElement.getAttribute("start").split("-")[0]);
						int startSubEpisode = Integer.parseInt(innerElement.getAttribute("start").split("-")[1]);
						int endEpisode = Integer.parseInt(innerElement.getAttribute("end").split("-")[0]);
						int endSubEpisode = Integer.parseInt(innerElement.getAttribute("end").split("-")[1]);
						String content = npcElement.getElementsByTagName("dialogue").item(k).getTextContent();
						Dialogue dialogue = new Dialogue(startEpisode, startSubEpisode, endEpisode, endSubEpisode, content);
						dialogueList[k] = dialogue;
					}									
				}
				NPC returnNPC = new NPC(name, imageList, portrait, row, col, dialogueList, range, sellList,isBoss);
				if(isBoss){
					int battleId = Integer.parseInt(npcElement.getElementsByTagName("battleEvent").item(0).getTextContent());
					int battleSubId = Integer.parseInt(npcElement.getElementsByTagName("battleEventSub").item(0).getTextContent());
					returnNPC.setBattleEvent(battleId, battleSubId);
				}
				returnList.add(returnNPC);
			}
		}
		return returnList;
	}
	
	public static RoleInit getRoleStatus(String RoleId){
		int index = getTypeIndex("role");
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName("role");
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element roleElement = (Element)node;
				if(roleElement.getAttribute("id").equals(RoleId)){
					RoleInit roleInit = new RoleInit();
					roleInit.title = roleElement.getElementsByTagName("title").item(0).getTextContent();
					roleInit.initHP = Integer.parseInt(roleElement.getElementsByTagName("initHP").item(0).getTextContent());
					roleInit.initMP = Integer.parseInt(roleElement.getElementsByTagName("initMP").item(0).getTextContent());
					roleInit.initAtk = Integer.parseInt(roleElement.getElementsByTagName("initAtk").item(0).getTextContent());
					roleInit.initDef = Integer.parseInt(roleElement.getElementsByTagName("initDef").item(0).getTextContent());
					roleInit.initSpd = Integer.parseInt(roleElement.getElementsByTagName("initSpd").item(0).getTextContent());
					roleInit.initExp = Integer.parseInt(roleElement.getElementsByTagName("initExp").item(0).getTextContent());
					roleInit.hpRate = Integer.parseInt(roleElement.getElementsByTagName("hpRate").item(0).getTextContent());
					roleInit.mpRate = Integer.parseInt(roleElement.getElementsByTagName("mpRate").item(0).getTextContent());
					roleInit.atkRate = Integer.parseInt(roleElement.getElementsByTagName("atkRate").item(0).getTextContent());
					roleInit.defRate = Integer.parseInt(roleElement.getElementsByTagName("defRate").item(0).getTextContent());
					roleInit.spdRate = Integer.parseInt(roleElement.getElementsByTagName("spdRate").item(0).getTextContent());
					roleInit.expRate = Integer.parseInt(roleElement.getElementsByTagName("expRate").item(0).getTextContent());
					int skillNum = roleElement.getElementsByTagName("skill").getLength();
					String[] skillList = new String[skillNum];
					int[] levelList = new int[skillNum];
					Node skillNode;
					Element skillElement;
					for(int k=0; k<skillNum; k++){	
						skillNode = roleElement.getElementsByTagName("skill").item(k);
						skillList[k] = skillNode.getTextContent();
						skillElement = (Element)skillNode;
						levelList[k] = Integer.parseInt( skillElement.getAttribute("level") );
					}
					roleInit.skillNameList = skillList;
					roleInit.skillLevelList = levelList;
					return roleInit;
				}
			}
		}
		return null;
	}
	
	public static ArrayList<RoleInit> getRoles(){
		int index = getTypeIndex("role");
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName("role");
		ArrayList<RoleInit> roles = new ArrayList<RoleInit>();
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element roleElement = (Element)node;
				RoleInit roleInit = new RoleInit();
				roleInit.title = roleElement.getElementsByTagName("title").item(0).getTextContent();
				roleInit.initHP = Integer.parseInt(roleElement.getElementsByTagName("initHP").item(0).getTextContent());
				roleInit.initMP = Integer.parseInt(roleElement.getElementsByTagName("initMP").item(0).getTextContent());
				roleInit.initAtk = Integer.parseInt(roleElement.getElementsByTagName("initAtk").item(0).getTextContent());
				roleInit.initDef = Integer.parseInt(roleElement.getElementsByTagName("initDef").item(0).getTextContent());
				roleInit.initSpd = Integer.parseInt(roleElement.getElementsByTagName("initSpd").item(0).getTextContent());
				roleInit.initExp = Integer.parseInt(roleElement.getElementsByTagName("initExp").item(0).getTextContent());
				roleInit.hpRate = Integer.parseInt(roleElement.getElementsByTagName("hpRate").item(0).getTextContent());
				roleInit.mpRate = Integer.parseInt(roleElement.getElementsByTagName("mpRate").item(0).getTextContent());
				roleInit.atkRate = Integer.parseInt(roleElement.getElementsByTagName("atkRate").item(0).getTextContent());
				roleInit.defRate = Integer.parseInt(roleElement.getElementsByTagName("defRate").item(0).getTextContent());
				roleInit.spdRate = Integer.parseInt(roleElement.getElementsByTagName("spdRate").item(0).getTextContent());
				roleInit.expRate = Integer.parseInt(roleElement.getElementsByTagName("expRate").item(0).getTextContent());
				int skillNum = roleElement.getElementsByTagName("skill").getLength();
				String[] skillList = new String[skillNum];
				int[] levelList = new int[skillNum];
				Node skillNode;
				Element skillElement;
				for(int k=0; k<skillNum; k++){	
					skillNode = roleElement.getElementsByTagName("skill").item(k);
					skillList[k] = skillNode.getTextContent();
					skillElement = (Element)skillNode;
					levelList[k] = Integer.parseInt( skillElement.getAttribute("level") );
				}
				roleInit.skillNameList = skillList;
				roleInit.skillLevelList = levelList;
				roles.add(roleInit);
			}
		}
		return roles;
	}
	
	public static Skill getSkillByName(String skillName){
		int index = getTypeIndex("skill");
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName("skill");
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element roleElement = (Element)node;
				if(roleElement.getAttribute("name").equals(skillName)){
					SkillInit skillInit = new SkillInit();
					skillInit.description = roleElement.getElementsByTagName("description").item(0).getTextContent();
					skillInit.mpCost = Integer.parseInt(roleElement.getElementsByTagName("mpCost").item(0).getTextContent());
					skillInit.isSingle = Boolean.parseBoolean(roleElement.getElementsByTagName("isSingle").item(0).getTextContent());
					skillInit.isNear = Boolean.parseBoolean(roleElement.getElementsByTagName("isNear").item(0).getTextContent());
					skillInit.isAttack = Boolean.parseBoolean(roleElement.getElementsByTagName("isAttack").item(0).getTextContent());
					skillInit.isHeal = Boolean.parseBoolean(roleElement.getElementsByTagName("isHeal").item(0).getTextContent());
					skillInit.amount = Integer.parseInt(roleElement.getElementsByTagName("amount").item(0).getTextContent());
					skillInit.biasRate = Double.parseDouble(roleElement.getElementsByTagName("biasRate").item(0).getTextContent());
					skillInit.hitRate = Double.parseDouble(roleElement.getElementsByTagName("hitRate").item(0).getTextContent());
					skillInit.isStatusAttack = Boolean.parseBoolean(roleElement.getElementsByTagName("isStatusAttack").item(0).getTextContent());
					skillInit.isStatusHeal = Boolean.parseBoolean(roleElement.getElementsByTagName("isStatusHeal").item(0).getTextContent());
					skillInit.status = Integer.parseInt(roleElement.getElementsByTagName("status").item(0).getTextContent());
					skillInit.statusHitRate = Double.parseDouble(roleElement.getElementsByTagName("statusHitRate").item(0).getTextContent());
					skillInit.sound = roleElement.getElementsByTagName("sound").item(0).getTextContent();
					return new Skill(skillName, skillInit);
				}
			}
		}
		return null;
	}
	
	public static int getSubEpisodeNum(int episodeId){
		int index = getTypeIndex("episode");
		ArrayList<ImageIcon> imageList = new ArrayList<ImageIcon>();
		if(index < 0)
			return -1;
		NodeList nodeList = docList[index].getElementsByTagName("episode");
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element)node;
				if(Integer.parseInt(element.getAttribute("id")) == episodeId)
					return Integer.parseInt(element.getElementsByTagName("subNum").item(0).getTextContent());
			}
		}
		return -1;
	}
	
	public static ArrayList<Episode> getEpisodes(){
		int index = getTypeIndex("episode");
		ArrayList<Episode> returnList = new ArrayList<Episode>();
		if(index < 0)
			return null;
		NodeList episodeList = docList[index].getElementsByTagName("episode");
		for(int i=0; i<episodeList.getLength(); i++){
			Element episode = (Element)episodeList.item(i);
			int id = Integer.parseInt(episode.getAttribute("id"));
			int subNum = Integer.parseInt(episode.getElementsByTagName("subNum").item(0).getTextContent());
			NodeList subEpisodeList = episode.getElementsByTagName("sub");
			for(int j=0; j<subNum; j++){
				Element subEpisode = (Element)subEpisodeList.item(j);
				int subId = Integer.parseInt(subEpisode.getAttribute("id"));
				int type = Integer.parseInt(subEpisode.getElementsByTagName("type").item(0).getTextContent());
				String name = subEpisode.getElementsByTagName("name").item(0).getTextContent();
				String targetName = subEpisode.getElementsByTagName("targetName").item(0).getTextContent();
				String itemEvent = subEpisode.getElementsByTagName("itemEvent").item(0).getTextContent();
				String roleEvent = subEpisode.getElementsByTagName("roleEvent").item(0).getTextContent();
				String mapBound = subEpisode.getElementsByTagName("areaBound").item(0).getTextContent();
				ArrayList<Item> needItemList = null;
				ArrayList<Item> getItemList = null;
				ArrayList<Item> loseItemList = null;
				ArrayList<Role> getRoleList = null;
				ArrayList<Role> loseRoleList = null;
				ArrayList<Integer> activeMapList = null;
				String boundWords = null;
				if(itemEvent.equals("true")){
					getItemList = new ArrayList<Item>();
					NodeList list = subEpisode.getElementsByTagName("getItem");
					for(int k=0; k<list.getLength(); k++){
						Item item = getItemByName(list.item(k).getTextContent());
						int num = Integer.parseInt(((Element)list.item(k)).getAttribute("num"));
						item.setAmount(num);
						getItemList.add(item);
					}
					loseItemList = new ArrayList<Item>();
					NodeList list2 = subEpisode.getElementsByTagName("loseItem");
					for(int k=0; k<list2.getLength(); k++){
						Item item = getItemByName(list2.item(k).getTextContent());
						int num = Integer.parseInt(((Element)list2.item(k)).getAttribute("num"));
						item.setAmount(num);
						loseItemList.add(item);
					}
				}
				if(roleEvent.equals("true")){
					
				}
				if(mapBound.equals("true")){
					boundWords = subEpisode.getElementsByTagName("boundWords").item(0).getTextContent();
					activeMapList = new ArrayList<Integer>();
					NodeList list = subEpisode.getElementsByTagName("area");
					for(int k=0; k<list.getLength(); k++)
						activeMapList.add(Integer.parseInt(list.item(k).getTextContent()));
					
				}
				Episode newEpisode = new Episode(id, subId, type, name, targetName, needItemList, getItemList, loseItemList, getRoleList, loseRoleList, activeMapList, boundWords);
				returnList.add(newEpisode);
			}
		}
		return returnList;
	}
	
	public static Item getItemByName(String resName){
		int index = getTypeIndex("item");
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName("item");
		
		if(resName.equals("MONEY"))
			return new Other(0, 1, Item.TYPE_OTHER, 0, "MONEY", null, null);
		for(int i=0; i<nodeList.getLength(); i++){
			Element element = (Element)nodeList.item(i);
			if(element.getElementsByTagName("name").item(0).getTextContent().equals(resName)){
				int id = Integer.parseInt(element.getAttribute("id"));
				int price = Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent());
				ImageIcon image = getIconByName("item", element.getElementsByTagName("image").item(0).getTextContent());
				String utility = element.getElementsByTagName("utility").item(0).getTextContent();
				int type = Integer.parseInt(element.getElementsByTagName("type").item(0).getTextContent());
				if(type == Item.TYPE_EQUIP){
					int position = Integer.parseInt(element.getElementsByTagName("position").item(0).getTextContent());
					int hpValue = Integer.parseInt(element.getElementsByTagName("hpValue").item(0).getTextContent());
					int mpValue = Integer.parseInt(element.getElementsByTagName("mpValue").item(0).getTextContent());
					int attValue = Integer.parseInt(element.getElementsByTagName("attValue").item(0).getTextContent());
					int defValue = Integer.parseInt(element.getElementsByTagName("defValue").item(0).getTextContent());
					int spdValue = Integer.parseInt(element.getElementsByTagName("spdValue").item(0).getTextContent());
					Equipment equip = new Equipment(id, 1, type, price, resName, utility, image);
					equip.setProperty(position, hpValue, mpValue, attValue, defValue, spdValue);
					return equip;
				}
				else if(type == Item.TYPE_POTION){
					boolean isSingle = Boolean.parseBoolean(element.getElementsByTagName("isSingle").item(0).getTextContent());
					int hpValue = Integer.parseInt(element.getElementsByTagName("hpValue").item(0).getTextContent());
					int mpValue = Integer.parseInt(element.getElementsByTagName("mpValue").item(0).getTextContent());
					int status = Integer.parseInt(element.getElementsByTagName("status").item(0).getTextContent());
					String soundName = element.getElementsByTagName("sound").item(0).getTextContent();
					String animationName = element.getElementsByTagName("animation").item(0).getTextContent();
					Potion potion = new Potion(id, 1, type, price, resName, utility, image);
					potion.setProperty(hpValue, mpValue, status, isSingle, soundName, animationName);
					return potion;
				}
				return null;
			}
		}
		return null;
	}
	
	public static ArrayList<Item> getItems(){
		int index = getTypeIndex("item");
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName("item");
		
		ArrayList<Item> returnList = new ArrayList<Item>();
		for(int i=0; i<nodeList.getLength(); i++){
			Element element = (Element)nodeList.item(i);
			String name = element.getElementsByTagName("name").item(0).getTextContent();
			if(name.equals("MONEY")){
				returnList.add(new Other(0, 1, Item.TYPE_OTHER, 0, "MONEY", null, null));
				continue;
			}
			int id = Integer.parseInt(element.getAttribute("id"));
			int price = Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent());
			ImageIcon image = getIconByName("item", element.getElementsByTagName("image").item(0).getTextContent());
			String utility = element.getElementsByTagName("utility").item(0).getTextContent();
			int type = Integer.parseInt(element.getElementsByTagName("type").item(0).getTextContent());
			if(type == Item.TYPE_EQUIP){
				int position = Integer.parseInt(element.getElementsByTagName("position").item(0).getTextContent());
				int hpValue = Integer.parseInt(element.getElementsByTagName("hpValue").item(0).getTextContent());
				int mpValue = Integer.parseInt(element.getElementsByTagName("mpValue").item(0).getTextContent());
				int attValue = Integer.parseInt(element.getElementsByTagName("attValue").item(0).getTextContent());
				int defValue = Integer.parseInt(element.getElementsByTagName("defValue").item(0).getTextContent());
				int spdValue = Integer.parseInt(element.getElementsByTagName("spdValue").item(0).getTextContent());
				Equipment equip = new Equipment(id, 1, type, price, name, utility, image);
				equip.setProperty(position, hpValue, mpValue, attValue, defValue, spdValue);
				returnList.add(equip);
			}
			else if(type == Item.TYPE_POTION){
				boolean isSingle = Boolean.parseBoolean(element.getElementsByTagName("isSingle").item(0).getTextContent());
				int hpValue = Integer.parseInt(element.getElementsByTagName("hpValue").item(0).getTextContent());
				int mpValue = Integer.parseInt(element.getElementsByTagName("mpValue").item(0).getTextContent());
				int status = Integer.parseInt(element.getElementsByTagName("status").item(0).getTextContent());
				String soundName = element.getElementsByTagName("sound").item(0).getTextContent();
				String animationName = element.getElementsByTagName("animation").item(0).getTextContent();
				Potion potion = new Potion(id, 1, type, price, name, utility, image);
				potion.setProperty(hpValue, mpValue, status, isSingle, soundName, animationName);
				returnList.add(potion);
			}
		}
		return returnList;
	}
	
	public static Monster[] getAllMonsters(){
		int index = getTypeIndex("monster");
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName("monster");
		
		Monster[] monsterList = new Monster[nodeList.getLength()];
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element)node;
				String name = element.getElementsByTagName("name").item(0).getTextContent();
				ImageIcon img = ResourceHandler.getIconByName("monster", element.getElementsByTagName("image")
										.item(0).getTextContent());
				int hp = Integer.parseInt(element.getElementsByTagName("hp").item(0).getTextContent());
				int strategy = Integer.parseInt(element.getElementsByTagName("strategy").item(0).
												getTextContent());
				int skillNum = Integer.parseInt(element.getElementsByTagName("skillNum").item(0).
												getTextContent());
				int money = Integer.parseInt(element.getElementsByTagName("money").item(0).
													getTextContent());
				int exp = Integer.parseInt(element.getElementsByTagName("exp").item(0).
													getTextContent());
				Item item = ResourceHandler.getItemByName(element.getElementsByTagName("item")
										.item(0).getTextContent());										
				Skill[] skillList = new Skill[skillNum];
				
				double[] skillRate = new double[skillNum];
				int[] skillOrder = new int[0];
				
				for(int j=0; j<skillNum; j++)
					skillList[j] = ResourceHandler.getSkillByName(element.getElementsByTagName("skill").item(j).getTextContent());
				
				String normalAnimationName = element.getElementsByTagName("NormalAnimationName").item(0).getTextContent();
				String attackAnimationName = element.getElementsByTagName("AttackAnimationName").item(0).getTextContent();
				
				monsterList[i] = new Monster(name, img, hp, strategy, skillList, 
											skillRate, skillOrder,money,exp,item, normalAnimationName, attackAnimationName);
			}
		}
		return monsterList;
	}
	
	public static Monster getMonster(String name){
		int index = getTypeIndex("monster");
		if(index < 0)
			return null;
		NodeList nodeList = docList[index].getElementsByTagName("monster");
		
		Monster[] monsterList = new Monster[nodeList.getLength()];
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element)node;
				if(element.getElementsByTagName("name").item(0).getTextContent().equals(name)){
					ImageIcon img = ResourceHandler.getIconByName("monster", element.getElementsByTagName("image")
											.item(0).getTextContent());
					int hp = Integer.parseInt(element.getElementsByTagName("hp").item(0).getTextContent());
					int strategy = Integer.parseInt(element.getElementsByTagName("strategy").item(0).
													getTextContent());
					int skillNum = Integer.parseInt(element.getElementsByTagName("skillNum").item(0).
													getTextContent());
					int money = Integer.parseInt(element.getElementsByTagName("money").item(0).
													getTextContent());
					int exp = Integer.parseInt(element.getElementsByTagName("exp").item(0).
													getTextContent());
					Item item = ResourceHandler.getItemByName(element.getElementsByTagName("item")
										.item(0).getTextContent());			
					Skill[] skillList = new Skill[skillNum];
					for(int j=0; j<skillNum; j++)
						skillList[j] = ResourceHandler.getSkillByName(element.getElementsByTagName("skill").item(j).getTextContent());
					
					double[] skillRate = new double[skillNum];
					int[] skillOrder = new int[0];
					
					String normalAnimationName = element.getElementsByTagName("NormalAnimationName").item(0).getTextContent();
					String attackAnimationName = element.getElementsByTagName("AttackAnimationName").item(0).getTextContent();
					
					Monster monster = new Monster(name, img, hp, strategy, skillList, skillRate, 
													skillOrder,money,exp,item,normalAnimationName, attackAnimationName);
					return monster;
				}
			}
		}
		return null;
	}
	
	public static BufferedImage[] getHumanAnimationFrames(String resName){
		int index = getTypeIndex("animation");
		if(index < 0)
			return null;		
		NodeList nodeList = ((Element)docList[index].getElementsByTagName("role").item(0)).getElementsByTagName("animation");
		
		BufferedImage srcImage;
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element)node;
				if(element.getElementsByTagName("name").item(0).getTextContent().equals(resName)){
					int frameNum = Integer.parseInt(element.getElementsByTagName("frameNum").
									item(0).getTextContent());
					try{
						srcImage = ImageIO.read(new FileInputStream(new File(element.
									getElementsByTagName("frameFile").item(0).getTextContent())));
						BufferedImage[] biArray = new BufferedImage[frameNum];
						for(int j=0; j<frameNum; j++){
							Node frameNode = element.getElementsByTagName("frame").item(j);
							Element frameElement = (Element)frameNode;
							int row = Integer.parseInt(frameElement.getElementsByTagName("row").item(0).getTextContent());
							int col = Integer.parseInt(frameElement.getElementsByTagName("col").item(0).getTextContent());
							biArray[j] = srcImage.getSubimage(FRAME_WIDTH * (col-1), FRAME_HEIGHT * (row-1), FRAME_WIDTH, FRAME_HEIGHT);
						}
						return biArray;
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}
		return null;
	}
	
	
	public static BufferedImage[] getMonsterAnimationFrames(String resName){
		int index = getTypeIndex("animation");
		if(index < 0)
			return null;
		NodeList nodeList = ((Element)docList[index].getElementsByTagName("monster").item(0)).getElementsByTagName("animation");
		
		BufferedImage srcImage;
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element)node;
				if(element.getElementsByTagName("name").item(0).getTextContent().equals(resName)){
					int frameNum = Integer.parseInt(element.getElementsByTagName("frameNum").
									item(0).getTextContent());
					int rowSize = Integer.parseInt(element.getElementsByTagName("rowSize").
									item(0).getTextContent());
					int colSize = Integer.parseInt(element.getElementsByTagName("colSize").
									item(0).getTextContent());
					int scale = Integer.parseInt(element.getElementsByTagName("scale").
									item(0).getTextContent());
					try{
						srcImage = ImageIO.read(new FileInputStream(new File(element.
									getElementsByTagName("frameFile").item(0).getTextContent())));
						BufferedImage[] biArray = new BufferedImage[frameNum];
						for(int j=0; j<frameNum; j++){
							Node frameNode = element.getElementsByTagName("frame").item(j);
							Element frameElement = (Element)frameNode;
							int row = Integer.parseInt(frameElement.getElementsByTagName("row").item(0).getTextContent());
							int col = Integer.parseInt(frameElement.getElementsByTagName("col").item(0).getTextContent());
							biArray[j] = srcImage.getSubimage(colSize * (col-1), rowSize * (row-1), colSize, rowSize);
							biArray[j] = Drawer.getResizedImg(biArray[j],colSize*scale,rowSize*scale);
						}
						return biArray;
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}
		return null;
	}
	
	public static BufferedImage[] getSkillAnimationFrames(String resName){
		int index = getTypeIndex("animation");
		if(index < 0)
			return null;		
		NodeList nodeList = ((Element)docList[index].getElementsByTagName("skill").item(0)).getElementsByTagName("animation");
		
		BufferedImage srcImage;
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element)node;
				if(element.getElementsByTagName("name").item(0).getTextContent().equals(resName)){
					int frameNum = Integer.parseInt(element.getElementsByTagName("frameNum").
									item(0).getTextContent());
					int rowSize = Integer.parseInt(element.getElementsByTagName("rowSize").
									item(0).getTextContent());
					int colSize = Integer.parseInt(element.getElementsByTagName("colSize").
									item(0).getTextContent());
					int scale = Integer.parseInt(element.getElementsByTagName("scale").
									item(0).getTextContent());
					try{
						srcImage = ImageIO.read(new FileInputStream(new File(element.
									getElementsByTagName("frameFile").item(0).getTextContent())));
						BufferedImage[] biArray = new BufferedImage[frameNum];
						for(int j=0; j<frameNum; j++){
							Node frameNode = element.getElementsByTagName("frame").item(j);
							Element frameElement = (Element)frameNode;
							int row = Integer.parseInt(frameElement.getElementsByTagName("row").item(0).getTextContent());
							int col = Integer.parseInt(frameElement.getElementsByTagName("col").item(0).getTextContent());
							biArray[j] = srcImage.getSubimage(colSize * (col-1), rowSize * (row-1), colSize, rowSize);
							biArray[j] = Drawer.getResizedImg(biArray[j],colSize*scale,rowSize*scale);
						}
						return biArray;
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}
		return null;
	}
}