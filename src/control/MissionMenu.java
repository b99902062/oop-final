package rpg.control;

import java.lang.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import java.awt.*;
import java.net.*;

import javax.swing.*;
import javax.imageio.*;

import rpg.draw.*;
import rpg.game.*;
import rpg.character.*;
import rpg.resource.*;
import rpg.map.*;
import rpg.audio.*;
import rpg.status.*;
import rpg.mode.*;
import rpg.item.*;

/**
 * A class that implements the mission menu
 * @author  Chander, Moe, William
 * @version 1.0
 */
 
public class MissionMenu extends JPanel implements Serializable, KeyListener{

	private static final long serialVersionUID = 1L;
	private static final int MISSION_WINDOW = 8;
	private static final int MISSION_WIDTH = 500;
	private static final int MISSION_HEIGHT = 60;
	private static final int PORTRAIT_SIZE = 60;
	
	private JPanel mainPanel = Drawer.drawBackGround(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), Game.gui.getWidth()* 4/5, Game.gui.getHeight()* 4/5));
	private JLabel quillPenLabel = new JLabel(new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "quillPenImage"), 400, 800)));
	
	private ArrayList<JLabel> missionLabelList = new ArrayList<JLabel>();
	private ArrayList<JLabel> portraitLabelList = new ArrayList<JLabel>();
	
	private int uppestIndex;
	private int downestIndex;
	private int totalEpisode = Game.gsh.getEpisodeTotal();
	
	private GameMenu caller;
		
	public MissionMenu(GameMenu caller){
		this.caller = caller;
		this.setOpaque(false);
		this.setBounds(0, 0, Game.gui.getWidth(), Game.gui.getHeight());
		this.setLayout(null);
		initialLayout();
		initialMissionList();
		setMissionBox();
		this.repaint();
		Game.gui.getGlassPane().addKeyListener(this);
		requestFocus();
	}
	
	private void initialLayout(){
		mainPanel.setLayout(null);
		mainPanel.setBounds(Game.gui.getWidth()/10, Game.gui.getHeight()/10, Game.gui.getWidth()* 4/5, Game.gui.getHeight()*4/5);
		mainPanel.setOpaque(false);
		this.add(mainPanel, 0);
		
		quillPenLabel.setBounds(Game.gui.getWidth()*7/10, 100, 400, 800);
		this.add(quillPenLabel, 0);
		
		uppestIndex = 0;
		downestIndex = MISSION_WINDOW;
	}
	
	private void initialMissionList(){
		int maxId = Game.gsh.getEpisodeId();
		int maxSubId = Game.gsh.getSubEpisodeId();
		
		for(int i=1; i<=maxId; i++){
			JLabel label = new JLabel("Episode " + i);
			label.setSize(MISSION_WIDTH, MISSION_HEIGHT);
			label.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
			label.setForeground(Color.RED);
			missionLabelList.add(label);
			portraitLabelList.add(null);
			int maxNum = ResourceHandler.getSubEpisodeNum(i);
			for(int j=1; j<=maxNum; j++){
				if(i == maxId && j == maxSubId)
					break;
				JLabel subLabel = new JLabel("Sub " + i + "-" + j + " -- " + Game.gsh.getEpisode(i, j).getName());
				subLabel.setSize(MISSION_WIDTH, MISSION_HEIGHT);
				subLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
				subLabel.setForeground(Color.BLUE);
				missionLabelList.add(subLabel);
				
				String targetName = Game.gsh.getEpisode(i, j).getTargetName();
				ImageIcon targetPortrait = Game.gsh.getNPCByName(targetName).getPortrait();
				JLabel portraitLabel = new JLabel(new ImageIcon(Drawer.getResizedImg(targetPortrait.getImage(), PORTRAIT_SIZE, PORTRAIT_SIZE)));
				portraitLabel.setSize(PORTRAIT_SIZE, PORTRAIT_SIZE);
				portraitLabelList.add(portraitLabel);
			}
		}
	}
	
	private void setMissionBox(){
		mainPanel.removeAll();
		for(int i=uppestIndex; i<downestIndex; i++){
			if(i >= missionLabelList.size())
				break;
				
			JLabel label = missionLabelList.get(i);
			JLabel portraitLabel = portraitLabelList.get(i);
			if(label.getText().startsWith("Episode"))
				label.setLocation(200, 40 + (MISSION_HEIGHT + 10) * i);
			else{
				portraitLabel.setLocation(300, 40 + (MISSION_HEIGHT + 10) * i);
				label.setLocation(400, 40 + (MISSION_HEIGHT + 10) * i);
			}
			mainPanel.add(label, 0);
			if(portraitLabel != null)
				mainPanel.add(portraitLabel, 0);
		}
	}
	
	private void moveMissionWindow(boolean isDown){
		if(isDown){
			uppestIndex++;
			downestIndex++;
		}
		else{
			uppestIndex--;
			downestIndex--;
		}
		setMissionBox();
	}
	
	public void keyTyped(KeyEvent e){
	}
	
	public synchronized void keyPressed(KeyEvent e){
		int keyCode = e.getKeyCode();
		
		if(keyCode == Game.ESC){
			((JPanel)Game.gui.getGlassPane()).remove(this);
			Game.gui.getGlassPane().removeKeyListener(this);
			Game.gui.getGlassPane().addKeyListener(caller);
			caller.setVisible(true);
			Game.gui.repaint();
		}
		else if(keyCode == Game.UP){
			if(uppestIndex > 0)
				moveMissionWindow(false);
		}
		else if(keyCode == Game.DOWN){
			if(downestIndex < totalEpisode)
				moveMissionWindow(true);
		}
	}
		
	public void keyReleased(KeyEvent e){
	}
}