package rpg.control;

import java.lang.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import java.awt.*;
import java.net.*;

import javax.swing.*;
import javax.imageio.*;

import rpg.draw.*;
import rpg.game.*;
import rpg.resource.*;
import rpg.map.*;
import rpg.audio.*;
import rpg.status.*;
import rpg.mode.*;

/**
 * A class that implements a menu in the game
 * @author  Chander, Moe, William
 * @version 1.0
 */
 
public class GameMenu extends JPanel implements Serializable, KeyListener, Runnable{

	private static final long serialVersionUID = 1L;

	private int wordHeight = 65;
	private int wordWidth = 300;
	private int paneHeight;
	private int paneWidth;

	private AudioPlayer switchPlayer = new AudioPlayer(false, ResourceHandler.getResourcePath("sound", "switchSound"));
	
	private JLabel[] labelList;
	private int wordAmount = 9;
	/*First and Last word is empty*/
	private String[] wordList = { "", "SaveGame", "LoadGame", "Mission", "Character", "BackPack", "Return", "Exit", "" };
	private int currentWord = 1;
	
	private boolean isSave = false;
	private boolean isLoad = false;
	private boolean isMission = false;
	private boolean isCharacter = false;
	private boolean isBackPack = false;
	private boolean isReturn = false;
	private boolean isExit = false;
	
	private Image image;
	
	private Controller c;
	
	public GameMenu(Controller c){
		this.c = c;
		paneHeight = wordHeight * wordAmount + 100;
		paneWidth = wordWidth + 100;
		
		this.setSize( paneWidth, paneHeight );
		this.setLocation( (Game.gui.getWidth()/2)-paneWidth/2, (Game.gui.getHeight()/2)-paneHeight/2 );
		
		/*
		System.out.println( "MENU height:" + this.getBounds().height );
		System.out.println( "MENU width:" + this.getBounds().width );
		System.out.println( "MENU x:" + this.getBounds().x );
		System.out.println( "MENU y:" + this.getBounds().y );
		*/
		
		this.setLayout( new GridLayout(wordAmount, 1) );
		this.addInitialWord();
		this.repaint();
	}
	
	public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null); 
    }
	
	public void setImage( Image i ){
		this.image = i;
		this.setOpaque(false);
	}
	
	public void run(){
	
		this.setVisible(true);
		
		execute();
		
		Game.gui.getGlassPane().removeKeyListener(this);
		Game.gui.getGlassPane().setVisible(false);
		JPanel glassPane = (JPanel)Game.gui.getGlassPane();
		glassPane.removeAll();
		
		if(isSave)
			Game.gui.requestFocus();
		else if(isLoad)
			Game.gui.requestFocus();
		else if(isMission)
			Game.gui.requestFocus();
		else if(isCharacter)
			Game.gui.requestFocus();
		else if(isBackPack)
			Game.gui.requestFocus();
		else if(isReturn)
			Game.gui.requestFocus();
		else if(isExit)
			c.notifyWaitForOut();
	}
	
	public synchronized void execute(){
		System.out.println("EXECUTING");
		try{
			wait();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void addInitialWord(){
		labelList = new JLabel[wordAmount];
		for( int i=0;i<wordAmount;i++ ){
			labelList[i] = new JLabel( wordList[i], JLabel.CENTER );
			labelList[i].setFont( new Font("Trebuchet MS", Font.BOLD, 50) );
			labelList[i].setSize(wordWidth, wordHeight);
			labelList[i].setOpaque(false);
			if( i==1 ) labelList[i].setForeground(Color.blue);
			else labelList[i].setForeground(Color.gray);
			
			System.out.println( "labelheight:" + labelList[i].getSize().height );
			System.out.println( "labelwidth:" + labelList[i].getSize().width );
			
			this.add( labelList[i] );
		}
	}
	
	public void keyTyped(KeyEvent e){
	}
	
	public synchronized void keyPressed(KeyEvent e){
		int keyCode = e.getKeyCode();
		System.out.println(keyCode);
		if(keyCode == Game.UP){
			if(currentWord > 1){
				Thread soundThread = new Thread(switchPlayer);
				soundThread.start();
				labelList[currentWord].setForeground(Color.gray);
				currentWord--;
				labelList[currentWord].setForeground(Color.blue);
			}
			else if(currentWord == 1){
				Thread soundThread = new Thread(switchPlayer);
				soundThread.start();
				labelList[currentWord].setForeground(Color.gray);
				currentWord = wordAmount - 2;
				labelList[currentWord].setForeground(Color.blue);
			}
		}
		else if(keyCode == Game.DOWN){
			if(currentWord < (wordAmount-2)){
				Thread soundThread = new Thread(switchPlayer);
				soundThread.start();
				labelList[currentWord].setForeground(Color.gray);
				currentWord++;
				labelList[currentWord].setForeground(Color.blue);
			}
			else if(currentWord == (wordAmount-2)){
				Thread soundThread = new Thread(switchPlayer);
				soundThread.start();
				labelList[currentWord].setForeground(Color.gray);
				currentWord = 1;
				labelList[currentWord].setForeground(Color.blue);
			}
		}
		else if(keyCode == Game.ENTER){
			if(labelList[currentWord].getText().equals("Exit")){
				isExit = true;
				notify();
			}
			else if(labelList[currentWord].getText().equals("Return")){
				isReturn = true;
				notify();
			}
			else if(labelList[currentWord].getText().equals("BackPack")){
				JPanel glassPane = (JPanel)Game.gui.getGlassPane();
				glassPane.removeKeyListener(this);
				this.setVisible(false);
				
				BackpackMenu bkm = new BackpackMenu(this);
				glassPane.add(bkm, 0);
				bkm.repaint();
			}
			else if(labelList[currentWord].getText().equals("Mission")){
				JPanel glassPane = (JPanel)Game.gui.getGlassPane();
				glassPane.removeKeyListener(this);
				this.setVisible(false);
				
				MissionMenu mm = new MissionMenu(this);
				glassPane.add(mm, 0);
				mm.repaint();
			}
			else if(labelList[currentWord].getText().equals("LoadGame")){
				JPanel glassPane = (JPanel)Game.gui.getGlassPane();
				glassPane.removeKeyListener(this);
				this.setVisible(false);
				
				SaveMenu sm = new SaveMenu(true, this, false);
				glassPane.add(sm, 0);
				sm.repaint();
			}
			else if(labelList[currentWord].getText().equals("SaveGame")){
				JPanel glassPane = (JPanel)Game.gui.getGlassPane();
				glassPane.removeKeyListener(this);
				this.setVisible(false);
				
				SaveMenu sm = new SaveMenu(true, this, true);
				glassPane.add(sm, 0);
				sm.repaint();
			}
			else if(labelList[currentWord].getText().equals("Character")){
				JPanel glassPane = (JPanel)Game.gui.getGlassPane();
				glassPane.removeKeyListener(this);
				this.setVisible(false);
				
				RoleMenu rm = new RoleMenu(this);
				glassPane.add(rm, 0);
				rm.repaint();
				
			}
			else{
				isReturn = true;
				notify();
			}
		}
		else if(keyCode == Game.ESC){
			isReturn = true;
			notify();
		}
	}
		
	public void keyReleased(KeyEvent e){
	}
	
	public synchronized void doReload(){
		isExit = true;
		notify();
	}
	
}