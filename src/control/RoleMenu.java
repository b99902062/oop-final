package rpg.control;

import java.lang.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import java.awt.*;
import java.net.*;

import javax.swing.*;
import javax.imageio.*;

import rpg.draw.*;
import rpg.game.*;
import rpg.resource.*;
import rpg.map.*;
import rpg.audio.*;
import rpg.status.*;
import rpg.mode.*;
import rpg.item.*;
import rpg.character.*;

/**
 * A class that implements the role menu
 * @author  Chander, Moe, William
 * @version 1.0
 */
 
public class RoleMenu extends JPanel implements Serializable, KeyListener, Runnable{

	private static final long serialVersionUID = 1L;
	
	private final int roleSelectX = 10;
	private final int roleSelectY = 0;
	private final int roleSelectWidth = Game.gui.getWidth()/5 - roleSelectX;
	private final int roleSelectHeight = Game.gui.getHeight() - roleSelectY;
	private final int roleInfoX = roleSelectX + roleSelectWidth;
	private final int roleInfoY = roleSelectY;
	private final int roleInfoWidth = Game.gui.getWidth() - roleInfoX;
	private final int roleInfoHeight = roleSelectHeight;
	
	private final int roleSelectImageX = roleSelectX + 5;
	private final int roleSelectImageY = roleSelectY + 30;
	private final int roleSelectImageWidth = roleSelectWidth - 25;
	private final int roleSelectImageHeight = (roleSelectHeight - 60)/4;
	private final int taskAmount = 3;
	private final int taskX = 30;//start from panel it added on, not the glasspane
	private final int taskY = 30;
	private final int taskWidth = (roleInfoWidth-taskX)/taskAmount-10;
	private final int taskHeight = (roleInfoHeight-taskY)/6;
	private final int roleInfoNum = 9;
	private final int infoWidth = roleInfoWidth;
	private final int infoHeight = (roleInfoHeight-taskHeight)/9-10;
	
	private final int skillWidth = roleInfoWidth/3;
	private final int skillHeight = (roleInfoHeight-taskHeight)/7;
	private final int skillInfoNum = 9;
	private JLabel[] skillInfoList = new JLabel[skillInfoNum*2];
	private final int skillInfoX = taskX+skillWidth;
	private final int skillInfoY = taskY+taskHeight;
	private final int skillInfoWidth = roleInfoWidth-skillWidth;
	private final int skillInfoHeight = (roleInfoHeight-taskHeight)/9-10;
	
	private final int modelX = taskX;
	private final int modelY = taskY + taskHeight-40;
	private final int modelWidth = roleInfoWidth/2;
	private final int modelHeight = roleInfoHeight-taskHeight-10;
	private final int equipTypeNum = 5;
	
	private final int modelImageWidth = 100;
	private final int modelImageHeight = 100;
	private final int modelUX = modelX + 220;
	private final int modelUY = modelY + 50;
	private final int modelLX = modelX + 50;
	private final int modelLY = modelY + 160;
	private final int modelCX = modelUX - 10;
	private final int modelCY = modelY + 288;
	private final int modelDX = modelUX - 10;
	private final int modelDY = modelY + 475;
	private final int modelRX = modelX + 385;
	private final int modelRY = modelLY + 10;
	
	private final int gridX = modelX + modelWidth - 60;
	private final int gridY = taskY + taskHeight - 60;
	private final int gridWidth = roleInfoWidth/2;
	private final int gridHeight = roleInfoHeight - taskHeight - 10;
	
	private final int firstGridX = gridX + 65;
	private final int firstGridY = gridY + 181;
	private final int singleGridWidth = gridWidth/4 - 30;
	private final int singleGridHeight = gridHeight/5 - 25;
	private final int singleGridMoveWidth = singleGridWidth - 1;
	private final int singleGridMoveHeight = singleGridHeight - 1;
	
	private final int wordsX = modelRX + 5;
	private final int wordsY = modelCY + 30;
	private final int wordsWidth = singleGridWidth/2 + 100;
	private final int wordsHeight = singleGridHeight*2/3 - 25;
	
	private final int wordX = gridX;
	private final int wordY = gridY;
	private final int wordWidth = gridWidth;
	private final int wordHeight = gridHeight/5 + 110;

	/*Images*/
	private Image selectBG = Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), roleSelectWidth, roleSelectHeight);
	private Image infoBG = Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), roleInfoWidth, roleInfoHeight);
	private ImageIcon selectFocusImage = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "focusImage3"), roleSelectImageWidth, roleSelectImageHeight));
	private ImageIcon taskFocusImage = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "focusbImage"), taskWidth, taskHeight));
	private ImageIcon infoFocusImage = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "focusImage"), infoWidth, infoHeight));
	private ImageIcon skillFocusImage = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "focusImage2"), skillWidth, skillHeight));
	
	private ImageIcon modelImage = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "modelImage"), modelWidth, modelHeight));
	private ImageIcon modelCImage = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "modelCImage"), modelWidth, modelHeight));
	private ImageIcon modelDImage = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "modelDImage"), modelWidth, modelHeight));
	private ImageIcon modelLImage = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "modelLImage"), modelWidth, modelHeight));
	private ImageIcon modelRImage = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "modelRImage"), modelWidth, modelHeight));
	private ImageIcon modelUImage = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "modelUImage"), modelWidth, modelHeight));
	
	private ImageIcon gridImage = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "gridImage"), gridWidth, gridHeight));
	private ImageIcon gridFocusImage = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "gridFocusImage"), singleGridWidth, singleGridHeight));
	
	private AudioPlayer switchPlayer = new AudioPlayer(false, ResourceHandler.getResourcePath("sound", "switchSound"));	
	
	/*Panels and Labels*/
	private JPanel roleSelect = new JPanel();
	private JPanel roleInfo = new JPanel();
	private JLabel selectFocusLabel = new JLabel(selectFocusImage);
	private JLabel taskFocusLabel = new JLabel(taskFocusImage);
	private JLabel infoFocusLabel = new JLabel(infoFocusImage);
	private JLabel skillFocusLabel = new JLabel(skillFocusImage);
	
	private JLabel modelLabel = new JLabel(modelImage);
	private JLabel modelCLabel = new JLabel(modelCImage);
	private JLabel modelDLabel = new JLabel(modelDImage);
	private JLabel modelLLabel = new JLabel(modelLImage);
	private JLabel modelRLabel = new JLabel(modelRImage);
	private JLabel modelULabel = new JLabel(modelUImage);
	
	private JLabel modelCFocus = new JLabel();
	private JLabel modelDFocus = new JLabel();
	private JLabel modelLFocus = new JLabel();
	private JLabel modelRFocus = new JLabel();
	private JLabel modelUFocus = new JLabel();
	
	private JLabel[] wordsLabel = new JLabel[6];
	private JLabel wordLabel = new JLabel();
	private JLabel gridLabel = new JLabel(gridImage);
	private JLabel gridFocusLabel = new JLabel(gridFocusImage);
	
	private int curItemIndex;
	
	private GameMenu gm;
	private LinkedList<Role> roleList = Game.gsh.getRoleList();
	private Role curRole = roleList.get(0);
	private ArrayList<Skill> roleSkills;
	
	private Equipment[] equipList;
	private LinkedList<Item> backpackEquips = Game.gsh.getEquipmentList();
	private LinkedList<Equipment> backpackHeadEquips = new LinkedList<Equipment>();
	private LinkedList<Equipment> backpackChestEquips = new LinkedList<Equipment>();
	private LinkedList<Equipment> backpackLeftEquips = new LinkedList<Equipment>();
	private LinkedList<Equipment> backpackRightEquips = new LinkedList<Equipment>();
	private LinkedList<Equipment> backpackFootEquips = new LinkedList<Equipment>();
	
	/*Modes and CurrentPlaces*/
	private final int SELECT_MODE = 0;
	private final int TASK_MODE = 1;
	private final int INFO_MODE = 2;
	private final int EQUIP_MODE = 3;
	private final int SKILL_MODE = 4;
	private int mode = SELECT_MODE;
	private int curSelect = 0;
	private int curTask = 0;
	private int curInfo = 0;
	private int curEquip = 0; /*left*/
	private int curEquip2 = 0; /*right*/
	private int curSkill = 0;
	
	public RoleMenu(GameMenu gm){
		
		this.gm = gm;
		this.setOpaque(false);
		this.setBounds(0, 0, Game.gui.getWidth(), Game.gui.getHeight());
		this.setLayout(null);
		
		initialLayout();
		initialRoleSelect();
		initialTaskSelect();
		Game.gui.getGlassPane().addKeyListener(this);
		requestFocus();
	}
	
	public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(selectBG, roleSelectX, roleSelectY, null); 
		g.drawImage(infoBG, roleInfoX, roleInfoY, null); 
    }
	
	public void initialLayout(){
		roleSelect.setBounds(roleSelectX, roleSelectY, roleSelectWidth, roleSelectHeight);
		roleSelect.setLayout(null);
		roleSelect.setOpaque(false);
		this.add(roleSelect, 0);
		
		roleInfo.setBounds(roleInfoX, roleInfoY, roleInfoWidth, roleInfoHeight);
		roleInfo.setFont(new Font("Trebuchet MS", Font.BOLD, 30));
		roleInfo.setLayout(null);
		roleInfo.setOpaque(false);
		this.add(roleInfo, 0);
	}
	
	public void initialRoleSelect(){
		for(int i=0; i<roleList.size(); i++){
			JLabel roleSelectImage = new JLabel(new ImageIcon( Drawer.getResizedImg(roleList.get(i).getPortrait().getImage(), roleSelectImageWidth, roleSelectImageHeight) ));
			roleSelectImage.setBounds(roleSelectImageX, roleSelectImageY + i*roleSelectImageHeight, roleSelectImageWidth, roleSelectImageHeight);
			roleSelect.add(roleSelectImage);
		}
		selectFocusLabel.setBounds(roleSelectImageX , roleSelectImageY, roleSelectImageWidth, roleSelectImageHeight);
		roleSelect.add(selectFocusLabel, 0);
		this.repaint();
	}
	
	public void initialTaskSelect(){
		System.out.println("roleInfoX:"+roleInfo.getX()+" roleInfoY:"+roleInfo.getY());
		for(int i=0; i<taskAmount; i++){
			JLabel task = new JLabel();
			if( i==0 ) task.setText("Info");
			else if( i==1 ) task.setText("Equipment");
			else if( i==2 ) task.setText("Skill");
			task.setBounds( taskX+taskWidth*i, taskY, taskWidth, taskHeight);
			task.setFont( new Font("FELIX TITLING", Font.BOLD, 40) );
			task.setForeground(Color.DARK_GRAY);
			task.setHorizontalAlignment(JLabel.CENTER);
			roleInfo.add(task);
			System.out.println("taskXalignment:"+task.getAlignmentX());
		}
		taskFocusLabel.setBounds(taskX , taskY, taskWidth, taskHeight);
		roleInfo.add(taskFocusLabel, 0);
		taskFocusLabel.setVisible(false);
		this.repaint();
	}
	
	public void initialRoleInfo( Role role ){
		JLabel[] roleInfoList = new JLabel[roleInfoNum*2];
		for(int i=0; i<roleInfoNum*2; i++){
			roleInfoList[i] = new JLabel();
			if( i==0 ) roleInfoList[i].setText("Name:");
			else if( i==1 ) roleInfoList[i].setText("HP:");
			else if( i==2 ) roleInfoList[i].setText("MP:");
			else if( i==3 ) roleInfoList[i].setText("Level:");
			else if( i==4 ) roleInfoList[i].setText("Exp:");
			else if( i==5 ) roleInfoList[i].setText("Status:");
			else if( i==6 ) roleInfoList[i].setText("Atk:");
			else if( i==7 ) roleInfoList[i].setText("Def:");
			else if( i==8 ) roleInfoList[i].setText("Spd:");
			
			else if( i==9 ) roleInfoList[i].setText(role.getName());
			else if( i==10 ) roleInfoList[i].setText(role.getCurHP()+"/"+role.getHP()+"");
			else if( i==11 ) roleInfoList[i].setText(role.getCurMP()+"/"+role.getMP()+"");
			else if( i==12 ) roleInfoList[i].setText(role.getLevel()+"");
			else if( i==13 ) roleInfoList[i].setText(role.getCurExp()+"/"+role.getExp()+"");
			else if( i==14 ) roleInfoList[i].setText(role.getStatusName()+"");
			else if( i==15 ) roleInfoList[i].setText(role.getAtk()+"");
			else if( i==16 ) roleInfoList[i].setText(role.getDef()+"");
			else if( i==17 ) roleInfoList[i].setText(role.getSpd()+"");
			
			if(i < roleInfoNum){
				roleInfoList[i].setBounds( taskX+infoWidth/4, taskY+taskHeight+infoHeight*i, infoWidth/4, infoHeight);
				roleInfoList[i].setFont( new Font("Footlight MT Light", Font.BOLD, 25) );
				roleInfoList[i].setForeground(Color.DARK_GRAY);
			}
			else{
				roleInfoList[i].setBounds( taskX+infoWidth/2, taskY+taskHeight+infoHeight*(i-roleInfoNum), infoWidth/4, infoHeight);
				roleInfoList[i].setFont( new Font("Footlight MT Light", Font.BOLD, 25) );
				roleInfoList[i].setForeground(Color.MAGENTA);
			}
			
			roleInfoList[i].setHorizontalAlignment(JLabel.CENTER);
			roleInfo.add(roleInfoList[i]);
		}
		roleInfo.add(infoFocusLabel, 0);
		this.repaint();
	}
	
	public void initialRoleSkill( Role role ){
		roleSkills = role.getSkills();
		int roleSkillNum = roleSkills.size();
		JLabel[] roleSkillList = new JLabel[roleSkillNum];
		for(int i=0; i<roleSkillNum; i++){
			roleSkillList[i] = new JLabel();
			roleSkillList[i].setText(roleSkills.get(i).getName());
			roleSkillList[i].setBounds(taskX, taskY+taskHeight+skillHeight*i, skillWidth, skillHeight);
			roleSkillList[i].setFont( new Font("Footlight MT Light", Font.BOLD, 35) );
			roleSkillList[i].setForeground(Color.MAGENTA);
			roleSkillList[i].setHorizontalAlignment(JLabel.CENTER);
			roleInfo.add(roleSkillList[i]);
		}
		skillFocusLabel.setBounds(taskX , taskY+taskHeight, skillWidth, skillHeight);
		roleInfo.add(skillFocusLabel, 0);
		
		/*Skill Info*/
		Skill skillSelect = roleSkills.get(curSkill);
		for(int i=0; i<skillInfoNum*2; i++){
			skillInfoList[i] = new JLabel();
			if(i == 0) skillInfoList[i].setText("Description:");
			else if(i == 1) skillInfoList[i].setText("MpCost:");
			else if(i == 2) skillInfoList[i].setText("Target:");
			else if(i == 3) skillInfoList[i].setText("Type:");
			else if(i == 4) skillInfoList[i].setText("Amount:");
			else if(i == 5) skillInfoList[i].setText("HitRate:");
			else if(i == 6) skillInfoList[i].setText("StatusType:");
			else if(i == 7) skillInfoList[i].setText("Status:");
			else if(i == 8) skillInfoList[i].setText("StatusHitRate:");
			else if(i == 9) skillInfoList[i].setText(skillSelect.getDescription());
			else if(i == 10) skillInfoList[i].setText(String.valueOf(skillSelect.getMpCost()));
			else if(i == 11) skillInfoList[i].setText(skillSelect.getTarget());
			else if(i == 12) skillInfoList[i].setText(skillSelect.getType());
			else if(i == 13) skillInfoList[i].setText(String.valueOf(skillSelect.getRealAmount()));
			else if(i == 14) skillInfoList[i].setText(String.valueOf(skillSelect.getHitRate()));
			else if(i == 15) skillInfoList[i].setText(skillSelect.getStatusType());
			else if(i == 16) skillInfoList[i].setText(skillSelect.getStatus());
			else if(i == 17) skillInfoList[i].setText(String.valueOf(skillSelect.getStatusHitRate()));
			
			if(i < skillInfoNum){
				skillInfoList[i].setBounds(skillInfoX+skillInfoWidth/4, skillInfoY+skillInfoHeight*i, skillInfoWidth/4, skillInfoHeight);
				skillInfoList[i].setFont( new Font("Footlight MT Light", Font.BOLD, 25) );
				skillInfoList[i].setForeground(Color.DARK_GRAY);
			}
			else{
				skillInfoList[i].setBounds(skillInfoX+skillInfoWidth/2, skillInfoY+skillInfoHeight*(i-skillInfoNum), skillInfoWidth/4, skillInfoHeight);
				skillInfoList[i].setFont( new Font("Footlight MT Light", Font.BOLD, 25) );
				skillInfoList[i].setForeground(Color.MAGENTA);
			}
			
			skillInfoList[i].setHorizontalAlignment(JLabel.CENTER);
			roleInfo.add(skillInfoList[i]);
		}
		this.repaint();
	}
	
	public void updateSkillInfo(){
		Skill skillSelect = roleSkills.get(curSkill);
		for(int i=9; i<skillInfoNum*2; i++){
			if(i == 9) skillInfoList[i].setText(skillSelect.getDescription());
			else if(i == 10) skillInfoList[i].setText(String.valueOf(skillSelect.getMpCost()));
			else if(i == 11) skillInfoList[i].setText(skillSelect.getTarget());
			else if(i == 12) skillInfoList[i].setText(skillSelect.getType());
			else if(i == 13) skillInfoList[i].setText(String.valueOf(skillSelect.getRealAmount()));
			else if(i == 14) skillInfoList[i].setText(String.valueOf(skillSelect.getHitRate()));
			else if(i == 15) skillInfoList[i].setText(skillSelect.getStatusType());
			else if(i == 16) skillInfoList[i].setText(skillSelect.getStatus());
			else if(i == 17) skillInfoList[i].setText(String.valueOf(skillSelect.getStatusHitRate()));
		}
		this.repaint();
	}
	
	public void initialRoleEquip( Role role ){
		/*Set Model*/
		modelULabel.setBounds(modelX, modelY, modelWidth, modelHeight);
		modelLLabel.setBounds(modelX, modelY, modelWidth, modelHeight);
		modelCLabel.setBounds(modelX, modelY, modelWidth, modelHeight);
		modelDLabel.setBounds(modelX, modelY, modelWidth, modelHeight);
		modelRLabel.setBounds(modelX, modelY, modelWidth, modelHeight);
		
		/*Set Word*/
		wordLabel.setBounds(wordX, wordY, wordWidth, wordHeight);
		wordLabel.setForeground(Color.MAGENTA);
		wordLabel.setHorizontalAlignment(JLabel.CENTER);
		
		for(int i=0;i<6;i++){
			wordsLabel[i] = new JLabel();
			wordsLabel[i].setBounds(wordsX, wordsY+i*wordsHeight, wordsWidth, wordsHeight);
			if(i == 0)
				wordsLabel[i].setFont( new Font("Footlight MT Light", Font.BOLD, 20) );
			else
				wordsLabel[i].setFont( new Font("Footlight MT Light", Font.PLAIN, 20) );	
			wordsLabel[i].setForeground(Color.MAGENTA);
			wordsLabel[i].setHorizontalAlignment(JLabel.CENTER);
		}
		
		/*Set Backpack Equipment*/
		gridLabel.setBounds(gridX, gridY, gridWidth, gridHeight);
		gridFocusLabel.setBounds(firstGridX, firstGridY, singleGridWidth, singleGridHeight);
		
	}
	
	public void updateEquipInfo( Role role ){
		
		/*Set Equipment Info*/
		roleInfo.add(gridLabel, 0);
		gridFocusLabel.setLocation(firstGridX+curEquip2%4*singleGridMoveWidth, firstGridY+curEquip2/4*singleGridMoveHeight);
		if(mode == EQUIP_MODE)
			roleInfo.add(gridFocusLabel, 0);
		
		/*Set Model and Words and Equipments*/
		Equipment e;
		backpackEquips = Game.gsh.getEquipmentList();
		backpackHeadEquips = new LinkedList<Equipment>();
		backpackChestEquips = new LinkedList<Equipment>();
		backpackLeftEquips = new LinkedList<Equipment>();
		backpackRightEquips = new LinkedList<Equipment>();
		backpackFootEquips = new LinkedList<Equipment>();
		for(int i=0;i<backpackEquips.size();i++){
			e = (Equipment)backpackEquips.get(i);
			if(e.getPosition() == Equipment.HEAD_POS)
				backpackHeadEquips.add(e);
			else if(e.getPosition() == Equipment.CHEST_POS)
				backpackChestEquips.add(e);
			else if(e.getPosition() == Equipment.RIGHTHAND_POS)
				backpackRightEquips.add(e);
			else if(e.getPosition() == Equipment.LEFTHAND_POS)
				backpackLeftEquips.add(e);
			else if(e.getPosition() == Equipment.FOOT_POS)
				backpackFootEquips.add(e);
		}
		
		LinkedList<Equipment> l = new LinkedList<Equipment>();
		if(curEquip == 0){
			roleInfo.add(modelULabel, 0);
			wordLabel.setText("Head");
			l = backpackHeadEquips;
		}
		else if(curEquip == 1){
			roleInfo.add(modelLLabel, 0);
			wordLabel.setText("LeftHand");
			l = backpackLeftEquips;
		}
		else if(curEquip == 2){
			roleInfo.add(modelCLabel, 0);
			wordLabel.setText("Body");
			l = backpackChestEquips;
		}
		else if(curEquip == 3){
			roleInfo.add(modelDLabel, 0);
			wordLabel.setText("Foot");
			l = backpackFootEquips;
		}
		else if(curEquip == 4){
			roleInfo.add(modelRLabel, 0);
			wordLabel.setText("RightHand");
			l = backpackRightEquips;
		}
		
		/*Add Words*/
		if(mode == EQUIP_MODE){
			if(curEquip2 < l.size()){
				wordsLabel[0].setText(l.get(curEquip2).getName());
				wordsLabel[1].setText("HP:" + l.get(curEquip2).getHpValue());
				wordsLabel[2].setText("MP:" + l.get(curEquip2).getMpValue());
				wordsLabel[3].setText("ATK:" + l.get(curEquip2).getAtkValue());
				wordsLabel[4].setText("DEF:" + l.get(curEquip2).getDefValue());
				wordsLabel[5].setText("SPD:" + l.get(curEquip2).getSpdValue());
			}
			else{
				wordsLabel[0].setText("Empty");
				wordsLabel[1].setText("");
				wordsLabel[2].setText("");
				wordsLabel[3].setText("");
				wordsLabel[4].setText("");
				wordsLabel[5].setText("");
			}
		}
		else if(mode == TASK_MODE){
			wordsLabel[0].setText("Total Increase");
			wordsLabel[1].setText("HP:" + role.getHpGain());
			wordsLabel[2].setText("MP:" + role.getMpGain());
			wordsLabel[3].setText("ATK:" + role.getAtkGain());
			wordsLabel[4].setText("DEF:" + role.getDefGain());
			wordsLabel[5].setText("SPD:" + role.getSpdGain());
		}
		for(int i=0;i<6;i++){
			roleInfo.add(wordsLabel[i]);
		}
		
		wordLabel.setFont( new Font("Footlight MT Light", Font.BOLD, 45) );/*TASK_MODE*/
		if(mode == EQUIP_MODE){
			wordLabel.setFont( new Font("Footlight MT Light", Font.BOLD, 30) );
			if(curEquip2 < l.size())
				wordLabel.setText(l.get(curEquip2).getUtility());
			else
				wordLabel.setText("");
		}
		roleInfo.add(wordLabel);	
		
		int row, column;
		JLabel equip;	
		for(int i=0;i<l.size();i++){
			row = i/4;
			column = i%4;
			equip = new JLabel(new ImageIcon(Drawer.getResizedImg(l.get(i).getPureImage(), singleGridWidth, singleGridHeight)));
			equip.setBounds(firstGridX+column*singleGridMoveWidth, firstGridY+row*singleGridMoveHeight, singleGridWidth, singleGridHeight);
			roleInfo.add(equip, 0);
		}
		
		/*Set Role Equipment*/
		equipList = role.getEquip();
		if(equipList[0] != null){
			modelUFocus.setBounds(modelUX, modelUY, modelImageWidth, modelImageHeight);
			modelUFocus.setIcon(new ImageIcon(Drawer.getResizedImg(equipList[Equipment.HEAD_POS].getPureImage(), modelImageWidth, modelImageHeight)));
			roleInfo.add(modelUFocus, 0);
		}
		if(equipList[1] != null){
			modelCFocus.setBounds(modelCX, modelCY, modelImageWidth, modelImageHeight);
			modelCFocus.setIcon(new ImageIcon(Drawer.getResizedImg(equipList[Equipment.CHEST_POS].getPureImage(), modelImageWidth, modelImageHeight)));
			roleInfo.add(modelCFocus, 0);
		}
		if(equipList[2] != null){
			modelRFocus.setBounds(modelRX, modelRY, modelImageWidth, modelImageHeight);
			modelRFocus.setIcon(new ImageIcon(Drawer.getResizedImg(equipList[Equipment.RIGHTHAND_POS].getPureImage(), modelImageWidth, modelImageHeight)));
			roleInfo.add(modelRFocus, 0);
		}
		if(equipList[3] != null){
			System.out.println("SomeThing at Lefthand");
			modelLFocus.setBounds(modelLX, modelLY, modelImageWidth, modelImageHeight);
			modelLFocus.setIcon(new ImageIcon(Drawer.getResizedImg(equipList[Equipment.LEFTHAND_POS].getPureImage(), modelImageWidth, modelImageHeight)));
			roleInfo.add(modelLFocus, 0);
		}
		if(equipList[4] != null){
			modelDFocus.setBounds(modelDX, modelDY, modelImageWidth, modelImageHeight);
			modelDFocus.setIcon(new ImageIcon(Drawer.getResizedImg(equipList[Equipment.FOOT_POS].getPureImage(), modelImageWidth, modelImageHeight)));
			roleInfo.add(modelDFocus, 0);
		}
		
		roleInfo.repaint();
	}
	
	public void updateFocus(){
		if(mode == SELECT_MODE){
			selectFocusLabel.setBounds(roleSelectImageX, roleSelectImageY+curSelect*roleSelectImageHeight, roleSelectImageWidth, roleSelectImageHeight);
			selectFocusLabel.setVisible(true);
			taskFocusLabel.setVisible(false);
			skillFocusLabel.setVisible(false);
		}
		else if(mode == TASK_MODE || mode == EQUIP_MODE){
			taskFocusLabel.setBounds(taskX+taskWidth*curTask, taskY, taskWidth, taskHeight);
			taskFocusLabel.setVisible(true);
			infoFocusLabel.setVisible(false);
			if(curTask != 2)
				skillFocusLabel.setVisible(false);
			else{
				skillFocusLabel.setVisible(true);
				skillFocusLabel.setBounds(taskX, taskY+taskHeight+skillHeight*curSkill, skillWidth, skillHeight);
			}
		}
	}
	
	public void run(){
	}
	
	public void keyTyped(KeyEvent e){
	}
	
	public synchronized void keyPressed(KeyEvent e){
		int keyCode = e.getKeyCode();
		System.out.println("In bag: " + keyCode);
		if(keyCode == Game.ESC){
			Thread soundThread = new Thread(switchPlayer);
			soundThread.start();
			if(mode == SELECT_MODE){
				((JPanel)Game.gui.getGlassPane()).remove(this);
				Game.gui.getGlassPane().removeKeyListener(this);
				Game.gui.getGlassPane().addKeyListener(gm);
				gm.setVisible(true);
				Game.gui.repaint();
			}
			else if(mode == TASK_MODE){
				mode = SELECT_MODE;
				roleInfo.removeAll();
				initialTaskSelect();
				curTask = 0;
				updateFocus();
			}
			else if(mode == INFO_MODE){
			}
			else if(mode == EQUIP_MODE){
				roleInfo.removeAll();
				initialTaskSelect();
				mode = TASK_MODE;
				updateEquipInfo(curRole);
			}
			else if(mode == SKILL_MODE){
			}
			updateFocus();
		}
		else if(keyCode == Game.ENTER){
			if(mode == SELECT_MODE){
				Thread soundThread = new Thread(switchPlayer);
				soundThread.start();
				curRole = roleList.get(curSelect);
				mode = TASK_MODE;
				initialRoleInfo(curRole);
			}
			else if(mode == TASK_MODE){
				if(curTask == 0){
				}
				else if(curTask == 1){
					Thread soundThread = new Thread(switchPlayer);
					soundThread.start();
					curEquip2 = 0;
					mode = EQUIP_MODE;
					roleInfo.removeAll();
					initialTaskSelect();
					updateEquipInfo(curRole);
				}
				else if(curTask == 2){
				}
			}
			else if(mode == INFO_MODE){
			}
			else if(mode == EQUIP_MODE){
				LinkedList<Equipment> l = new LinkedList<Equipment>();
				int pos = Equipment.HEAD_POS;
				if(curEquip == 0){
					l = backpackHeadEquips;
					pos = Equipment.HEAD_POS;
				}
				else if(curEquip == 1){
					l = backpackLeftEquips;
					pos = Equipment.LEFTHAND_POS;
				}
				else if(curEquip == 2){
					l = backpackChestEquips;
					pos = Equipment.CHEST_POS;
				}
				else if(curEquip == 3){
					l = backpackFootEquips;
					pos = Equipment.FOOT_POS;
				}
				else if(curEquip == 4){
					l = backpackRightEquips;
					pos = Equipment.RIGHTHAND_POS;
				}
					
				Thread soundThread = new Thread(switchPlayer);
				soundThread.start();
				
				/*Drop Equipment, Add in Backpack*/
				if(equipList[pos] != null){
					Game.gsh.addItem(equipList[pos]);
					curRole.dropEquip(pos);
				}
				/*Add Equipment, Drop from Backpack*/
				if(curEquip2 < l.size()){
					Game.gsh.removeItem(l.get(curEquip2));
					curRole.addEquip(l.get(curEquip2));
				}
				
				roleInfo.removeAll();
				initialTaskSelect();
				mode = TASK_MODE;
				updateEquipInfo(curRole);
			}
			else if(mode == SKILL_MODE){
			}
			updateFocus();
		}
		else if(keyCode == Game.UP){
			if(mode == SELECT_MODE){
				if(curSelect > 0){
					Thread soundThread = new Thread(switchPlayer);
					soundThread.start();
					curSelect--;
				}
			}
			else if(mode == TASK_MODE){
				if(curTask == 2){
					if(curSkill > 0){
						Thread soundThread = new Thread(switchPlayer);
						soundThread.start();
						curSkill--;
						updateSkillInfo();
					}
				}
				else if(curTask == 1){
					Thread soundThread = new Thread(switchPlayer);
					soundThread.start();
					if(curEquip > 0){
						curEquip--;
					}
					else if(curEquip == 0){
						curEquip = equipTypeNum-1;
					}
					roleInfo.removeAll();
					initialTaskSelect();
					updateEquipInfo(curRole);
				}
			}
			else if(mode == INFO_MODE){
			}
			else if(mode == EQUIP_MODE){
				Thread soundThread = new Thread(switchPlayer);
				soundThread.start();
				if(curEquip2/4 != 0){
					curEquip2 -= 4;
				}
				else{
					curEquip2 += 12;
				}
				roleInfo.removeAll();
				initialTaskSelect();
				updateEquipInfo(curRole);
			}
			else if(mode == SKILL_MODE){
			}
			updateFocus();
		}
		else if(keyCode == Game.DOWN){
			if(mode == SELECT_MODE){
				if(curSelect < roleList.size()-1){
					Thread soundThread = new Thread(switchPlayer);
					soundThread.start();
					curSelect++;
				}
			}
			else if(mode == TASK_MODE){
				if(curTask == 2){
					if(curSkill < roleSkills.size()-1){
						Thread soundThread = new Thread(switchPlayer);
						soundThread.start();
						curSkill++;
						updateSkillInfo();
					}
				}
				else if(curTask == 1){
					Thread soundThread = new Thread(switchPlayer);
					soundThread.start();
					if(curEquip < equipTypeNum - 1){
						curEquip++;
					}
					else if(curEquip == equipTypeNum-1){
						curEquip = 0;
					}
					roleInfo.removeAll();
					initialTaskSelect();
					updateEquipInfo(curRole);
				}
			}
			else if(mode == INFO_MODE){
			}
			else if(mode == EQUIP_MODE){
				Thread soundThread = new Thread(switchPlayer);
				soundThread.start();
				if(curEquip2/4 != 3){
					curEquip2 += 4;
				}
				else{
					curEquip2 -= 12;
				}
				roleInfo.removeAll();
				initialTaskSelect();
				updateEquipInfo(curRole);
			}
			else if(mode == SKILL_MODE){
			}
			updateFocus();
		}
		else if(keyCode == Game.LEFT){
			if(mode == SELECT_MODE){
			}
			else if(mode == TASK_MODE){
				if(curTask > 0){
					Thread soundThread = new Thread(switchPlayer);
					soundThread.start();
					curTask--;
					if(curTask == 0){
						roleInfo.removeAll();
						initialTaskSelect();
						initialRoleInfo(curRole);
					}
					else if(curTask == 1){
						curEquip = 0;
						roleInfo.removeAll();
						initialTaskSelect();
						initialRoleEquip(curRole);
						updateEquipInfo(curRole);
					}
				}
				else if(curTask == 0){
					Thread soundThread = new Thread(switchPlayer);
					soundThread.start();
					mode = SELECT_MODE;
					roleInfo.removeAll();
					initialTaskSelect();
					curTask = 0;
					updateFocus();
				}
			}
			else if(mode == INFO_MODE){
			}
			else if(mode == EQUIP_MODE){
				Thread soundThread = new Thread(switchPlayer);
				soundThread.start();
				if(curEquip2%4 != 0){
					curEquip2 -= 1;
				}
				else{
					curEquip2 += 3;
				}
				roleInfo.removeAll();
				initialTaskSelect();
				updateEquipInfo(curRole);
			}
			else if(mode == SKILL_MODE){
			}
			updateFocus();
		}
		else if(keyCode == Game.RIGHT){
			if(mode == SELECT_MODE){
				Thread soundThread = new Thread(switchPlayer);
				soundThread.start();
				curRole = roleList.get(curSelect);
				mode = TASK_MODE;
				initialRoleInfo(curRole);
			}
			else if(mode == TASK_MODE){
				if(curTask < taskAmount-1){
					Thread soundThread = new Thread(switchPlayer);
					soundThread.start();
					curTask++;
					if(curTask == 1){
						curEquip = 0;
						roleInfo.removeAll();
						initialTaskSelect();
						initialRoleEquip(curRole);
						updateEquipInfo(curRole);
					}
					else if(curTask == 2){
						curSkill = 0;
						roleInfo.removeAll();
						initialTaskSelect();
						initialRoleSkill(curRole);
					}
				}
			}
			else if(mode == INFO_MODE){
			}
			else if(mode == EQUIP_MODE){
				Thread soundThread = new Thread(switchPlayer);
				soundThread.start();
				if(curEquip2%4 != 3){
					curEquip2 += 1;
				}
				else{
					curEquip2 -= 3;
				}
				roleInfo.removeAll();
				initialTaskSelect();
				updateEquipInfo(curRole);
			}
			else if(mode == SKILL_MODE){
			}
			updateFocus();
		}
	}
		
	public void keyReleased(KeyEvent e){
	}
	
}