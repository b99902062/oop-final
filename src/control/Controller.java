package rpg.control;

import java.lang.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;

import rpg.map.*;
import rpg.game.*;
import rpg.draw.*;
import rpg.resource.*;
import rpg.mode.*;
import rpg.character.*;
import rpg.item.*;

/**
 * A class that handles the control for maps and user actions
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Controller implements Runnable, KeyListener{

	private JLabel[][] block;
	private int rowNum;
	private int colNum;
	private int offsetRow;
	private int offsetCol;
	private int curRow;
	private int curCol;
	private Map map;
	private boolean isKilled;
	private JPanel panel;
	private JLabel charLabel;
	private int stepCount;
	private double battleRate = 1;
	private boolean isMaze;
	private boolean toFightBoss;
	
	private JPanel glassMenu;
	private GameMenu menu = null;
	
	private String lookDir;
	private NPC[] npcList;
	private boolean isDialogOn;
	private int contentCount;
	private int curContent;
	private String[] contentList;
	private JLabel words = new JLabel();
	private JLabel portrait = new JLabel();
	private NPC curNPC;
	
	private ArrayList<String> infoToShow;
	private boolean hasInfoToShow;
	private int infoIndex;
	
	private boolean isBoundWordsOn;
	
	private Timer timer;
	
	HashSet<Integer> pressedKeys = new HashSet<Integer>();
	
	private JLabel weatherLabel = new JLabel();
	private boolean isWeatherChange;
	private Thread weatherThread;
	private WeatherController wc;
	
	private boolean menuExit = false;
	public static final int MOVE_AMOUNT = 10;
	
	public Controller(JPanel _panel, JLabel[][] _block, Map _map, JLabel _charLabel, NPC[] npcs){
		try{
			rowNum = _block.length;
			colNum = _block[0].length;
			block = _block;
			map = new Map(_map);
			panel = _panel;
			charLabel = _charLabel;
			npcList = npcs;
			
			weatherLabel.setBounds(0, 0, Game.gui.getWidth(), Game.gui.getHeight());
			wc = new WeatherController(weatherLabel);
			
			curRow = Game.gsh.getCurPos()[0] * Drawer.LARGE_BLOCK_SIZE;
			curCol = Game.gsh.getCurPos()[1] * Drawer.LARGE_BLOCK_SIZE;
			Game.gui.addKeyListener(this);
			timer = new Timer(30, new ActionListener(){
				public void actionPerformed(ActionEvent e){
					if(!pressedKeys.isEmpty()){
                        if(pressedKeys.iterator().hasNext()){
							doMove(pressedKeys.iterator().next());
						}
                    } 
				}
			});
			timer.start();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public synchronized boolean waitForOut(String type){
		try{
			if(Game.gsh.getCurMap().getType().equals("Maze"))
				isMaze = true;
			wait();
		}catch(Exception e){
			e.printStackTrace();
		}
		Game.gui.removeKeyListener(this);
		isKilled = true;
		timer.stop();
		wc.stop();
		
		if(menuExit == true || Game.gsh.getBattle()) return true;
		
		if(Game.gsh.getCurMap().getType().equals(type))
			return false;
		else
			return true;
	}
	
	public synchronized void notifyWaitForOut(){
		menuExit = true;
		notify();
	}
	
	public boolean getMenuExit(){
		return menuExit;
	}
	
	public void run(){
		try{
			while(!isKilled){
				for(int row=0; row<rowNum; row++)
					for(int col=0; col<colNum; col++)
						if(map.getElement(row, col) != null && map.getElement(row, col).getListLength() > 1){
							block[row][col].setIcon(map.getNextScaledFrame(row, col, Drawer.LARGE_BLOCK_SIZE, Drawer.LARGE_BLOCK_SIZE));
						}
				charLabel.repaint();
				Thread.sleep(1000);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void keyTyped(KeyEvent e){
	}
	
	public void keyPressed(KeyEvent e){
		int keyCode = e.getKeyCode();
		if(keyCode == Game.UP){
			pressedKeys.add(keyCode);
		}
		else if(keyCode == Game.DOWN){
			pressedKeys.add(keyCode);
		}
		else if(keyCode == Game.LEFT){
			pressedKeys.add(keyCode);
		}
		else if(keyCode == Game.RIGHT){
			pressedKeys.add(keyCode);
		}
		else if(keyCode == Game.SPACE){
			checkEvent();
		}
		else if(keyCode == Game.ESC){
			saveCaptureImage();
			showMenu();
		}
		else if(keyCode == Game.R || keyCode == Game.S){
			if(Game.gsh.getCurMapId() != 4)
				return;
			if(!isWeatherChange){
				Game.gui.getContentPane().add(weatherLabel, 0);
				weatherThread = new Thread(wc);
				if(keyCode == Game.R)
					wc.wakeUp(WeatherController.TYPE_RAIN);
				else if(keyCode == Game.S)
					wc.wakeUp(WeatherController.TYPE_SNOW);
				weatherThread.start();
				isWeatherChange = true;
			}
			else{
				wc.stop();
				Game.gui.getContentPane().remove(weatherLabel);
				Game.gui.getContentPane().repaint();
				isWeatherChange = false;
			}
		}
	}
	
	public void keyReleased(KeyEvent e){
		pressedKeys.remove(e.getKeyCode());
	}
	
	private void saveCaptureImage(){
		try{
			Robot robot = new Robot();
			Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
			Rectangle rect = new Rectangle(0, 0, d.width, d.height);
			BufferedImage image = robot.createScreenCapture(rect);
			Game.gsh.setCaptureImage(new ImageIcon(Drawer.getResizedImg(image, 200, 200)));
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private synchronized void doMove(int dir){
		if(dir == Game.UP){
			lookDir = "Up";
			if(stepCount > 3)
				charLabel.setIcon(Game.gsh.getCharImage()[9]);
			else
				charLabel.setIcon(Game.gsh.getCharImage()[11]);
			stepCount = (stepCount + 1) % 6;
			
			if(!canMove((curRow - MOVE_AMOUNT), curCol))
				return;
			
			curRow -= MOVE_AMOUNT;
			panel.setLocation(panel.getX(), panel.getY() + MOVE_AMOUNT);
			setPos(curRow, curCol);
			checkLeave();
			checkBattle();
		}
		else if(dir == Game.DOWN){
			lookDir = "Down";
			if(stepCount > 3)
				charLabel.setIcon(Game.gsh.getCharImage()[0]);
			else
				charLabel.setIcon(Game.gsh.getCharImage()[2]);
			stepCount = (stepCount + 1) % 6;
			if(!canMove((curRow + MOVE_AMOUNT), curCol))
				return;
			curRow += MOVE_AMOUNT;
			panel.setLocation(panel.getX(), panel.getY() - MOVE_AMOUNT);
			setPos(curRow, curCol);
			checkLeave();
			checkBattle();
		}
		else if(dir == Game.LEFT){
			lookDir = "Left";
			if(stepCount > 3)
				charLabel.setIcon(Game.gsh.getCharImage()[3]);
			else
				charLabel.setIcon(Game.gsh.getCharImage()[5]);
			stepCount = (stepCount + 1) % 6;
			
			if(!canMove(curRow, (curCol - MOVE_AMOUNT)))
				return;
			
			curCol -= MOVE_AMOUNT;
			panel.setLocation(panel.getX() + MOVE_AMOUNT, panel.getY());
			setPos(curRow, curCol);
			checkLeave();
			checkBattle();
		}
		else if(dir == Game.RIGHT){
			lookDir = "Right";
			if(stepCount > 3)
				charLabel.setIcon(Game.gsh.getCharImage()[6]);
			else
				charLabel.setIcon(Game.gsh.getCharImage()[8]);
			stepCount = (stepCount + 1) % 6;
				
			if(!canMove(curRow, (curCol + MOVE_AMOUNT)))
				return;
			
			curCol += MOVE_AMOUNT;
			
			panel.setLocation(panel.getX() - MOVE_AMOUNT, panel.getY());
			setPos(curRow, curCol);
			checkLeave();
			checkBattle();
		}
	}
	
	private void checkLeave(){
		if(checkBound(curRow/Drawer.LARGE_BLOCK_SIZE , curCol/Drawer.LARGE_BLOCK_SIZE)
			&& map.getElement(curRow/Drawer.LARGE_BLOCK_SIZE, curCol/Drawer.LARGE_BLOCK_SIZE) != null
			&& map.getElement(curRow/Drawer.LARGE_BLOCK_SIZE, curCol/Drawer.LARGE_BLOCK_SIZE).isExit()){
				int mapId = map.getElement(curRow/Drawer.LARGE_BLOCK_SIZE, curCol/Drawer.LARGE_BLOCK_SIZE).getNextMapId();
				int nextRow = map.getElement(curRow/Drawer.LARGE_BLOCK_SIZE, curCol/Drawer.LARGE_BLOCK_SIZE).getNextPosRow();
				int nextCol = map.getElement(curRow/Drawer.LARGE_BLOCK_SIZE, curCol/Drawer.LARGE_BLOCK_SIZE).getNextPosCol();
				if(!Game.gsh.isLimitArea(mapId)){
					setMessageBar();
					setMessage(Game.gsh.getRole(Game.gsh.getName()).getPortrait(), Game.gsh.getBoundWords());
					isBoundWordsOn = true;
					return;
				}
				Game.gsh.setLocation(mapId, nextRow, nextCol);
				notify();
		}
	}
	
	private void checkBattle(){
		if(Math.random() < 0.9 || !isMaze)
			return;
		if(Math.random() > battleRate){
			Game.gsh.setBattle(true);
			notify();
		}
		else
			battleRate *= MazeHandler.RISING_RATE;
	}
	
	private synchronized void checkBoss(){
		if(curNPC.isBoss() && curNPC.isBattleEvent(Game.gsh.getEpisodeId(), Game.gsh.getSubEpisodeId()))
			toFightBoss = true;
		else
			toFightBoss = false;
	}
	
	private synchronized void gotoBoss(){
		Game.gsh.setBoss(true);
		Game.gsh.setBattle(true);
		Game.gsh.setBossName(curNPC.getName());
		toFightBoss = false;
		notify();
	}
	
	private void setPos(int row, int col){
		int[] pos = {row/Drawer.LARGE_BLOCK_SIZE, col/Drawer.LARGE_BLOCK_SIZE};
		Game.gsh.setCurPos(pos);
	}
	
	private boolean canMove(int row, int col){
		if(!checkBound(row, col)
			|| isDialogOn
			|| map.getElement(row/Drawer.LARGE_BLOCK_SIZE, col/Drawer.LARGE_BLOCK_SIZE) == null
			|| map.getElement(row/Drawer.LARGE_BLOCK_SIZE, col/Drawer.LARGE_BLOCK_SIZE).isObstacle())
				return false;
		for(int i=0; i<npcList.length; i++){
			if(npcList[i].getRow() == row/Drawer.LARGE_BLOCK_SIZE && npcList[i].getCol() == col/Drawer.LARGE_BLOCK_SIZE)
				return false;
		}
		return true;
	}
	
	private boolean checkBound(double row, double col){
		if(row < 0 || col < 0 || row >= map.getHeight()*Drawer.LARGE_BLOCK_SIZE || col >= map.getWidth()*Drawer.LARGE_BLOCK_SIZE)
			return false;
		return true;
	}
	
	private void showMenu(){
	
		Image menuBgImage = ResourceHandler.getImageByName("background", "menuBG");
		
		menu = new GameMenu(this);
		menu.setImage(Drawer.getResizedImg(menuBgImage, menu.getWidth(), menu.getHeight()));
			
		glassMenu = (JPanel)Game.gui.getGlassPane();
		glassMenu.removeAll();
		glassMenu.add(menu);
		glassMenu.setVisible(true);
		glassMenu.setLayout( null );
		
		glassMenu.addKeyListener(menu);
		glassMenu.requestFocus();
		
		Thread thr = new Thread(menu);
		thr.start();
		
		/*
		if( menu.saveStatus() ){
			Game.gui.requestFocus();
		}
		else if( menu.returnStatus() ){
			Game.gui.requestFocus();
		}
		else if( menu.exitStatus() ){
			Game.gui.requestFocus();
		}*/
	}
	
	private void checkEvent(){
		if(isDialogOn){
			if(isBoundWordsOn){
				isBoundWordsOn = false;
				closeMessageBar();
				return;
			}
			
			if(curContent < contentCount){
				showDialog(curNPC, curContent++);
				return;
			}
			
			if(hasInfoToShow){
				if(infoIndex < infoToShow.size()){
					showInfo(infoIndex++);
					return;
				}
				
				hasInfoToShow = false;
				closeMessageBar();
				return;
			}
			
			checkBoss();
			if(Game.gsh.checkEvent(curNPC))
				infoToShow = collectInfos();
			else
				infoToShow = null;
				
			if(infoToShow != null && infoToShow.size() > 0){
				hasInfoToShow = true;
				infoIndex = 0;
				showInfo(infoIndex++);
			}
			else{
				closeMessageBar();
				if(toFightBoss)
					gotoBoss();
				
				curNPC.resume();
			}
		}
		else{
			for(int i=0; i<npcList.length; i++){
				if((lookDir.equals("Up") && npcList[i].getRow() == (curRow - MOVE_AMOUNT)/Drawer.LARGE_BLOCK_SIZE && npcList[i].getCol() == curCol/Drawer.LARGE_BLOCK_SIZE)
				|| (lookDir.equals("Down") && npcList[i].getRow() == (curRow + MOVE_AMOUNT)/Drawer.LARGE_BLOCK_SIZE && npcList[i].getCol() == curCol/Drawer.LARGE_BLOCK_SIZE)
				|| (lookDir.equals("Left") && npcList[i].getRow() == curRow/Drawer.LARGE_BLOCK_SIZE && npcList[i].getCol() == (curCol - MOVE_AMOUNT)/Drawer.LARGE_BLOCK_SIZE)
				|| (lookDir.equals("Right") && npcList[i].getRow() == curRow/Drawer.LARGE_BLOCK_SIZE && npcList[i].getCol() == (curCol + MOVE_AMOUNT)/Drawer.LARGE_BLOCK_SIZE)){
					callDialog(i, false);
					break;
				}
			}
		}
	}
	
	public void callDialog(int i, boolean isForceTalk){
		curNPC = npcList[i];
		contentCount = npcList[i].getDialogue().getContentCount();
		curContent = 0;
		ArrayList<String> sellList = curNPC.getSellList();
		if(sellList != null && !isForceTalk){
			JPanel glassPane = (JPanel)Game.gui.getGlassPane();
			glassPane.removeAll();
			Game.gui.removeKeyListener(this);
			StoreMenu sm = new StoreMenu(this, sellList, i);
			glassPane.add(sm, 0);
			glassPane.setVisible(true);
			sm.repaint();
			return;
		}
		setMessageBar();
		showDialog(npcList[i], curContent++);
		curNPC.lookDir(lookDir);
		curNPC.pause();
	}
	
	private ArrayList<String> collectInfos(){
		ArrayList<String> infos = new ArrayList<String>();
		ArrayList<Item> list1 = Game.gsh.getGetItemList();
		if(list1 != null)
			for(int i=0; i<list1.size(); i++)
				infos.add(new String("You got " + list1.get(i).getName() + " * " + list1.get(i).getAmount()));
		ArrayList<Item> list2 = Game.gsh.getLoseItemList();
		if(list2 != null)
			for(int i=0; i<list2.size(); i++)
				infos.add(new String("You lose " + list2.get(i).getName() + " * " + list2.get(i).getAmount()));
		ArrayList<Role> list3 = Game.gsh.getGetRoleList();
		if(list3 != null)
			for(int i=0; i<list3.size(); i++)
				infos.add(new String(list3.get(i).getName() + " has joined"));
		ArrayList<Role> list4 = Game.gsh.getLoseRoleList();
		if(list4 != null)
			for(int i=0; i<list4.size(); i++)
				infos.add(new String(list4.get(i).getName() + " has left"));
		return infos;
	}
	
	private void setMessageBar(){
		JPanel gp = (JPanel) Game.gui.getGlassPane();
		gp.setLayout(null);
		BufferedImage img = Drawer.getResizedImg(ResourceHandler.getImageByName("other", "messageBar"), Game.gui.getWidth(), Game.gui.getHeight()/4);
		JLabel testLabel = new JLabel(new ImageIcon(img));
		testLabel.setBounds(0, Game.gui.getHeight()*3/4, Game.gui.getWidth(), Game.gui.getHeight()/4);
		gp.add(testLabel);
		words.setFont(new Font("Trebuchet MS", Font.BOLD, 30));
		words.setForeground(Color.WHITE);
		words.setBounds(Game.gui.getHeight() * 2/5, Game.gui.getHeight()*3/4 + 5, Game.gui.getWidth() * 3 / 5, Game.gui.getHeight()/4);
		gp.add(words, 0);
		portrait.setBounds(20, Game.gui.getHeight()*3/4 + 50, Drawer.PORTRAIT_BLOCK_SIZE, Drawer.PORTRAIT_BLOCK_SIZE);
		gp.add(portrait, 0);
		gp.setVisible(true);
		panel.repaint();
		isDialogOn = true;
	}
	
	private void showDialog(NPC npc, int index){
		ImageIcon img;
		if(Game.gsh.isRole(npc.getDialogue().getWho(index)))
			img = Game.gsh.getRole(npc.getDialogue().getWho(index)).getPortrait();
		else
			img = npc.getPortrait();
		setMessage(img, npc.getDialogue().getContent(index));
	}
	
	private void showInfo(int index){
		setMessage(null, infoToShow.get(index));
	}
	
	private void setMessage(ImageIcon img, String msg){
		portrait.setIcon(img);
		words.setText(msg);
		panel.repaint();
	}
	
	private void closeMessageBar(){
		Game.gui.getGlassPane().setVisible(false);
		isDialogOn = false;
		((JPanel)Game.gui.getGlassPane()).removeAll();
	}
}

class WeatherController implements Runnable{
	private int weatherCount;
	private ArrayList<BufferedImage> rainImgList = new ArrayList<BufferedImage>();
	private ArrayList<BufferedImage> snowImgList = new ArrayList<BufferedImage>();
	private boolean isKilled;
	private JLabel weatherLabel;
	private int type;
	
	public static final int TYPE_RAIN 	= 1;
	public static final int TYPE_SNOW 	= 2;
	
	public WeatherController(JLabel weatherLabel){
		this.weatherLabel = weatherLabel;
		rainImgList.add(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "rain1Image"), Game.gui.getWidth(), Game.gui.getHeight()));
		rainImgList.add(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "rain2Image"), Game.gui.getWidth(), Game.gui.getHeight()));
		snowImgList.add(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "snow1Image"), Game.gui.getWidth(), Game.gui.getHeight()));
		snowImgList.add(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "snow2Image"), Game.gui.getWidth(), Game.gui.getHeight()));
		snowImgList.add(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "snow3Image"), Game.gui.getWidth(), Game.gui.getHeight()));
		type = TYPE_RAIN;
	}
	
	public void run(){
		while(!isKilled){
			ArrayList<BufferedImage> list;
			if(type == TYPE_RAIN)
				list = rainImgList;
			else if(type == TYPE_SNOW)
				list = snowImgList;
			else
				list = rainImgList;
			weatherLabel.setIcon(new ImageIcon(list.get(weatherCount)));
			weatherCount = (weatherCount + 1) % list.size();
			try{
				Thread.sleep(300);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}
	
	public void stop(){
		isKilled = true;
	}
	
	public void wakeUp(int type){
		this.type = type;
		weatherCount = 0;
		isKilled = false;
	}
}