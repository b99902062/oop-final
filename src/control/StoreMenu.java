package rpg.control;

import java.lang.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import java.awt.*;
import java.net.*;

import javax.swing.*;
import javax.imageio.*;

import rpg.draw.*;
import rpg.game.*;
import rpg.character.*;
import rpg.resource.*;
import rpg.map.*;
import rpg.audio.*;
import rpg.status.*;
import rpg.mode.*;
import rpg.item.*;

/**
 * A class that implements the store menu
 * @author  Chander, Moe, William
 * @version 1.0
 */
 
public class StoreMenu extends JPanel implements Serializable, KeyListener{

	private static final long serialVersionUID = 1L;
	private static final int ITEM_INTERVAL = 50;
	
	private AudioPlayer switchPlayer = new AudioPlayer(false, ResourceHandler.getResourcePath("sound", "switchSound"));	
	private JPanel mainPanel = Drawer.drawBackGround(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), Game.gui.getWidth()/3, Game.gui.getHeight()*7/10));
	private JPanel infoPanel = Drawer.drawBackGround(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), Game.gui.getWidth()/2, Game.gui.getHeight()/5));
	private JPanel typePanel = Drawer.drawBackGround(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), (int)(Game.gui.getWidth() * 0.15), (int)(Game.gui.getWidth() * 0.15)));
	private JPanel amountPanel = Drawer.drawBackGround(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), (int)(Game.gui.getWidth() * 0.15), 60));
	private JPanel moneyPanel = Drawer.drawBackGround(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), (int)(Game.gui.getWidth() * 0.15), 60));
	private JPanel actionPanel = Drawer.drawBackGround(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), (int)(Game.gui.getWidth() * 0.15), Game.gui.getHeight()/4 + 20));
	
	/*Role Panel*/
	private final int roleStatusX = Game.gui.getX() + 10;
	private final int roleStatusY = 40;
	private final int roleStatusWidth = Game.gui.getWidth()/4 - 10;
	private final int roleStatusHeight = Game.gui.getHeight()*9/10 + 40;
	private JPanel rolePanel = Drawer.drawBackGround(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), roleStatusWidth, roleStatusHeight));
	
	private ArrayList<JLabel> itemLabelList;
	private ArrayList<JLabel> priceLabelList;
	private JLabel infoLabel = new JLabel();
	private JLabel typeLabel = new JLabel();
	private JLabel moneyLabel = new JLabel("$ " + Game.gsh.getMoney());
	private JLabel amountLabel = new JLabel("You have: ");
	private ArrayList<JLabel> actionLabelList = new ArrayList<JLabel>();
	
	private ImageIcon swordIcon = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "swordIcon"), 45, 45));
	private JLabel actionIconLabel = new JLabel(swordIcon);
	private JLabel itemIconLabel = new JLabel(swordIcon);
	private JLabel remainLabel = new JLabel(new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "remainSign"), 30, 30)));
	private JLabel remainReverseLabel = new JLabel(new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "remainReverseSign"), 30, 30)));
	
	/*Role Labels*/
	private final int rolePicX = roleStatusX;
	private final int rolePicY = roleStatusY;
	private final int rolePicWidth = roleStatusWidth*2/5;
	private final int rolePicHeight = roleStatusHeight/4;
	
	private final int roleNowX = rolePicX + rolePicWidth;
	private final int roleNowY = rolePicY;
	private final int roleNowWidth = rolePicWidth;
	private final int roleNowHeight = rolePicHeight/5;
	
	private final int roleAddX = roleNowX + roleNowWidth - 50;
	private final int roleAddY = rolePicY;
	private final int roleAddWidth = roleNowWidth/2;
	private final int roleAddHeight = roleNowHeight;
	
	private LinkedList<JLabel> roleLabelList = new LinkedList<JLabel>();
	private LinkedList<JLabel> roleHpLabelList = new LinkedList<JLabel>();
	private LinkedList<JLabel> roleMpLabelList = new LinkedList<JLabel>();
	private LinkedList<JLabel> roleAtkLabelList = new LinkedList<JLabel>();
	private LinkedList<JLabel> roleDefLabelList = new LinkedList<JLabel>();
	private LinkedList<JLabel> roleSpdLabelList = new LinkedList<JLabel>();
	private LinkedList<JLabel> roleHpGainLabelList = new LinkedList<JLabel>();
	private LinkedList<JLabel> roleMpGainLabelList = new LinkedList<JLabel>();
	private LinkedList<JLabel> roleAtkGainLabelList = new LinkedList<JLabel>();
	private LinkedList<JLabel> roleDefGainLabelList = new LinkedList<JLabel>();
	private LinkedList<JLabel> roleSpdGainLabelList = new LinkedList<JLabel>();
	
	private int actionIndex;
	private int itemIndex;
	private int itemUpestIndex;
	private int itemDownestIndex;
	
	private boolean isInActionBox;
	private boolean isInItemBox;
	private boolean isSell;
	
	private Controller controller;
	private ArrayList<String> sellList;
	private int npcIndex;
		
	public StoreMenu(Controller controller, ArrayList<String> sellList, int npcIndex){
		this.controller = controller;
		this.sellList = sellList;
		this.npcIndex = npcIndex;
		this.setOpaque(false);
		this.setBounds(0, 0, Game.gui.getWidth(), Game.gui.getHeight());
		this.setLayout(null);
		Game.gui.addKeyListener(this);
		
		initialLayout();
		setActionBox();
		actionIndex = 0;
		setActionIcon(actionIndex);
		isInActionBox = true;
	}
	
	private void initialLayout(){
		mainPanel.setBounds(Game.gui.getWidth()/4, 50, Game.gui.getWidth()/3, Game.gui.getHeight()*7/10);
		mainPanel.setLayout(null);
		this.add(mainPanel);
		
		infoPanel.setBounds(Game.gui.getWidth()/4, Game.gui.getHeight()*8/10, Game.gui.getWidth()/2, Game.gui.getHeight()/5);
		this.add(infoPanel);
		
		typePanel.setBounds(Game.gui.getWidth() * 4/7 + 30, Game.gui.getHeight()*1/10, (int)(Game.gui.getWidth() * 0.15), (int)(Game.gui.getWidth() * 0.15));
		this.add(typePanel);
		
		amountPanel.setBounds(Game.gui.getWidth() * 4/7 + 30, Game.gui.getHeight()*4/10 - 30, (int)(Game.gui.getWidth() * 0.15), 60);
		this.add(amountPanel);
		
		moneyPanel.setBounds(Game.gui.getWidth() * 4/7 + 30, Game.gui.getHeight()*4/10 + 30, (int)(Game.gui.getWidth() * 0.15), 60);
		this.add(moneyPanel);
		
		actionPanel.setBounds(Game.gui.getWidth() * 4/7 + 30, Game.gui.getHeight()*5/10 + 10, (int)(Game.gui.getWidth() * 0.15), Game.gui.getHeight()/4 + 20);
		this.add(actionPanel);
		
		/*Role Panel*/
		rolePanel.setBounds(roleStatusX, roleStatusY, roleStatusWidth, roleStatusHeight);
		this.add(rolePanel);
		
		infoLabel.setBounds(30, 20, Game.gui.getWidth()/2 - 60, Game.gui.getHeight()/5 - 40);
		infoLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 25));
		infoPanel.add(infoLabel);
		
		typeLabel.setBounds(30, 30, (int)(Game.gui.getWidth() * 0.15) - 60, (int)(Game.gui.getWidth() * 0.15) - 60);
		typePanel.add(typeLabel);
		
		moneyLabel.setBounds(30, 5, (int)(Game.gui.getWidth() * 0.15)-50, 60);
		moneyLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 26));
		moneyPanel.add(moneyLabel);
		
		amountLabel.setBounds(30, 0, (int)(Game.gui.getWidth() * 0.15)-50, 60);
		amountLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 24));
		amountPanel.add(amountLabel);
		
		/*Role Panel's Label*/
		LinkedList<Role> roles = Game.gsh.getRoles();
		for(int i=0;i<roles.size();i++){
			roleLabelList.add(new JLabel(roles.get(i).getPortrait()));
			roleLabelList.get(i).setBounds(rolePicX, rolePicY+rolePicHeight*i, rolePicWidth, rolePicHeight);
			rolePanel.add(roleLabelList.get(i));
			
			roleHpLabelList.add(new JLabel());
			roleHpLabelList.get(i).setBounds(roleNowX, roleNowY+rolePicHeight*i, roleNowWidth, roleNowHeight);
			roleHpLabelList.get(i).setFont(new Font("Trebuchet MS", Font.PLAIN , 20));
			rolePanel.add(roleHpLabelList.get(i));
			
			roleMpLabelList.add(new JLabel());
			roleMpLabelList.get(i).setBounds(roleNowX, roleNowY+roleNowHeight*1+rolePicHeight*i, roleNowWidth, roleNowHeight);
			roleMpLabelList.get(i).setFont(new Font("Trebuchet MS", Font.PLAIN , 20));
			rolePanel.add(roleMpLabelList.get(i));
			
			roleAtkLabelList.add(new JLabel());
			roleAtkLabelList.get(i).setBounds(roleNowX, roleNowY+roleNowHeight*2+rolePicHeight*i, roleNowWidth, roleNowHeight);
			roleAtkLabelList.get(i).setFont(new Font("Trebuchet MS", Font.PLAIN , 20));
			rolePanel.add(roleAtkLabelList.get(i));
			
			roleDefLabelList.add(new JLabel());
			roleDefLabelList.get(i).setBounds(roleNowX, roleNowY+roleNowHeight*3+rolePicHeight*i, roleNowWidth, roleNowHeight);
			roleDefLabelList.get(i).setFont(new Font("Trebuchet MS", Font.PLAIN , 20));
			rolePanel.add(roleDefLabelList.get(i));
			
			roleSpdLabelList.add(new JLabel());
			roleSpdLabelList.get(i).setBounds(roleNowX, roleNowY+roleNowHeight*4+rolePicHeight*i, roleNowWidth, roleNowHeight);
			roleSpdLabelList.get(i).setFont(new Font("Trebuchet MS", Font.PLAIN , 20));
			rolePanel.add(roleSpdLabelList.get(i));
			
			roleHpGainLabelList.add(new JLabel());
			roleHpGainLabelList.get(i).setBounds(roleAddX, roleAddY+rolePicHeight*i, roleAddWidth, roleAddHeight);
			roleHpGainLabelList.get(i).setFont(new Font("Trebuchet MS", Font.BOLD , 20));
			rolePanel.add(roleHpGainLabelList.get(i));
			
			roleMpGainLabelList.add(new JLabel());
			roleMpGainLabelList.get(i).setBounds(roleAddX, roleAddY+roleAddHeight*1+rolePicHeight*i, roleAddWidth, roleAddHeight);
			roleMpGainLabelList.get(i).setFont(new Font("Trebuchet MS", Font.BOLD , 20));
			rolePanel.add(roleMpGainLabelList.get(i));
			
			roleAtkGainLabelList.add(new JLabel());
			roleAtkGainLabelList.get(i).setBounds(roleAddX, roleAddY+roleAddHeight*2+rolePicHeight*i, roleAddWidth, roleAddHeight);
			roleAtkGainLabelList.get(i).setFont(new Font("Trebuchet MS", Font.BOLD , 20));
			rolePanel.add(roleAtkGainLabelList.get(i));
			
			roleDefGainLabelList.add(new JLabel());
			roleDefGainLabelList.get(i).setBounds(roleAddX, roleAddY+roleAddHeight*3+rolePicHeight*i, roleAddWidth, roleAddHeight);
			roleDefGainLabelList.get(i).setFont(new Font("Trebuchet MS", Font.BOLD , 20));
			rolePanel.add(roleDefGainLabelList.get(i));
			
			roleSpdGainLabelList.add(new JLabel());
			roleSpdGainLabelList.get(i).setBounds(roleAddX, roleAddY+roleAddHeight*4+rolePicHeight*i, roleAddWidth, roleAddHeight);
			roleSpdGainLabelList.get(i).setFont(new Font("Trebuchet MS", Font.BOLD , 20));
			rolePanel.add(roleSpdGainLabelList.get(i));
			
		}
	}
	
	private void setActionBox(){
		actionPanel.setLayout(null);
		String[] options = {"Buy", "Sell", "Talk", "Exit"};
		int interval = (actionPanel.getHeight() - 50) / 4;
		for(int i=0; i<options.length; i++){
			JLabel label = new JLabel(options[i]);
			label.setBounds(actionPanel.getWidth()/2, 10 + (10+interval) * i, actionPanel.getWidth()/3, interval);
			label.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
			actionLabelList.add(label);
			actionPanel.add(actionLabelList.get(i), 0);
		}
		actionIconLabel.setSize(45, 45);
		actionPanel.add(actionIconLabel);
		this.repaint();
	}
	
	private void setActionIcon(int index){
		int interval = (actionPanel.getHeight() - 50) / 4;
		actionIconLabel.setLocation(20, 10 + (10+interval) * index);
		actionLabelList.get(index).setForeground(Color.GREEN);
		if(index > 0)
			actionLabelList.get(index-1).setForeground(Color.BLACK);
		if(index < actionLabelList.size() - 1)
			actionLabelList.get(index+1).setForeground(Color.BLACK);
	}
	
	private void setBuyBox(){
		mainPanel.removeAll();
		
		remainLabel.setBounds(Game.gui.getWidth() / 6, Game.gui.getHeight() * 6/10, 45, 45);
		mainPanel.add(remainLabel, 0);
		
		itemLabelList = new ArrayList<JLabel>();
		priceLabelList = new ArrayList<JLabel>();
		for(int i=itemUpestIndex; i<itemDownestIndex; i++){
			if(i >= sellList.size())
				break;
			JLabel label = new JLabel(sellList.get(i));
			label.setBounds(80, 30 + (10 + ITEM_INTERVAL)*(i-itemUpestIndex), mainPanel.getWidth() * 2/3, ITEM_INTERVAL);
			label.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
			itemLabelList.add(label);
			mainPanel.add(itemLabelList.get((i-itemUpestIndex)), 0);
			
			Item item = ResourceHandler.getItemByName(itemLabelList.get((i-itemUpestIndex)).getText());
			JLabel priceLabel = new JLabel(item.getPrice() + "");
			priceLabel.setBounds(mainPanel.getWidth() * 2/3 + 80, 30 + (10 + ITEM_INTERVAL)*(i-itemUpestIndex), 100, ITEM_INTERVAL);
			priceLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
			priceLabelList.add(priceLabel);
			mainPanel.add(priceLabelList.get((i-itemUpestIndex)), 0);
		}
		itemIconLabel.setSize(45, 45);
		mainPanel.add(itemIconLabel);
		
		if(itemDownestIndex < sellList.size())
			remainLabel.setVisible(true);
		else
			remainLabel.setVisible(false);
		if(itemUpestIndex == 0)
			remainReverseLabel.setVisible(false);
		else
			remainReverseLabel.setVisible(true);
	}
	
	private void setSellBox(){
		mainPanel.removeAll();
		
		setRemainLabel();
		
		itemLabelList = new ArrayList<JLabel>();
		priceLabelList = new ArrayList<JLabel>();
		LinkedList<Item> itemList = Game.gsh.getItems();
		for(int i=itemUpestIndex; i<itemDownestIndex; i++){
			if(i >= itemList.size())
				break;
			JLabel label = new JLabel(itemList.get(i).getName());
			label.setBounds(80, 30 + (10 + ITEM_INTERVAL)*(i-itemUpestIndex), mainPanel.getWidth() * 2/3, ITEM_INTERVAL);
			label.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
			itemLabelList.add(label);
			mainPanel.add(itemLabelList.get((i-itemUpestIndex)), 0);
			
			JLabel priceLabel = new JLabel(itemList.get(i).getPrice() * 8/10 + "");
			priceLabel.setBounds(mainPanel.getWidth() * 2/3 + 80, 30 + (10 + ITEM_INTERVAL)*(i-itemUpestIndex), 100, ITEM_INTERVAL);
			priceLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
			priceLabelList.add(priceLabel);
			mainPanel.add(priceLabelList.get((i-itemUpestIndex)), 0);
		}
		itemIconLabel.setSize(45, 45);
		mainPanel.add(itemIconLabel);
		
		if(itemDownestIndex < itemList.size())
			remainLabel.setVisible(true);
		else
			remainLabel.setVisible(false);
		if(itemUpestIndex == 0)
			remainReverseLabel.setVisible(false);
		else
			remainReverseLabel.setVisible(true);
	}
	
	private void setItemIcon(int index){
		if(index < itemUpestIndex)
			moveIndexBound(true);
		else if(index >= itemDownestIndex)
			moveIndexBound(false);
		itemIconLabel.setLocation(20, 30 + (10 + ITEM_INTERVAL) * (index-itemUpestIndex));
		itemLabelList.get((index-itemUpestIndex)).setForeground(Color.GREEN);
		priceLabelList.get((index-itemUpestIndex)).setForeground(Color.GREEN);
		if((index-itemUpestIndex) > 0){
			itemLabelList.get((index-itemUpestIndex)-1).setForeground(Color.BLACK);
			priceLabelList.get((index-itemUpestIndex)-1).setForeground(Color.BLACK);
		}
		if((index-itemUpestIndex) < itemLabelList.size()-1){
			itemLabelList.get((index-itemUpestIndex)+1).setForeground(Color.BLACK);
			priceLabelList.get((index-itemUpestIndex)+1).setForeground(Color.BLACK);
		}
		updateInfo();
	}
	
	private void setInitialIndexBound(){
		itemUpestIndex = 0;
		itemDownestIndex = 8;
	}
	
	private void moveIndexBound(boolean isUp){
		if(isUp){
			itemUpestIndex--;
			itemDownestIndex--;
		}
		else{
			itemUpestIndex++;
			itemDownestIndex++;
		}
		if(isSell)
			setSellBox();
		else
			setBuyBox();
	}
	
	private void setRemainLabel(){
		remainLabel.setBounds(Game.gui.getWidth() / 6, Game.gui.getHeight() * 6/10 + 50, 30, 30);
		remainLabel.setVisible(false);
		mainPanel.add(remainLabel, 0);
		
		remainReverseLabel.setBounds(Game.gui.getWidth() / 6, 0, 30, 30);
		remainReverseLabel.setVisible(false);
		mainPanel.add(remainReverseLabel, 0);
	}
	
	private void updateInfo(){
		Item item = ResourceHandler.getItemByName(itemLabelList.get(itemIndex-itemUpestIndex).getText());
		infoLabel.setText(item.getUtility());
		typeLabel.setIcon(new ImageIcon(Drawer.getResizedImg(item.getTypeImage(), typeLabel.getWidth(), typeLabel.getHeight())));
		moneyLabel.setText("$ " + Game.gsh.getMoney());
		amountLabel.setText("You have: " + Game.gsh.countItem(item));
		
		LinkedList<Role> roles = Game.gsh.getRoles();
		for(int i=0;i<roles.size();i++){
			roleHpLabelList.get(i).setText("HP:" + roles.get(i).getHP());
			roleMpLabelList.get(i).setText("MP:" + roles.get(i).getMP());
			roleAtkLabelList.get(i).setText("ATK:" + roles.get(i).getAtk());
			roleDefLabelList.get(i).setText("DEF:" + roles.get(i).getDef());
			roleSpdLabelList.get(i).setText("SPD:" + roles.get(i).getSpd());
			
			roleHpGainLabelList.get(i).setText(roles.get(i).getHpChange(item));
			if(!roles.get(i).getHpChange(item).equals("")){
				if(roles.get(i).getHpChange(item).toCharArray()[1] == '+' && roles.get(i).getHpChange(item).toCharArray()[3] != '0')
					roleHpGainLabelList.get(i).setForeground(Color.GREEN);
				else if(roles.get(i).getHpChange(item).toCharArray()[1] == '-') 
					roleHpGainLabelList.get(i).setForeground(Color.RED);
				else
					roleHpGainLabelList.get(i).setForeground(Color.YELLOW);
			}
			
			roleMpGainLabelList.get(i).setText(roles.get(i).getMpChange(item));
			if(!roles.get(i).getMpChange(item).equals("")){
				if(roles.get(i).getMpChange(item).toCharArray()[1] == '+' && roles.get(i).getMpChange(item).toCharArray()[3] != '0')
					roleMpGainLabelList.get(i).setForeground(Color.GREEN);
				else if(roles.get(i).getMpChange(item).toCharArray()[1] == '-') 
					roleMpGainLabelList.get(i).setForeground(Color.RED);
				else
					roleMpGainLabelList.get(i).setForeground(Color.YELLOW);
			}
			
			roleAtkGainLabelList.get(i).setText(roles.get(i).getAtkChange(item));
			if(!roles.get(i).getAtkChange(item).equals("")){
				if(roles.get(i).getAtkChange(item).toCharArray()[1] == '+' && roles.get(i).getAtkChange(item).toCharArray()[3] != '0')
					roleAtkGainLabelList.get(i).setForeground(Color.GREEN);
				else if(roles.get(i).getAtkChange(item).toCharArray()[1] == '-') 
					roleAtkGainLabelList.get(i).setForeground(Color.RED);
				else
					roleAtkGainLabelList.get(i).setForeground(Color.YELLOW);
			}
			
			roleDefGainLabelList.get(i).setText(roles.get(i).getDefChange(item));
			if(!roles.get(i).getDefChange(item).equals("")){
				if(roles.get(i).getDefChange(item).toCharArray()[1] == '+' && roles.get(i).getDefChange(item).toCharArray()[3] != '0')
					roleDefGainLabelList.get(i).setForeground(Color.GREEN);
				else if(roles.get(i).getDefChange(item).toCharArray()[1] == '-') 
					roleDefGainLabelList.get(i).setForeground(Color.RED);
				else
					roleDefGainLabelList.get(i).setForeground(Color.YELLOW);
			}
			
			roleSpdGainLabelList.get(i).setText(roles.get(i).getSpdChange(item));
			if(!roles.get(i).getSpdChange(item).equals("")){
				if(roles.get(i).getSpdChange(item).toCharArray()[1] == '+' && roles.get(i).getSpdChange(item).toCharArray()[3] != '0')
					roleSpdGainLabelList.get(i).setForeground(Color.GREEN);
				else if(roles.get(i).getSpdChange(item).toCharArray()[1] == '-') 
					roleSpdGainLabelList.get(i).setForeground(Color.RED);
				else
					roleSpdGainLabelList.get(i).setForeground(Color.YELLOW);
			}
		}
		
		this.repaint();
		mainPanel.repaint();
		infoPanel.repaint();
		typePanel.repaint();
		moneyLabel.repaint();
	}
	
	public void keyPressed(KeyEvent e){
		int keyCode = e.getKeyCode();
		
		if(isInActionBox){
			if(keyCode == Game.ESC)
				leaveMenu();
			else if(keyCode == Game.UP){
				if(actionIndex > 0)
					setActionIcon(--actionIndex);
			}
			else if(keyCode == Game.DOWN){
				if(actionIndex < actionLabelList.size()-1)
					setActionIcon(++actionIndex);
			}
			else if(keyCode == Game.ENTER || keyCode == Game.SPACE){
				if(actionLabelList.get(actionIndex).getText().equals("Exit"))
					leaveMenu();
				else if(actionLabelList.get(actionIndex).getText().equals("Buy")){
					isInActionBox = false;
					isInItemBox = true;
					setInitialIndexBound();
					setBuyBox();
					itemIndex = 0;
					setItemIcon(itemIndex);
					isSell = false;
				}
				else if(actionLabelList.get(actionIndex).getText().equals("Sell")){
					if(Game.gsh.getItems().size() == 0)
						return;
					isInActionBox = false;
					isInItemBox = true;
					setInitialIndexBound();
					setSellBox();
					itemIndex = 0;
					setItemIcon(itemIndex);
					isSell = true;
				}
				else if(actionLabelList.get(actionIndex).getText().equals("Talk")){
					leaveMenu();
					controller.callDialog(npcIndex, true);
				}
			}
		}
		else if(isInItemBox){
			if(keyCode == Game.ESC)
				leaveItemBox();
			else if(keyCode == Game.UP){
				if(itemIndex > 0)
					setItemIcon(--itemIndex);
			}
			else if(keyCode == Game.DOWN){
				if(!isSell){
					if(itemIndex < sellList.size()-1)
						setItemIcon(++itemIndex);
				}
				else{
					if(itemIndex < Game.gsh.getItems().size()-1)
						setItemIcon(++itemIndex);
				}
			}
			else if(keyCode == Game.ENTER || keyCode == Game.SPACE){
				if(!isSell){
					Item item = ResourceHandler.getItemByName(itemLabelList.get(itemIndex).getText());
					if(item.getPrice() > Game.gsh.getMoney()){
						infoLabel.setText("Seems like you don't have enough money");
						return;
					}
					Game.gsh.addItem(item);
					Game.gsh.setMoney(Game.gsh.getMoney() - item.getPrice());
					updateInfo();
				}
				else{
					Item item = ResourceHandler.getItemByName(itemLabelList.get(itemIndex-itemUpestIndex).getText());
					Game.gsh.removeItem(item);
					Game.gsh.setMoney(Game.gsh.getMoney() + item.getPrice() * 8 /10);
					setSellBox();
					if(Game.gsh.getItems().size() == 0){
						leaveItemBox();
						return;
					}
					itemIndex = itemIndex < (Game.gsh.getItems().size()-1) ? itemIndex : itemIndex-1;
					if(itemIndex < 0)
						itemIndex = 0;
					
					setItemIcon(itemIndex);
				}
			}
		}
	}
	
	public void keyTyped(KeyEvent e){
	}
		
	public void keyReleased(KeyEvent e){
	}
	
	private void leaveItemBox(){
		isInItemBox = false;
		isInActionBox = true;
		mainPanel.removeAll();
		typeLabel.setIcon(null);
		infoLabel.setText("");
		this.repaint();
	}
	
	private void leaveMenu(){
		((JPanel)Game.gui.getGlassPane()).removeAll();
		Game.gui.getGlassPane().setVisible(false);
		Game.gui.removeKeyListener(this);
		Game.gui.addKeyListener(controller);
		this.repaint();
	}
}