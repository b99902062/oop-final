package rpg.control;

import java.lang.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import java.awt.*;
import java.net.*;

import javax.swing.*;
import javax.imageio.*;

import rpg.draw.*;
import rpg.game.*;
import rpg.character.*;
import rpg.resource.*;
import rpg.map.*;
import rpg.audio.*;
import rpg.status.*;
import rpg.mode.*;
import rpg.item.*;

/**
 * A class that implements the backpack menu
 * @author  Chander, Moe, William
 * @version 1.0
 */
 
public class BackpackMenu extends JPanel implements Serializable, KeyListener{

	private static final long serialVersionUID = 1L;
	private static final int ITEM_SIZE = 100;
	private static final int LINE_SIZE = 9;
	
	private AudioPlayer switchPlayer = new AudioPlayer(false, ResourceHandler.getResourcePath("sound", "switchSound"));	
	private Image mainBG = Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), Game.gui.getWidth() * 3/4, Game.gui.getHeight()*3/4);
	private Image typeBG = Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), Game.gui.getWidth()/5 - 20, Game.gui.getHeight()/4);
	private Image infoBG = Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), Game.gui.getWidth()*4/5, Game.gui.getHeight()/4);
	private Image moneyBG = Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), Game.gui.getWidth()/4, Game.gui.getHeight()/10);
	private ImageIcon focusImage = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "focusbImage"), ITEM_SIZE, ITEM_SIZE));
	private ImageIcon swordIcon = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "swordIcon"), 60, 60));
	
	private JLabel focusLabel = new JLabel(focusImage);
	private JLabel typeImage = new JLabel();
	private JLabel info = new JLabel();
	private JLabel amount = new JLabel();
	private JLabel money = new JLabel();
	private JLabel iconLabel = new JLabel(swordIcon);
	
	private JPanel mainPanel = new JPanel();
	private JPanel statusPanel = Drawer.drawBackGround(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), Game.gui.getWidth()/4 -20, Game.gui.getHeight()/2 + 20));
	
	private int curItemIndex;
	private int curChoiceIndex;
	
	private JPanel choiceMenu = new JPanel();
	
	private boolean isInChoice;
	
	private LinkedList<Role> roleList = Game.gsh.getRoles();
	private ArrayList<String> choiceNameList;
	private ArrayList<JLabel> choiceList;
	private ArrayList<JPanel> roleStatusList;
	private ArrayList<JLabel> roleHPList;
	private ArrayList<JLabel> roleMPList;
	private ArrayList<JLabel> roleHPTextList;
	private ArrayList<JLabel> roleMPTextList;
	
	private GameMenu gm;
		
	public BackpackMenu(GameMenu gm){
		this.gm = gm;
		this.setOpaque(false);
		this.setBounds(0, 0, Game.gui.getWidth(), Game.gui.getHeight());
		this.setLayout(null);
		initialLayout();
		showItems();
		resetFocus();
		mainPanel.add(focusLabel, 0);
		this.repaint();
		Game.gui.getGlassPane().addKeyListener(this);
		requestFocus();
	}
	
	public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(mainBG, 0, Game.gui.getHeight()/4, null); 
		g.drawImage(typeBG, 20, 10, null); 
		g.drawImage(infoBG, Game.gui.getWidth()/5, 10, null); 
		g.drawImage(moneyBG, Game.gui.getWidth() * 3/4, Game.gui.getHeight()/4 + 30, null); 
    }
	
	public void initialLayout(){
		typeImage.setBounds(50, 30, Game.gui.getWidth()/5 - 100, getHeight()/4 - 40);
		this.add(typeImage, 0);
		
		info.setBounds(Game.gui.getWidth()/5 + 50, 10, Game.gui.getWidth()*4/5, getHeight()/4);
		info.setFont(new Font("Trebuchet MS", Font.BOLD, 30));
		this.add(info, 0);
		
		amount.setBounds(Game.gui.getWidth()*4/5 + 50, 50, Game.gui.getWidth()*1/5 -50, getHeight()/4);
		amount.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
		this.add(amount, 0);
		
		money.setBounds(Game.gui.getWidth() * 3/4 + 50, Game.gui.getHeight()/4 + 30, Game.gui.getWidth() /4 - 60, getHeight()/10);
		money.setFont(new Font("Trebuchet MS", Font.BOLD, 25));
		money.setText("$ " + Game.gsh.getMoney());
		this.add(money, 0);
		
		mainPanel.setLayout(null);
		mainPanel.setBounds(0, Game.gui.getHeight()/4 + 10, Game.gui.getWidth()* 3/4, Game.gui.getHeight()*3/4 - 10);
		mainPanel.setOpaque(false);
		this.add(mainPanel, 0);
		
		setStatusPanel();
	}
	
	private void setStatusPanel(){
		statusPanel.setLayout(new GridLayout(4, 1));
		statusPanel.setBounds(Game.gui.getWidth()*3/4, Game.gui.getHeight()/2 - 50, Game.gui.getWidth() / 4, Game.gui.getHeight() / 2 + 30);
		statusPanel.setOpaque(false);
		
		setRoleStatusList();
		
		for(int i=0; i<roleStatusList.size(); i++)
			statusPanel.add(roleStatusList.get(i));
		
		this.add(statusPanel, 0);
	}
	
	private void setRoleStatusList(){
		roleStatusList = new ArrayList<JPanel>();
		
		roleHPList = new ArrayList<JLabel>();
		roleMPList = new ArrayList<JLabel>();
		roleHPTextList = new ArrayList<JLabel>();
		roleMPTextList = new ArrayList<JLabel>();
		
		for(int i=0; i<roleList.size(); i++){
			JPanel panel = new JPanel();
			panel.setLayout(null);
			panel.setOpaque(false);
			panel.setSize(Game.gui.getWidth() / 4 - 20, 100);
			
			JLabel portrait = new JLabel(new ImageIcon(Drawer.getResizedImg(roleList.get(i).getPortrait().getImage(), 80, 80)));
			portrait.setBounds(20, 20, panel.getWidth() / 3, panel.getHeight());
			panel.add(portrait, 0);
			
			JLabel hpLabel = new JLabel(new ImageIcon(ResourceHandler.getImageByName("other", "hpImage")));
			hpLabel.setBounds(20 + panel.getWidth() / 3, 50, 100 * roleList.get(i).getCurHP() / roleList.get(i).getHP(), 10);
			panel.add(hpLabel, 0);
			
			JLabel hpTextLabel = new JLabel(roleList.get(i).getCurHP() + "/" + roleList.get(i).getHP());
			hpTextLabel.setBounds(120 + panel.getWidth() / 3, 50, 80, 30);
			panel.add(hpTextLabel, 0);
			
			JLabel mpLabel = new JLabel(new ImageIcon(ResourceHandler.getImageByName("other", "mpImage")));
			mpLabel.setBounds(20 + panel.getWidth() / 3, 80, 100 * roleList.get(i).getCurMP() / roleList.get(i).getMP(), 10);
			panel.add(mpLabel, 0);
			
			JLabel mpTextLabel = new JLabel(roleList.get(i).getCurMP() + "/" + roleList.get(i).getMP());
			mpTextLabel.setBounds(120 + panel.getWidth() / 3, 80, 80, 30);
			panel.add(mpTextLabel, 0);
			
			roleHPList.add(hpLabel);
			roleMPList.add(mpLabel);
			roleHPTextList.add(hpTextLabel);
			roleMPTextList.add(mpTextLabel);
			
			roleStatusList.add(panel);
		}
	}
	
	private void resetFocus(){
		focusLabel.setBounds(50 , 50, ITEM_SIZE, ITEM_SIZE);
		curItemIndex = 0;
		updateInfo();
	}
	
	public void showItems(){
		LinkedList<Item> itemList = Game.gsh.getItems();
		for(int i=0; i<itemList.size(); i++){
			JLabel label = new JLabel(new ImageIcon(Drawer.getResizedImg(itemList.get(i).getImage().getImage(), 50, 50)));
			label.setBounds(50 + ITEM_SIZE * (i % LINE_SIZE), 50 + ITEM_SIZE * (i / LINE_SIZE), ITEM_SIZE, ITEM_SIZE);
			mainPanel.add(label);
		}
	}
	
	public void updateInfo(){
		if(Game.gsh.getItems().size() == 0)
			return;
		info.setText(Game.gsh.getItems().get(curItemIndex).getUtility());
		amount.setText("Amount: " + Game.gsh.getItems().get(curItemIndex).getAmount());
		typeImage.setIcon(new ImageIcon(Drawer.getResizedImg(Game.gsh.getItems().get(curItemIndex).getTypeImage(), typeImage.getWidth(), typeImage.getHeight())));
		money.setText("$ " + Game.gsh.getMoney());
		
		for(int i=0; i<roleList.size(); i++){
			JPanel panel = roleStatusList.get(i);
			roleHPList.get(i).setSize(100 * roleList.get(i).getCurHP() / roleList.get(i).getHP(), 10);
			roleMPList.get(i).setSize(100 * roleList.get(i).getCurMP() / roleList.get(i).getMP(), 10);
			roleHPTextList.get(i).setText(roleList.get(i).getCurHP() + "/" + roleList.get(i).getHP());
			roleMPTextList.get(i).setText(roleList.get(i).getCurMP() + "/" + roleList.get(i).getMP());
		}
		
		this.repaint();
	}
	
	public void keyTyped(KeyEvent e){
	}
	
	public synchronized void keyPressed(KeyEvent e){
		int keyCode = e.getKeyCode();
		
		if(!isInChoice){
			if(keyCode == Game.ESC){
				((JPanel)Game.gui.getGlassPane()).remove(this);
				Game.gui.getGlassPane().removeKeyListener(this);
				Game.gui.getGlassPane().addKeyListener(gm);
				gm.setVisible(true);
				Game.gui.repaint();
			}
			else if(keyCode == Game.UP){
				if(curItemIndex - LINE_SIZE >= 0){
					focusLabel.setLocation(focusLabel.getX(), focusLabel.getY() - ITEM_SIZE);
					curItemIndex -= LINE_SIZE;
					updateInfo();
					Thread soundThread = new Thread(switchPlayer);
					soundThread.start();
				}
			}
			else if(keyCode == Game.DOWN){
				if(curItemIndex + LINE_SIZE < Game.gsh.getItems().size()){
					focusLabel.setLocation(focusLabel.getX(), focusLabel.getY() + ITEM_SIZE);
					curItemIndex += LINE_SIZE;
					updateInfo();
					Thread soundThread = new Thread(switchPlayer);
					soundThread.start();
				}
			}
			else if(keyCode == Game.LEFT){
				if(curItemIndex - 1 >= 0 && curItemIndex % LINE_SIZE != 0){
					focusLabel.setLocation(focusLabel.getX() - ITEM_SIZE, focusLabel.getY());
					curItemIndex -= 1;
					updateInfo();
					Thread soundThread = new Thread(switchPlayer);
					soundThread.start();
				}
			}
			else if(keyCode == Game.RIGHT){
				if(curItemIndex + 1 < Game.gsh.getItems().size() && (curItemIndex + 1) % LINE_SIZE != 0){
					focusLabel.setLocation(focusLabel.getX() + ITEM_SIZE, focusLabel.getY());
					curItemIndex += 1;
					updateInfo();
					Thread soundThread = new Thread(switchPlayer);
					soundThread.start();
				}
			}
			else if(keyCode == Game.R){
				mainPanel.removeAll();
				Game.gsh.reArrangeLists();
				resetFocus();
				showItems();
				mainPanel.add(focusLabel, 0);
				this.repaint();
			}
			else if(keyCode == Game.ENTER || keyCode == Game.SPACE){
				if(Game.gsh.getItems().size() > 0 && Game.gsh.getItems().get(curItemIndex).getType() == Item.TYPE_POTION){
					setConfirmBox();
					showChoiceMenu();
					isInChoice = true;
				}
			}
		}
		else{
			if(keyCode == Game.UP){
				if(curChoiceIndex > 0)
					setChoiceFocus(--curChoiceIndex);
			}
			else if(keyCode == Game.DOWN){
				if(curChoiceIndex < choiceList.size() - 1)
					setChoiceFocus(++curChoiceIndex);
			}
			else if(keyCode == Game.ENTER || keyCode == Game.SPACE){
				if(Game.gsh.getItems().size() > 0){
					if(choiceNameList.get(curChoiceIndex).equals("Use")){
						if(((Potion)Game.gsh.getItems().get(curItemIndex)).isSingle()){
							hideChoiceMenu();
							setRoleBox();
							showChoiceMenu();
						}
						else{
							doEffect((Potion)Game.gsh.getItems().get(curItemIndex), null);
							hideChoiceMenu();
							isInChoice = false;
						}
					}
					else if(choiceNameList.get(curChoiceIndex).equals("Cancel")){
						hideChoiceMenu();
						isInChoice = false;
					}
					else{
						Role target = Game.gsh.getRole(choiceNameList.get(curChoiceIndex));
						if(target != null)
							doEffect((Potion)Game.gsh.getItems().get(curItemIndex), target);
						hideChoiceMenu();
						isInChoice = false;
					}
				}
			}
			else if(keyCode == Game.ESC){
				hideChoiceMenu();
				isInChoice = false;
			}
		}
	}
		
	public void keyReleased(KeyEvent e){
	}
	
	private void setConfirmBox(){
		choiceNameList = new ArrayList<String>();
		choiceNameList.add("Use");
		choiceNameList.add("Cancel");
		setChoiceMenu(choiceNameList);
	}
	
	private void setRoleBox(){
		choiceNameList = new ArrayList<String>();
		for(int i=0; i<roleList.size(); i++)
			choiceNameList.add(roleList.get(i).getName());
		setChoiceMenu(choiceNameList);
	}
	
	private void setChoiceMenu(ArrayList<String> nameList){
		curChoiceIndex = 0;
		int choiceNum = nameList.size();
		
		choiceMenu.setLayout(null);
		choiceMenu.setBounds(Game.gui.getWidth()/2 - 100, Game.gui.getHeight()/2 - 50, 300, 100 * choiceNum);

		JLabel choiceBG = new JLabel(new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), 300, 100 * choiceNum)));
		choiceBG.setBounds(0, 0, 300, 100 * choiceNum);
		choiceMenu.add(choiceBG);
		
		iconLabel.setSize(60, 60);
		choiceMenu.add(iconLabel, 0);
		
		choiceList = new ArrayList<JLabel>();
		for(int i=0; i<choiceNum; i++){
			JLabel label = new JLabel(nameList.get(i));
			label.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
			label.setBounds(90, 10 + 100*i, 200, 90);
			choiceList.add(label);
			choiceMenu.add(choiceList.get(i), 0);
		}
		this.repaint();
	}
	
	private void showChoiceMenu(){
		setChoiceFocus(0);
		this.add(choiceMenu);
		this.repaint();
	}
	
	private void hideChoiceMenu(){
		choiceMenu.removeAll();
		this.remove(choiceMenu);
		this.repaint();
	}
	
	private void setChoiceFocus(int index){
		if(index > 0)
			choiceList.get(index-1).setForeground(Color.BLACK);
		if(index < choiceList.size() - 1)
			choiceList.get(index+1).setForeground(Color.BLACK);
		iconLabel.setLocation(10, 10 + 100 * index);
		choiceList.get(index).setForeground(Color.GREEN);
	}
	
	private boolean doEffect(Potion potion, Role target){
		potion.doEffect(target);
		int tmpIndex = curItemIndex;
		if(Game.gsh.getItems().get(curItemIndex).getAmount() == 1)
			tmpIndex = tmpIndex < (Game.gsh.getItems().size()-1) ? tmpIndex : tmpIndex-1;
		if(tmpIndex < 0)
			tmpIndex = 0;
		Game.gsh.removeItem(potion);
		mainPanel.removeAll();
		mainPanel.add(focusLabel, 0);
		setFocusTo(tmpIndex);
		showItems();
		return true;
	}
	
	private void setFocusTo(int index){
		curItemIndex = index;
		focusLabel.setLocation(50 + ITEM_SIZE * (index % LINE_SIZE), 50 + ITEM_SIZE * (index / LINE_SIZE));
		updateInfo();
		this.repaint();
	}
	
}