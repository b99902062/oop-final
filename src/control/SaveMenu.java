package rpg.control;

import java.lang.*;
import java.util.*;
import java.text.*;
import java.io.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.event.*;
import java.util.*;
import java.awt.*;

import rpg.game.*;
import rpg.mode.*;
import rpg.status.*;
import rpg.character.*;
import rpg.draw.*;
import rpg.resource.*;

/**
 * A class that handles the saving menu and the loading menu
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class SaveMenu extends JPanel implements KeyListener{
	
	private static final long serialVersionUID = 1L;
	private static final int SAVE_SLOT_INTERVAL = 200;
	private static final int CAPTURE_SIZE = 150;
	private static final int MAX_SAVE_SLOT = 3;
	
	private JPanel optionPanel = Drawer.drawBackGround(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), Game.gui.getWidth()/5, Game.gui.getHeight()*3/10));
	private JPanel savePanel = Drawer.drawBackGround(Drawer.getResizedImg(ResourceHandler.getImageByName("background", "menuBG"), Game.gui.getWidth()*3/4, Game.gui.getHeight()*9/10));
	
	private ArrayList<JLabel> optionLabelList = new ArrayList<JLabel>();
	private ArrayList<JPanel> saveList = new ArrayList<JPanel>();
	
	private ImageIcon swordIcon = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "swordIcon"), 45, 45));
	private JLabel optionIconLabel = new JLabel(swordIcon);
	
	private ImageIcon focusIcon = new ImageIcon(Drawer.getResizedImg(ResourceHandler.getImageByName("other", "wideFocus"), Game.gui.getWidth()*3/4 -60, SAVE_SLOT_INTERVAL));
	private JLabel focusLabel = new JLabel(focusIcon);
	
	private int optionIndex;
	private int saveIndex;
	
	private boolean isInOptions;
	private boolean isSave;
	
	private boolean canSave;
	private Object caller;
	
	public SaveMenu(boolean canSave, Object caller, boolean fromSave){
		this.canSave = canSave;
		this.caller = caller;
		this.setOpaque(false);
		this.setBounds(0, 0, Game.gui.getWidth(), Game.gui.getHeight());
		this.setLayout(null);
		JPanel glassPane = (JPanel)Game.gui.getGlassPane();
		
		glassPane.setVisible(true);
		glassPane.setLayout(null);
		glassPane.addKeyListener(this);
		glassPane.requestFocus();
		
		initialLayout();
		setOptions();
		if(fromSave)
			optionIndex = 1;
		else
			optionIndex = 0;
		setOptionIcon(optionIndex);
		isInOptions = true;
		glassPane.repaint();
	}
	
	private void initialLayout(){
		optionPanel.setBounds(30, 30, Game.gui.getWidth()/5, Game.gui.getHeight()*3/10);
		optionPanel.setLayout(null);
		this.add(optionPanel);
		
		savePanel.setBounds(Game.gui.getWidth()/5 +50, 20, Game.gui.getWidth()*3/4, Game.gui.getHeight()*9/10);
		savePanel.setLayout(null);
		this.add(savePanel);
		
		focusLabel.setSize(Game.gui.getWidth()*3/4 - 60, SAVE_SLOT_INTERVAL);
		focusLabel.setVisible(false);
	}
	
	private void setOptions(){
		optionPanel.setLayout(null);
		String[] options = {"Load", "Save", "Exit"};
		int interval = (optionPanel.getHeight() - 50) / options.length;
		for(int i=0; i<options.length; i++){
			JLabel label = new JLabel(options[i]);
			if(!canSave && options[i].equals("Save"))
				label.setForeground(Color.LIGHT_GRAY);
			label.setBounds(optionPanel.getWidth()/2, 10 + (10+interval) * i, optionPanel.getWidth()/3, interval);
			label.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
			optionLabelList.add(label);
			optionPanel.add(optionLabelList.get(i), 0);
		}
		optionIconLabel.setSize(45, 45);
		optionPanel.add(optionIconLabel);
		this.repaint();
	}
	
	private void setOptionIcon(int index){
		int interval = (optionPanel.getHeight() - 50) / optionLabelList.size();
		optionIconLabel.setLocation(20, 10 + (10+interval) * index);
		
		if(canSave || !optionLabelList.get(index).getText().equals("Save"))
			optionLabelList.get(index).setForeground(Color.GREEN);
		if(index > 0){
			if(!canSave && optionLabelList.get(index-1).getText().equals("Save"))
				optionLabelList.get(index-1).setForeground(Color.LIGHT_GRAY);
			else
				optionLabelList.get(index-1).setForeground(Color.BLACK);
		}
		if(index < optionLabelList.size() - 1){
			if(!canSave && optionLabelList.get(index+1).getText().equals("Save"))
				optionLabelList.get(index+1).setForeground(Color.LIGHT_GRAY);
			else
				optionLabelList.get(index+1).setForeground(Color.BLACK);
		}
	}
	
	private void setSaveBox(){
		savePanel.removeAll();
		savePanel.add(focusLabel, 0);
		
		for(int i=0; i<MAX_SAVE_SLOT; i++){
			JPanel panel = new JPanel();
			panel.setBounds(30, 40 + (10 + SAVE_SLOT_INTERVAL) * i, Game.gui.getWidth()*3/4 -60, SAVE_SLOT_INTERVAL);
			panel.setLayout(null);
			panel.setOpaque(false);
			
			JLabel saveIdLabel = new JLabel("SLOT  " + i);
			saveIdLabel.setBounds(30, 20, 80, 30);
			saveIdLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
			panel.add(saveIdLabel, 0);
			
			try{
				String saveFileName = new String("save/slot_" + i);
				File file = new File(saveFileName);
				if(file.exists()){
					ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
					GameStatusHandler gsh = (GameStatusHandler)ois.readObject();
					ois.close();
					
					JLabel nameLabel = new JLabel(gsh.getName());
					nameLabel.setBounds(savePanel.getWidth() / 4, 10, 200, 50);
					nameLabel.setForeground(Color.DARK_GRAY);
					nameLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 24));
					panel.add(nameLabel, 0);
					
					JLabel episodeLabel = new JLabel("Episode " + gsh.getEpisodeId() + "-" + gsh.getSubEpisodeId());
					episodeLabel.setBounds(savePanel.getWidth() / 2, 10, 200, 50);
					episodeLabel.setForeground(Color.BLUE);
					episodeLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
					panel.add(episodeLabel, 0);
					
					JLabel timeLabel = new JLabel(gsh.getTime());
					timeLabel.setBounds(savePanel.getWidth() / 2 - 80, 100, 250, 50);
					timeLabel.setForeground(Color.BLACK);
					timeLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
					panel.add(timeLabel, 0);
					
					LinkedList<Role> roleList = gsh.getRoles();
					for(int j=0; j<roleList.size(); j++){
						JLabel levelLabel = new JLabel("Lv." + roleList.get(j).getLevel());
						levelLabel.setBounds(220 + 100 * j, 70, 50, 30);
						panel.add(levelLabel);
						
						JLabel portraitLabel = new JLabel(new ImageIcon(Drawer.getResizedImg(roleList.get(j).getPortrait().getImage(),80,80)));
						portraitLabel.setBounds(190 + 100 * j, 100, 80, 80);
						panel.add(portraitLabel);
					}
					
					JLabel captureLabel = new JLabel(new ImageIcon(Drawer.getResizedImg(gsh.getCaptureImage().getImage(), CAPTURE_SIZE, CAPTURE_SIZE)));
					captureLabel.setBounds(panel.getWidth()*3/4, 30, CAPTURE_SIZE, CAPTURE_SIZE);
					panel.add(captureLabel, 0);
					
				}
				else{
					
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
			saveList.add(panel);
			savePanel.add(panel, 0);
		}
		focusLabel.setVisible(true);
		this.repaint();
	}
	
	private void setSaveIcon(int index){
		focusLabel.setLocation(30, 40 + (10 + SAVE_SLOT_INTERVAL) * index);
	}
	
	private void saveStatus(int index){
		String saveFileName = new String("save/slot_" + index);
		File file = new File(saveFileName);
		Game.gsh.setTime(getDateTime());
		try{
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file, false));
			oos.writeObject(Game.gsh);
			oos.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private void loadStatus(int index){
		GameStatusHandler gsh = null;
		try{
			String saveFileName = new String("save/slot_" + index);
			File file = new File(saveFileName);
			if(file.exists()){
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
				gsh = (GameStatusHandler)ois.readObject();
				ois.close();
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		if(gsh == null)
			return;
		
		Game.gsh = gsh;
		Game.gsh.reloadEpisodes();
		gsh.setReload(true);
		leaveMenu();
		if(canSave)
			((GameMenu)caller).doReload();
		else
			((LoginHandler)caller).doReload();
	}
	
	private String getDateTime(){
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
		Date date = new Date();
		String strDate = sdFormat.format(date);
		System.out.println(strDate);
		return strDate;
	}
	
	public void keyPressed(KeyEvent e){
		int keyCode = e.getKeyCode();
		
		if(isInOptions){
			if(keyCode == Game.ESC)
				leaveMenu();
			else if(keyCode == Game.UP){
				if(optionIndex > 0)
					setOptionIcon(--optionIndex);
			}
			else if(keyCode == Game.DOWN){
				if(optionIndex < optionLabelList.size()-1)
					setOptionIcon(++optionIndex);
			}
			else if(keyCode == Game.SPACE || keyCode == Game.ENTER){
				if(optionLabelList.get(optionIndex).getText().equals("Exit"))
					leaveMenu();
				else if(optionLabelList.get(optionIndex).getText().equals("Save")){
					if(!canSave)
						return;
					isInOptions = false;
					isSave = true;
					setSaveBox();
					saveIndex = 0;
					setSaveIcon(saveIndex);
				}
				else if(optionLabelList.get(optionIndex).getText().equals("Load")){
					isInOptions = false;
					isSave = false;
					setSaveBox();
					saveIndex = 0;
					setSaveIcon(saveIndex);
				}
			}
		}
		else{
			if(keyCode == Game.ESC){
				isInOptions = true;
				savePanel.removeAll();
				this.repaint();
			}
			else if(keyCode == Game.UP){
				if(saveIndex > 0)
					setSaveIcon(--saveIndex);
			}
			else if(keyCode == Game.DOWN){
				if(saveIndex < MAX_SAVE_SLOT-1)
					setSaveIcon(++saveIndex);
			}
			else if(keyCode == Game.SPACE || keyCode == Game.ENTER){
				if(isSave){
					saveStatus(saveIndex);
					setSaveBox();
				}
				else
					loadStatus(saveIndex);
			}
		}
		
	}
	
	public void keyTyped(KeyEvent e){
	}
	
	public void keyReleased(KeyEvent e){
	}
	
	private void leaveMenu(){
		JPanel glassPane = (JPanel)Game.gui.getGlassPane();
		glassPane.removeKeyListener(this);
		glassPane.remove(this);
		if(canSave){
			Game.gui.getGlassPane().addKeyListener((GameMenu)caller);
			((GameMenu)caller).setVisible(true);
		}
		else{
			Game.gui.getGlassPane().setVisible(false);
			Game.gui.addKeyListener((LoginHandler)caller);
			Game.gui.requestFocus();
		}
		Game.gui.repaint();
	}
	
	
}