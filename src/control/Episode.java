package rpg.control;

import java.lang.*;
import java.util.ArrayList;
import java.io.*;

import rpg.character.*;
import rpg.item.*;
import rpg.resource.*;
/**
 * A class that implements an episode
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Episode implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int id;
	private int subId;
	private int type;
	private String name;
	private String targetName;
	private String boundWords;
	private ArrayList<Item> needItemList;
	private ArrayList<Item> getItemList;
	private ArrayList<Item> loseItemList;
	private ArrayList<Role> getRoleList;
	private ArrayList<Role> loseRoleList;
	private ArrayList<Integer> boundMapList;
	
	public Episode(int id, int subId, int type, String name, String targetName, ArrayList<Item> needItemList, ArrayList<Item> getItemList, ArrayList<Item> loseItemList, ArrayList<Role> getRoleList, ArrayList<Role> loseRoleList, ArrayList<Integer> boundMapList, String boundWords){
		this.id = id;
		this.subId = subId;
		this.type = type;
		this.name = new String(name);
		this.targetName = new String(targetName);
		this.needItemList = needItemList;
		this.getItemList = getItemList;
		this.loseItemList = loseItemList;
		this.getRoleList = getRoleList;
		this.loseRoleList = loseRoleList;
		this.boundMapList = boundMapList;
		this.boundWords = boundWords;
	}
	
	public int getId(){
		return id;
	}
	
	public int getSubId(){
		return subId;
	}
	
	public int getType(){
		return type;
	}
	
	public String getName(){
		return new String(name);
	}
	
	public String getTargetName(){
		return new String(targetName);
	}
	
	public ArrayList<Item> getNeedItemList(){
		return needItemList;
	}
	
	public ArrayList<Item> getGetItemList(){
		return getItemList;
	}
	
	public ArrayList<Item> getLoseItemList(){
		return loseItemList;
	}
	
	public ArrayList<Role> getGetRoleList(){
		return getRoleList;
	}
	
	public ArrayList<Role> getLoseRoleList(){
		return loseRoleList;
	}
	
	public ArrayList<Integer> getBoundMapList(){
		return boundMapList;
	}
	
	public String getBoundWords(){
		return boundWords;
	}
	
	public String toString(){
		return new String(id + "-" + subId + " " + name + "<" + targetName + ">");
	}
}