package rpg.character;

import java.lang.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;

import rpg.item.*;
import rpg.draw.*;
/**
 * A class that handles monsters information
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Monster implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private ImageIcon image;
	private int hp;
	private int strategy;
	private Skill[]	skillList;
	private double[] skillRate;
	private int[] skillOrder;
	private String normalAnimationName;
	private String attackAnimationName;
	private int test;
	private int exp;
	private int money;
	private Item item;
	
	public Monster(String _name, ImageIcon _img, int _hp, int _strategy, Skill[] _skillList, 
					double[] _skillRate, int[] _skillOrder,int _money,int _exp,Item _item, String _normalAnimationName, 
					String _attackAnimationName){
		name = _name;
		image = _img;
		hp = _hp;
		strategy = _strategy;
		skillList = _skillList;
		skillRate = _skillRate;
		skillOrder = _skillOrder;
		money = _money;
		exp = _exp;
		item = _item;
		normalAnimationName = _normalAnimationName;
		attackAnimationName = _attackAnimationName;
	}
	
	public Monster(Monster monster){
		name = monster.getName();
		image = monster.getImage();
		hp = monster.getHp();
		strategy = monster.getStrategy();
		skillList = monster.getSkillList();
		skillRate = monster.getSkillRate();
		skillOrder = monster.getSkillOrder();
		exp = monster.getExp();
		money =  monster.getMoney();
		item = monster.getItem();
		normalAnimationName = monster.getNormalAnimationName();
		attackAnimationName = monster.getAttackAnimationName();
	}
	
	public String getName(){
		return new String(name);
	}
	
	public ImageIcon getImage(){
		return image;
	}
	
	public int getHp(){
		return hp;
	}
	public void setHp(int _hp){
		hp = _hp;
	}
	
	public int getStrategy(){
		return strategy;
	}
	
	public Skill[] getSkillList(){
		return skillList;
	}
	
	public double[] getSkillRate(){
		return skillRate;
	}
	
	public int[] getSkillOrder(){
		return skillOrder;
	}
	
	public int getMoney(){
		return money;
	}
	
	public int getExp(){
		return exp;
	}
	
	public Item getItem(){
		return item;
	}
	
	public String getNormalAnimationName(){
		return normalAnimationName;
	}
	
	public String getAttackAnimationName(){
		return attackAnimationName;
	}
	
	public String toString(){
		return new String(name);
	}
}