package rpg.character;

import java.lang.*;
import java.io.*;
import java.awt.image.*;
import javax.imageio.*;
import javax.swing.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;

import rpg.resource.*;
import rpg.item.*;
import rpg.draw.*;
/**
 * A class that handles monsters and their files
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class MonsterHandler{
	private static String resourceFileName = "xml/monster.xml";
	private static Document doc;
	private static final int FRAME_WIDTH = 32;
	private static final int FRAME_HEIGHT = 48;
	
	static{
		try{
			File file = new File(resourceFileName);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			doc = db.parse(file);
			doc.getDocumentElement().normalize();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static Monster[] getAllMonsters(){
		NodeList nodeList = doc.getElementsByTagName("monster");
		Monster[] monsterList = new Monster[nodeList.getLength()];
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element)node;
				String name = element.getElementsByTagName("name").item(0).getTextContent();
				ImageIcon img = ResourceHandler.getIconByName("monster", element.getElementsByTagName("image")
										.item(0).getTextContent());
				int hp = Integer.parseInt(element.getElementsByTagName("hp").item(0).getTextContent());
				int strategy = Integer.parseInt(element.getElementsByTagName("strategy").item(0).
												getTextContent());
				int skillNum = Integer.parseInt(element.getElementsByTagName("skillNum").item(0).
												getTextContent());
				Skill[] skillList = new Skill[skillNum];
				
				double[] skillRate = new double[skillNum];
				int[] skillOrder = new int[0];
				
				for(int j=0; j<skillNum; j++)
					skillList[j] = ResourceHandler.getSkillByName(element.getElementsByTagName("skill").item(j).getTextContent());
				
				String normalAnimationName = element.getElementsByTagName("NormalAnimationName").item(0).getTextContent();
				String attackAnimationName = element.getElementsByTagName("AttackAnimationName").item(0).getTextContent();
				
				monsterList[i] = new Monster(name, img, hp, strategy, skillList, 
											skillRate, skillOrder, normalAnimationName, attackAnimationName);
			}
		}
		return monsterList;
	}
	
	public static Monster getMonster(String name){
		NodeList nodeList = doc.getElementsByTagName("monster");
		Monster[] monsterList = new Monster[nodeList.getLength()];
		for(int i=0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element)node;
				if(element.getElementsByTagName("name").item(0).getTextContent().equals(name)){
					ImageIcon img = ResourceHandler.getIconByName("monster", element.getElementsByTagName("image")
											.item(0).getTextContent());
					int hp = Integer.parseInt(element.getElementsByTagName("hp").item(0).getTextContent());
					int strategy = Integer.parseInt(element.getElementsByTagName("strategy").item(0).
													getTextContent());
					int skillNum = Integer.parseInt(element.getElementsByTagName("skillNum").item(0).
													getTextContent());
					Skill[] skillList = new Skill[skillNum];
					for(int j=0; j<skillNum; j++)
						skillList[j] = ResourceHandler.getSkillByName(element.getElementsByTagName("skill").item(j).getTextContent());
					
					double[] skillRate = new double[skillNum];
					int[] skillOrder = new int[0];
					
					String normalAnimationName = element.getElementsByTagName("NormalAnimationName").item(0).getTextContent();
					String attackAnimationName = element.getElementsByTagName("AttackAnimationName").item(0).getTextContent();
					
					Monster monster = new Monster(name, img, hp, strategy, skillList, skillRate, 
													skillOrder, normalAnimationName, attackAnimationName);
					return monster;
				}
			}
		}
		return null;
	}
}