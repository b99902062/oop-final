package rpg.character;

import java.lang.*;
import java.util.*;
import javax.swing.*;
import java.io.*;

import rpg.item.*;
import rpg.resource.*;
import rpg.control.*;

/**
 * A class that handles an role's status
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Role implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String name;
	private ImageIcon[] iconList; /*Down  0 to 2*/
								  /*Left  3 to 5*/
								  /*Right 6 to 8*/
								  /*Up    9 to 11*/
	private ImageIcon portrait;	
	private String title;
	
	private int status;
	private int curHP;//current HP
	private int curMP;
	private int curExp;//current exp
	
	private int level;

	private int initHP;//initial HP
	private int initMP;
	private int initAtk;
	private int initDef;
	private int initSpd;
	private int initExp;
	private int hpRate;//HP Grow Rate P.S. Current full HP = initHP + hpRate*level
	private int mpRate;
	private int atkRate;
	private int defRate;
	private int spdRate;
	private int expRate;
	
	/*Skill*/
	private ArrayList<Skill> skillList = new ArrayList<Skill>();
	private ArrayList<SkillTable> skillTable = new ArrayList<SkillTable>();
	
	/*Equipment*/
	public static final int EQUIPMENT_NUM = 5;
	private Equipment[] equipList = new Equipment[EQUIPMENT_NUM];
	
	public static int POISONED 	= 1;
	public static int SILENCE 	= 2;
	public static int PARALYSIS = 4;
	public static int BLIND 	= 8;
	public static int DEAD		= 16;
	
	//private Controller controller;//Now Controller used, use controller to set messagebar
	
	public Role(String _name, ImageIcon[] _iconList, ImageIcon _portrait, int roleId){
		name = new String(_name);
		iconList = _iconList;
		portrait = _portrait;
		
		RoleInit roleInit = ResourceHandler.getRoleStatus(roleId + "");
		this.title = roleInit.title;
		this.initHP = roleInit.initHP;
		this.initMP = roleInit.initMP;
		this.initAtk = roleInit.initAtk;
		this.initDef = roleInit.initDef;
		this.initSpd = roleInit.initSpd;
		this.initExp = roleInit.initExp;
		this.hpRate = roleInit.hpRate;
		this.mpRate = roleInit.mpRate;
		this.atkRate = roleInit.atkRate;
		this.defRate = roleInit.defRate;
		this.spdRate = roleInit.spdRate;
		this.expRate = roleInit.expRate;
		this.level = 1;
		this.curHP = this.getHP();
		this.curMP = this.getMP();
		this.curExp = 0;
		this.status = 0;
		
		for( int i=0;i<roleInit.skillNameList.length;i++ ){
			if(roleInit.skillLevelList[i] == 1)
				skillList.add( ResourceHandler.getSkillByName(roleInit.skillNameList[i]) );
			skillTable.add(new SkillTable(roleInit.skillLevelList[i], roleInit.skillNameList[i])); 
		}
		/*System.out.println("Roleskill_1Name:"+skillList.get(0).getName());
		System.out.println("Roleskill_Length:"+skillList.size());
		System.out.println("Roleskilltable_1Level:"+skillTable.get(0).getLevel());
		System.out.println("Roleskilltable_1Name:"+skillTable.get(0).getName());
		System.out.println("Roleskilltable_2Level:"+skillTable.get(1).getLevel());
		System.out.println("Roleskilltable_2Name:"+skillTable.get(1).getName());*/
		
	}
	
	public boolean checkUpgrade(){
		boolean upgrade = false;
		while(curExp >= initExp+(level*expRate)){
			curExp -= initExp+(level*expRate);
			level++;
			updateRoleState();
			updateRoleSkill();
			upgrade = true;
		}
		return upgrade;
	}
	
	public void updateRoleState(){
		this.curHP = this.getHP();
		this.curMP = this.getMP();
		this.status = 0;
		//show message
	}
	
	public void updateRoleSkill(){
		for(int i=0;i<skillTable.size();i++){
			if(skillTable.get(i).getLevel() == this.level){
				skillList.add( ResourceHandler.getSkillByName(skillTable.get(i).getName()) );
			}
		}
		//show message
	}
	
	public void showMessage(String message){
	}
	
	public Equipment[] getEquip(){
		Equipment[] e = new Equipment[equipList.length];
		System.arraycopy(equipList, 0, e, 0, equipList.length);
		return e;
	}
	
	public void addEquip(Equipment e){
		if(e instanceof Equipment && equipList[e.getPosition()] == null){
			equipList[e.getPosition()] = e;
		}
		refreshRole(e, "add");
	}
	
	public void dropEquip(int position){
		if(equipList[position] instanceof Equipment)
			refreshRole(equipList[position], "drop");
		equipList[position] = null;
	}
	
	private void refreshRole(Equipment e, String s){
		if(s.equals("add")){
			curHP += e.getHpValue(); 
			curMP += e.getMpValue();
		}
		else if(s.equals("drop")){
			curHP -= e.getHpValue(); 
			curMP -= e.getMpValue();
			if(curHP <= 0) curHP = 1;
			if(curMP <= 0) curMP = 1;
		}
	}
	
	public String getName(){
		return name;
	}
	
	public ImageIcon[] getIconList(){
		ImageIcon[] returnList = new ImageIcon[iconList.length];
		for(int i=0; i<iconList.length; i++)
			returnList[i] = iconList[i];
		return returnList;
	}
	
	public ImageIcon getPortrait(){
		return portrait;
	}
	
	public int getCurHP(){
		return curHP;
	}
	
	public int getCurMP(){
		return curMP;
	}
	
	public int getCurExp(){
		return curExp;
	}
	
	public int getStatus(){
		return status;
	}
	
	public String getStatusName(){
		if(status == 0) return "NONE";
		else if(status == 1) return "POISONED";
		else if(status == 2) return "SILENCE";
		else if(status == 4) return "PARALYSIS";
		else if(status == 8) return "BLIND";
		else if(status == 16) return "DEAD";
		else return "MULTIPLE";
	}
	
	public int getLevel(){
		return level;
	}
	
	//Current Max HP
	public int getHP(){
		int hpGain = 0;
		for(int i=0;i<EQUIPMENT_NUM;i++){
			if(equipList[i] instanceof Equipment)
				hpGain += equipList[i].getHpValue();
		}
		return initHP+(level*hpRate)+hpGain;
	}
	
	//Current Max MP
	public int getMP(){
		int mpGain = 0;
		for(int i=0;i<EQUIPMENT_NUM;i++){
			if(equipList[i] instanceof Equipment)
				mpGain += equipList[i].getMpValue();
		}
		return initMP+(level*mpRate)+mpGain;
	}
	
	//Current Max Exp
	public int getExp(){
		return initExp+(level*expRate);
	}
	
	public int getAtk(){
		int atkGain = 0;
		for(int i=0;i<EQUIPMENT_NUM;i++){
			if(equipList[i] instanceof Equipment)
				atkGain += equipList[i].getAtkValue();
		}
		return initAtk+(level*atkRate)+atkGain;
	}
	
	public int getDef(){
		int defGain = 0;
		for(int i=0;i<EQUIPMENT_NUM;i++){
			if(equipList[i] instanceof Equipment)
				defGain += equipList[i].getDefValue();
		}
		return initDef+(level*defRate)+defGain;
	}

	public int getSpd(){
		int spdGain = 0;
		for(int i=0;i<EQUIPMENT_NUM;i++){
			if(equipList[i] instanceof Equipment)
				spdGain += equipList[i].getSpdValue();
		}
		return initSpd+(level*spdRate)+spdGain;
	}
	
	public int getHpGain(){
		int hpGain = 0;
		for(int i=0;i<EQUIPMENT_NUM;i++){
			if(equipList[i] instanceof Equipment)
				hpGain += equipList[i].getHpValue();
		}
		return hpGain;
	}
	
	public int getMpGain(){
		int mpGain = 0;
		for(int i=0;i<EQUIPMENT_NUM;i++){
			if(equipList[i] instanceof Equipment)
				mpGain += equipList[i].getMpValue();
		}
		return mpGain;
	}
	
	public int getAtkGain(){
		int atkGain = 0;
		for(int i=0;i<EQUIPMENT_NUM;i++){
			if(equipList[i] instanceof Equipment)
				atkGain += equipList[i].getAtkValue();
		}
		return atkGain;
	}
	
	public int getDefGain(){
		int defGain = 0;
		for(int i=0;i<EQUIPMENT_NUM;i++){
			if(equipList[i] instanceof Equipment)
				defGain += equipList[i].getDefValue();
		}
		return defGain;
	}
	
	public int getSpdGain(){
		int spdGain = 0;
		for(int i=0;i<EQUIPMENT_NUM;i++){
			if(equipList[i] instanceof Equipment)
				spdGain += equipList[i].getSpdValue();
		}
		return spdGain;
	}
	
	public void setCurHP(int hp){
		curHP = hp;
		if(curHP > initHP+hpRate*level)
			curHP = initHP+hpRate*level;
		else if(curHP <=0)
			curHP = 0;
	}
	
	public void setCurMP(int mp){
		curMP = mp;
		if(curMP > initMP+mpRate*level)
			curMP = initMP+mpRate*level;
		else if(curMP <=0)
			curMP = 0;
	}

	public void setCurExp(int exp){
		curExp = exp;
	}
	
	public ArrayList<Skill> getSkills(){
		ArrayList<Skill> returnList = new ArrayList<Skill>();
		for(int i=0; i<skillList.size(); i++)
			returnList.add(skillList.get(i));
		return returnList;
	}
	
	public String getHpChange(Item item){
		int dif;
		Equipment e;
		if(item instanceof Equipment){
			e = (Equipment)item;
			if(equipList[e.getPosition()] instanceof Equipment)
				dif = e.getHpValue() - equipList[e.getPosition()].getHpValue();
			else
				dif = e.getHpValue();
			if(dif < 0) 
				return (" - " + (-dif));
			else if(dif > 0)
				return (" + " + dif);
			else
				return " + 0";
		}
		return "";
	}
	
	public String getMpChange(Item item){
		int dif;
		Equipment e;
		if(item instanceof Equipment){
			e = (Equipment)item;
			if(equipList[e.getPosition()] instanceof Equipment)
				dif = e.getMpValue() - equipList[e.getPosition()].getMpValue();
			else
				dif = e.getMpValue();
			if(dif < 0) 
				return (" - " + (-dif));
			else if(dif > 0)
				return (" + " + dif);
			else
				return " + 0";
		}
		return "";
	}
	
	public String getAtkChange(Item item){
		int dif;
		Equipment e;
		if(item instanceof Equipment){
			e = (Equipment)item;
			if(equipList[e.getPosition()] instanceof Equipment)
				dif = e.getAtkValue() - equipList[e.getPosition()].getAtkValue();
			else
				dif = e.getAtkValue();
			if(dif < 0) 
				return (" - " + (-dif));
			else if(dif > 0)
				return (" + " + dif);
			else
				return " + 0";
		}
		return "";
	}
	
	public String getDefChange(Item item){
		int dif;
		Equipment e;
		if(item instanceof Equipment){
			e = (Equipment)item;
			if(equipList[e.getPosition()] instanceof Equipment)
				dif = e.getDefValue() - equipList[e.getPosition()].getDefValue();
			else
				dif = e.getDefValue();
			if(dif < 0) 
				return (" - " + (-dif));
			else if(dif > 0)
				return (" + " + dif);
			else
				return " + 0";
		}
		return "";
	}
	
	public String getSpdChange(Item item){
		int dif;
		Equipment e;
		if(item instanceof Equipment){
			e = (Equipment)item;
			if(equipList[e.getPosition()] instanceof Equipment)
				dif = e.getSpdValue() - equipList[e.getPosition()].getSpdValue();
			else
				dif = e.getSpdValue();
			if(dif < 0) 
				return (" - " + (-dif));
			else if(dif > 0)
				return (" + " + dif);
			else
				return " + 0";
		}
		return "";
	}
	
	/*Below is status related*/
	public boolean isPoisoned(){
		if((status & POISONED) > 0)
			return true;
		return false;
	}
	
	public boolean isSilence(){
		if((status & SILENCE) > 0)
			return true;
		return false;
	}
	
	public boolean isParalysis(){
		if((status & PARALYSIS) > 0)
			return true;
		return false;
	}
	
	public boolean isBlind(){
		if((status & BLIND) > 0)
			return true;
		return false;
	}
	
	public boolean isDead(){
		if((status & DEAD) > 0)
			return true;
		return false;
	}
	
	public void setPoisoned(){
		status |= POISONED; 
	}
	
	public void setSilence(){
		status |= SILENCE;
	}
	
	public void setParalysis(){
		status |= PARALYSIS;
	}
	
	public void setBlind(){
		status |= BLIND;
	}
	
	public void setDead(){
		status |= DEAD;
	}
	
	public void setStatus(int value){
		status = value;
	}
}