package rpg.character;

import java.lang.*;
import java.io.*;

import rpg.game.*;
import rpg.status.*;
/**
 * A class that handles an NPC's dialogue
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Dialogue implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int startEpisode;
	private int startSubEpisode;
	private int endEpisode;
	private int endSubEpisode;
	private String[] who;
	private String[] content;
	private int contentCount;
	
	public Dialogue(int se, int sse, int ee, int ese, String c){
		startEpisode = se;
		startSubEpisode = sse;
		endEpisode = ee;
		endSubEpisode = ese;
		String[] rawStr = c.split("\n");
		who = new String[rawStr.length];
		content = new String[rawStr.length];
		for(int i=0; i<rawStr.length; i++){
			if(rawStr[i].contains(":")){
				who[contentCount] = rawStr[i].split(":")[0];
				content[contentCount] = rawStr[i].split(":")[1];
				contentCount++;
			}
		}
	}
	
	public int getStartEpisode(){
		return startEpisode;
	}
	
	public int getStartSubEpisode(){
		return startSubEpisode;
	}
	
	public int getEndEpisode(){
		return endEpisode;
	}
	
	public int getEndSubEpisode(){
		return endSubEpisode;
	}
	
	public String getContent(){
		if(who[0].contains("Me"))
			return new String(Game.gsh.getName() + ": " + content[0]);
		return new String(who[0] + ": " + content[0]);
	}
	
	public String getContent(int index){
		if(who[index].contains("Me"))
			return new String(Game.gsh.getName() + ": " + content[index]);
		return new String(who[index] + ": " + content[index]);
	}
	
	public int getContentCount(){
		return contentCount;
	}
	
	public String[] getContents(){
		String[] returnStrList = new String[contentCount];
		for(int i=0; i<contentCount; i++){
			if(who[i].contains("Me"))
				returnStrList[i] = new String(Game.gsh.getName() + ": " + content[i]);
			else
				returnStrList[i] = new String(who[i] + ": " + content[i]);
		}
		return returnStrList;
	}
	
	public String getWho(int index){
		if(who[index].contains("Me"))
			return new String(Game.gsh.getName());
		return new String(who[index]);
	}
}