package rpg.character;

import java.lang.*;
import java.util.*;
import javax.swing.*;

import rpg.item.*;

/**
 * A class that save a role's initial status
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class RoleInit{
	
	public String title;
	public int initHP;
	public int initMP;
	public int initAtk;
	public int initDef;
	public int initSpd;
	public int initExp;
	public int hpRate;
	public int mpRate;
	public int atkRate;
	public int defRate;
	public int spdRate;
	public int expRate;
	public String[] skillNameList;
	public int[] skillLevelList;
	
	public RoleInit(){
	}
	
}