package rpg.character;

import java.lang.*;
import java.util.*;
import javax.swing.*;
import java.io.*;

import rpg.item.*;
import rpg.resource.*;
import rpg.control.*;

/**
 * A class that saves the relation between skills and levels
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class SkillTable implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int level;
	private String name;
	
	public SkillTable(int _level, String _name){
		level = _level;
		name = _name;
	}
	
	public int getLevel(){
		return level;
	}
	
	public String getName(){
		return name;
	}
}