package rpg.character;

import java.lang.*;
import java.util.ArrayList;
import javax.swing.*;
import java.io.*;

import rpg.map.*;
import rpg.game.*;
import rpg.draw.*;
import rpg.control.*;
/**
 * A class that handles an NPC's status
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class NPC implements Runnable, Serializable{
	
	private static final long serialVersionUID = 1L;
	private String name;
	private ArrayList<ImageIcon> imageList;
	private ImageIcon portrait;
	private int originRow;
	private int originCol;
	private int curRow;
	private int curCol;
	private int dialogueNum;
	private int range;
	private boolean isKilled;
	private boolean isPaused;
	private boolean isMerchant;
	private boolean isBoss;
	private int battleEvent;
	private int battleEventSub;
	private Map map;
	private Dialogue[] dialogueList;
	private JLabel npcLabel;
	private JLabel roleLabel;
	private ArrayList<String> sellList;
	
	public NPC(String name, ArrayList<ImageIcon> imageList, ImageIcon portrait, int row, int col, Dialogue[] dialogueList, int range, ArrayList<String> sellList,boolean isBoss){
		this.name = new String(name);
		this.imageList = imageList;
		this.portrait = portrait;
		this.originRow = row;
		this.originCol = col;
		this.curRow = row;
		this.curCol = col;
		this.dialogueNum = dialogueList.length;
		this.dialogueList = dialogueList;
		this.range = range;
		this.sellList = sellList;
		this.isBoss = isBoss;
	}
	
	public NPC(NPC npc){
		name = new String(npc.getName());
		imageList = npc.getImageList();
		portrait = npc.getPortrait();
		originRow = npc.getRow();
		originCol = npc.getCol();
		curRow = npc.getRow();
		curCol = npc.getCol();
		dialogueList = npc.getDialogues();
		dialogueNum = dialogueList.length;
		range = npc.getRange();
		sellList = npc.getSellList();
		isBoss = npc.isBoss();
		battleEvent = npc.getBattleEvent();
		battleEventSub = npc.getBattleEventSub();
	}
	
	public void initialNPC(JLabel label, Map _map, JLabel _roleLabel){
		map = _map;
		npcLabel = label;
		roleLabel = _roleLabel;
	}
	
	public void run(){
		int[] move = {0, 0};
		boolean isMoving = false;
		int moveDist = 0;
		while(!isKilled){
			if(isPaused){
				try{
					Thread.sleep(2000);
					continue;
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
			
			if(isMoving){
				try{
					npcLabel.setLocation(npcLabel.getX() + move[1] * Controller.MOVE_AMOUNT, npcLabel.getY() + move[0] * Controller.MOVE_AMOUNT);
					moveDist += Controller.MOVE_AMOUNT;
					if(moveDist >= Drawer.LARGE_BLOCK_SIZE){
						isMoving = false;
						move[0] = 0;
						move[1] = 0;
						roleLabel.repaint();
						Thread.sleep(3000);
					}
					else{
						roleLabel.repaint();
						Thread.sleep(200);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			else{
				int dir = (int)(Math.random()*2);
				move[dir] += Math.pow((-1), (int)(Math.random()*2));
				if(canMove(curRow + move[0], curCol + move[1])){
					curRow = curRow + move[0];
					curCol = curCol + move[1];				
					npcLabel.setIcon(dirImage(move[0], move[1]));
					isMoving = true;
					moveDist = 0;
					try{
						roleLabel.repaint();
						Thread.sleep(200);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				else{
					try{
						Thread.sleep(100);
					}catch(Exception e){
						e.printStackTrace();
					}
					move[0] = 0;
					move[1] = 0;
				}
			}
		}
		System.out.println(name + "'s thread is killed");
	}
	
	public void pause(){
		isPaused = true;
	}
	
	public void resume(){
		isPaused = false;
	}
	
	public void stop(){
		isKilled = true;
	}
	
	public ImageIcon dirImage(int rowMove, int colMove){
		if(rowMove > 0)
			return imageList.get(0);
		else if(rowMove < 0)
			return imageList.get(3);
		else if(colMove > 0)
			return imageList.get(2);
		return imageList.get(1);
	}
	
	public void lookDir(String dir){
		if(dir.equals("Up"))
			npcLabel.setIcon(imageList.get(0));
		else if(dir.equals("Down"))
			npcLabel.setIcon(imageList.get(3));
		else if(dir.equals("Left"))
			npcLabel.setIcon(imageList.get(2));
		else if(dir.equals("Right"))
			npcLabel.setIcon(imageList.get(1));
	}
	
	private boolean canMove(int row, int col){
		if(row < 0 || col < 0 || row >= map.getHeight() || col > map.getWidth())
			return false;
		if(	Math.abs(row - originRow)<=range
		&& 	Math.abs(col - originCol)<=range
		&&	(Game.gsh.getCurPos()[0] != row && Game.gsh.getCurPos()[1] != col)
		&&	map.getElement(row, col) != null
		&&	!map.getElement(row, col).isObstacle())
			return true;
		return false;
	}
	
	public boolean isBoss(){
		return isBoss;
	}
	
	public void setBoss(boolean value){
		isBoss = value;
	}
	
	public String getName(){
		return new String(name);
	}
	
	public ImageIcon getImage(){
		return imageList.get(0);
	}
	
	public ArrayList<ImageIcon> getImageList(){
		return imageList;
	}
	
	public ImageIcon getPortrait(){
		return portrait;
	}
	
	public int getRow(){
		return curRow;
	}
	
	public int getCol(){
		return curCol;
	}
	
	public int getRange(){
		return range;
	}
	
	public Dialogue[] getDialogues(){
		Dialogue[] returnDialog = new Dialogue[dialogueNum];
		for(int i=0; i<dialogueNum; i++)
			returnDialog[i] = dialogueList[i];
		return returnDialog;
	}
	
	public Dialogue getDialogue(){
		for(int i=0; i<dialogueNum; i++){
			if(alreadyPass(dialogueList[i].getStartEpisode(), dialogueList[i].getStartSubEpisode())
				&& !alreadyPass(dialogueList[i].getEndEpisode(), dialogueList[i].getEndSubEpisode())){
				return dialogueList[i];
			}
		}
		return null;
	}
	
	private boolean alreadyPass(int episode, int subEpisode){
		if(subEpisode <= 0)
			return false;
		int curEpisode = Game.gsh.getEpisodeId();
		int curSubEpisode = Game.gsh.getSubEpisodeId();
		if(curEpisode < episode)
			return false;
		if(curEpisode > episode)
			return true;
		if(curSubEpisode >= subEpisode)
			return true;
		return false;
	}
	
	public ArrayList<String> getSellList(){
		return sellList;
	}
	
	public void setBattleEvent(int id, int subId){
		battleEvent = id;
		battleEventSub = subId;
		System.out.println("--- " + id + ", " + subId + "---");
	}
	
	public boolean isBattleEvent(int id, int subId){
		if(id == battleEvent && subId == battleEventSub)
			return true;
		else
			System.out.println("ID: " + id + "SUBID: " + subId + " We need( " + battleEvent + " , " + battleEventSub + ")");
		return false;
	}
	
	public int getBattleEvent(){
		return battleEvent;
	}
	
	public int getBattleEventSub(){
		return battleEventSub;
	}
	
	public String toString(){
		return name;
	}
}