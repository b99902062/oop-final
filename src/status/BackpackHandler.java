package rpg.status;

import java.lang.*;
import java.util.*;
import java.io.*;
import rpg.item.*;

/**
 * A class that handles status of the player's backpack
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class BackpackHandler implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int money;
	
	private LinkedList<Item> equipmentList = new LinkedList<Item>();
	private LinkedList<Item> potionList = new LinkedList<Item>();
	private LinkedList<Item> tossList = new LinkedList<Item>();
	private LinkedList<Item> otherList = new LinkedList<Item>();
	private LinkedList<Item> keyList = new LinkedList<Item>();
	
	public BackpackHandler(){
	}
	
	public void setMoney(int value){
		money = value;
	}
	
	public int getMoney(){
		return money;
	}
	
	public LinkedList<Item> cloneItemList(LinkedList<Item> list){
		LinkedList<Item> returnList = new LinkedList<Item>();
		for(int i=0; i<list.size(); i++)
			returnList.add(list.get(i));
		return returnList;
	}
	
	public LinkedList<Item> getEquipmentList(){
		return cloneItemList(equipmentList);
	}
	
	public LinkedList<Item> getPotionList(){
		return cloneItemList(potionList);
	}
	
	public LinkedList<Item> getTossList(){
		return cloneItemList(tossList);
	}
	
	public LinkedList<Item> getOtherList(){
		return cloneItemList(otherList);
	}
	
	public LinkedList<Item> getKeyList(){
		return cloneItemList(keyList);
	}
	
	public void addItem(Item item){
		if(item.getType() == Item.TYPE_EQUIP)
			equipmentList.add(item);
		else if(item.getType() == Item.TYPE_POTION)
			potionList.add(item);
		else if(item.getType() == Item.TYPE_TOSS)
			tossList.add(item);
		else if(item.getType() == Item.TYPE_OTHER)
			otherList.add(item);
		else if(item.getType() == Item.TYPE_KEY)
			keyList.add(item);
	}
	
	public void removeItem(Item item){
		if(item.getType() == Item.TYPE_EQUIP){
			Item target = getItemInList(item, equipmentList);
			if(target == null)
				return;
			if(target.getAmount() > 1)
				target.setAmount(target.getAmount()-1);
			else
				equipmentList.remove(target);
		}
		else if(item.getType() == Item.TYPE_POTION){		
			Item target = getItemInList(item, potionList);
			if(target == null)
				return;
			if(target.getAmount() > 1)
				target.setAmount(target.getAmount()-1);
			else
				potionList.remove(target);
		}
		else if(item.getType() == Item.TYPE_TOSS){
			Item target = getItemInList(item, tossList);
			if(target == null)
				return;
			if(target.getAmount() > 1)
				target.setAmount(target.getAmount()-1);
			else
				tossList.remove(target);
		}
		else if(item.getType() == Item.TYPE_OTHER){
			Item target = getItemInList(item, otherList);
			if(target == null)
				return;
			if(target.getAmount() > 1)
				target.setAmount(target.getAmount()-1);
			else
				otherList.remove(target);
		}
		else if(item.getType() == Item.TYPE_KEY){
			Item target = getItemInList(item, keyList);
			if(target == null)
				return;
			if(target.getAmount() > 1)
				target.setAmount(target.getAmount()-1);
			else
				keyList.remove(target);
		}
	}
	
	private Item getItemInList(Item item, LinkedList<Item> list){
		for(int i=0; i<list.size(); i++)
			if(list.get(i).getName().equals(item.getName()))
				return list.get(i);
		return null;
	}
	
	public int getItemCount(){
		return equipmentList.size() + potionList.size() + tossList.size() + otherList.size() + keyList.size();
	}
	
	private LinkedList<Item> reArrangeList(LinkedList<Item> list){
		LinkedList<Item> newList = new LinkedList<Item>();
		for(int i=0; i<list.size(); i++){
			int pos = getPositionInList(newList, list.get(i).getName());
			if(pos != -1){
				Item item = newList.get(pos);
				item.setAmount(item.getAmount() + 1);
				newList.set(pos, item);
			}
			else{
				int j = 0;
				while(true){
					if(j == newList.size() || list.get(i).getId() > newList.get(j).getId()){
						newList.add(j, list.get(i));
						break;
					}
					else
						j++;
				}
			}
		}
		return newList;
	}
	
	private int getPositionInList(LinkedList<Item> list, String itemName){
		for(int i=0; i<list.size(); i++){
			if(list.get(i).getName().equals(itemName))
				return i;
			else
				System.out.println(list.get(i).getName() + ", " + itemName);
		}
		return -1;
	}
	
	public void reArrangeLists(){
		equipmentList = reArrangeList(equipmentList);
		potionList = reArrangeList(potionList);
		tossList = reArrangeList(tossList);
		otherList = reArrangeList(otherList);
		keyList = reArrangeList(keyList);
	}
	
	public int countItem(Item item){
		int count = 0;
		LinkedList<Item> list;
		if(item.getType() == Item.TYPE_EQUIP)
			list = equipmentList;
		else if(item.getType() == Item.TYPE_POTION)
			list = potionList;
		else if(item.getType() == Item.TYPE_TOSS)
			list = tossList;
		else if(item.getType() == Item.TYPE_KEY)
			list = keyList;
		else
			list = otherList;
		for(int i=0; i<list.size(); i++)
			if(list.get(i).getName().equals(item.getName()))
				count+=list.get(i).getAmount();
		return count;
	}
}