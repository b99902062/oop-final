package rpg.status;

import java.lang.*;
import java.io.*;

/**
 * A class that handles which map are you in and the coordinate
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class LocationHandler implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int mapId;
	private int row;
	private int col;
	
	public LocationHandler(int _mapId){
		mapId = _mapId;
		int[] initPos = {5, 7};
		setCurPos(initPos);
	}
	
	public void setCurMapId(int _mapId){
		mapId =  _mapId;
	}
	public int getCurMapId(){
		return mapId;
	}
	public int[] getCurPos(){
		int[] Pos = new int[2];
		Pos[0] = row;
		Pos[1] = col;
		return Pos;
	}
	
	public void setCurPos(int[] Pos){
		row = Pos[0];
		col = Pos[1];
	}
	
	public void setLocation(int _mapId, int _row, int _col){
		row = _row;
		col = _col;
		mapId = _mapId;
	}
}