package rpg.status;

import java.lang.*;
import java.util.ArrayList;
import java.io.*;

import rpg.game.*;
import rpg.control.*;
import rpg.resource.*;
import rpg.character.*;
import rpg.item.*;
/**
 * A class that handles the progress that the player plays
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class EpisodeHandler implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int curEpisodeId;
	private int curSubEpisodeId;
	private ArrayList<Episode> episodeList;
	private int curIndex;
	
	private ArrayList<Item> getItemList;
	private ArrayList<Item> loseItemList;
	private ArrayList<Role> getRoleList;
	private ArrayList<Role> loseRoleList;
	
	public EpisodeHandler(){
		curEpisodeId = 1;
		curSubEpisodeId = 1;
		curIndex = 0;
		episodeList = ResourceHandler.getEpisodes();
	}
	
	public boolean checkEvent(NPC npc){
		if(curIndex >= episodeList.size()){
			System.out.println("No further episode");
			return false;
		}
		Episode episode = episodeList.get(curIndex);
		if(episode.getTargetName().equals(npc.getName())){
			if(episode.getType() == 1){
				progressStory();
				
				getItemList = episode.getGetItemList();
				if(getItemList != null){
					for(int i=0; i<getItemList.size(); i++){
						if(getItemList.get(i).getName().equals("MONEY"))
							Game.gsh.setMoney(Game.gsh.getMoney() + getItemList.get(i).getAmount());
						else
							Game.gsh.addItem(getItemList.get(i));
					}
				}
				
				loseItemList = episode.getLoseItemList();
				if(loseItemList != null){
					for(int i=0; i<loseItemList.size(); i++){
						if(loseItemList.get(i).getName().equals("MONEY"))
							Game.gsh.setMoney(Game.gsh.getMoney() - loseItemList.get(i).getAmount());
						else
							Game.gsh.removeItem(loseItemList.get(i));
					}
				}
				return true;
	
			}
			else if(episode.getType() == 2){
				return true;
				// if(Game.gsh.ifHave(episode.getNeedItemList()))
					// progressStory();
			}
			else if(episode.getType() == 3){
				return true;
				// if(Game.gsh.ifHave(episode.getNeedItemList()))
					// progressStory();
			}
		}
		return false;
	}
	
	public void progressStory(){
		if(curSubEpisodeId < ResourceHandler.getSubEpisodeNum(curEpisodeId))
			curSubEpisodeId++;
		else{
			curEpisodeId++;
			curSubEpisodeId = 1;
		}
		curIndex++;
	}
	
	public int getEpisodeId(){
		return curEpisodeId;
	}
	
	public int getSubEpisodeId(){
		return curSubEpisodeId;
	}
	
	public Episode getEpisode(int id, int subId){
		for(int i=0; i<episodeList.size(); i++)
			if(episodeList.get(i).getId() == id
			&& episodeList.get(i).getSubId() == subId)
				return episodeList.get(i);
		return null;
	}
	
	public ArrayList<Item> getGetItemList(){
		return getItemList;
	}
	
	public ArrayList<Item> getLoseItemList(){
		return loseItemList;
	}
	
	public ArrayList<Role> getGetRoleList(){
		return getRoleList;
	}
	
	public ArrayList<Role> getLoseRoleList(){
		return loseRoleList;
	}
	
	public boolean isLimitArea(int mapId){
		if(curIndex >= episodeList.size())
			return false;
		Episode episode = episodeList.get(curIndex);
		ArrayList<Integer> list = episode.getBoundMapList();
		if(list == null)
			return true;
		for(int i=0; i<list.size(); i++)
			if(mapId == list.get(i))
				return false;
		return true;
	}
	
	public String getBoundWords(){
		if(curIndex >= episodeList.size())
			return "I should stay here until the programmer writes new story.";
		return episodeList.get(curIndex).getBoundWords();
	}
	
	public int getEpisodeTotal(){
		return episodeList.size();
	}
	
	public void reloadEpisodes(){
		episodeList = ResourceHandler.getEpisodes();
		for(int i=0; i<episodeList.size(); i++)
			if(episodeList.get(i).getId() == curEpisodeId
			&& episodeList.get(i).getSubId() == curSubEpisodeId){
				curIndex = i;
				break;
			}
		
	}
	/** Normal case the input should be bigger than current storyId*/
	
	
}