package rpg.status;

import java.lang.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.*;
import java.net.*;
import java.util.LinkedList;
import java.util.ArrayList;

import javax.swing.*;
import javax.imageio.*;

import rpg.draw.*;
import rpg.game.*;
import rpg.item.*;
import rpg.resource.*;
import rpg.map.*;
import rpg.audio.*;
import rpg.status.*;
import rpg.character.*;
import rpg.control.*;

/**
 * A class that handles all status that need to save and will be used during the game
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class GameStatusHandler implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private int charType;
	private String saveTime;
	private EpisodeHandler eh;
	private TreasureHandler th;
	private NPCHandler npch;
	private PlayerHandler ph;
	private BackpackHandler bh;
	private LocationHandler lh;
	private ImageIcon captureImage;
	private boolean isBattle;
	private boolean isReload;
	private boolean isBoss;
	private String BossName;
	
	public GameStatusHandler(String _name,int _charType){
		name = _name;
		charType = _charType;
		eh = new EpisodeHandler();
		th = new TreasureHandler();
		npch = new NPCHandler();
		ph = new PlayerHandler(name, charType);
		bh = new BackpackHandler();
		lh = new LocationHandler(1);
		isBoss = false;
	}
	
	public String getName(){
		return new String(name);
	}
	
	public ImageIcon[] getCharImage(){
		return ph.getRole().getIconList();
	}
	
	public int getCharType(){
		return charType;
	}
	
	public void setLocation(int mapId, int row, int col){
		lh.setLocation(mapId, row, col);
	}
	
	public int getCurMapId(){
		return lh.getCurMapId();
	}
	
	public Map getCurMap(){
		return ResourceHandler.getMapById(getCurMapId() + "");
	}
	
	public void setCurMapId(int mapId){
		lh.setCurMapId(mapId);
	}
	
	public int[] getCurPos(){
		return lh.getCurPos();
	}
	
	public void setCurPos(int[] Pos){
		lh.setCurPos(Pos);
	}
	
	public NPC[] getMapNPC(){
		return npch.getMapNPC(getCurMapId());
	}
	
	public NPC getNPCByName(String name){
		return npch.getNPCByName(name);
	}
	
	public int getEpisodeId(){
		return eh.getEpisodeId();
	}
	
	public int getSubEpisodeId(){
		return eh.getSubEpisodeId();
	}
	
	public Episode getEpisode(int id, int subId){
		return eh.getEpisode(id, subId);
	}
	
	public int getEpisodeTotal(){
		return eh.getEpisodeTotal();
	}
	
	public boolean checkEvent(NPC npc){
		return eh.checkEvent(npc);
	}
	
	public boolean isRole(String name){
		return ph.isRole(name);
	}
	
	public Role getRole(String name){
		return ph.getRole(name);
	}
	
	public Role getRole(int index){
		return ph.getRole(index);
	}
	
	public LinkedList<Role> getRoles(){
		return ph.getRoleList();
	}
	
	public boolean getBattle(){
		return isBattle;
	}
	
	public void setBattle(boolean battle){
		isBattle = battle;
	}
	
	public void addItem(Item item){
		bh.addItem(item);
	}
	
	public void removeItem(Item item){
		bh.removeItem(item);
	}
	
	public int getMoney(){
		return bh.getMoney();
	}
	
	public void setMoney(int value){
		bh.setMoney(value);
	}
	
	public int getItemCount(){
		return bh.getItemCount();
	}
	
	public int countItem(Item item){
		return bh.countItem(item);
	}
	
	public void reArrangeLists(){
		bh.reArrangeLists();
	}
	
	public LinkedList<Item> getItems(){
		LinkedList<Item> returnList = new LinkedList<Item>();
		LinkedList<Item> tmpList = bh.getEquipmentList();
		for(int i=0; i<tmpList.size(); i++)
			returnList.add(tmpList.get(i));
		tmpList = bh.getPotionList();
		for(int i=0; i<tmpList.size(); i++)
			returnList.add(tmpList.get(i));
		tmpList = bh.getTossList();
		for(int i=0; i<tmpList.size(); i++)
			returnList.add(tmpList.get(i));
		tmpList = bh.getOtherList();
		for(int i=0; i<tmpList.size(); i++)
			returnList.add(tmpList.get(i));
		tmpList = bh.getKeyList();
		for(int i=0; i<tmpList.size(); i++)
			returnList.add(tmpList.get(i));
		return returnList;
	}
	
	public LinkedList<Item> getPotionList(){
		return bh.getPotionList();
	}
	
	public LinkedList<Item> getTossList(){
		return bh.getTossList();
	}
	
	public LinkedList<Role> getRoleList(){
		return ph.getRoleList();
	}
	
	public ArrayList<Item> getGetItemList(){
		return eh.getGetItemList();
	}
	
	public ArrayList<Item> getLoseItemList(){
		return eh.getLoseItemList();
	}
	
	public ArrayList<Role> getGetRoleList(){
		return eh.getGetRoleList();
	}
	
	public ArrayList<Role> getLoseRoleList(){
		return eh.getLoseRoleList();
	}
	
	public boolean isLimitArea(int id){
		return eh.isLimitArea(id);
	}
	
	public String getBoundWords(){
		return eh.getBoundWords();
	}
	
	public void reloadEpisodes(){
		eh.reloadEpisodes();
	}
	
	public ImageIcon getCaptureImage(){
		return captureImage;
	}
	
	public void setCaptureImage(ImageIcon img){
		captureImage = img;
	}
	
	public boolean isReload(){
		return isReload;
	}
	
	public void setReload(boolean value){
		isReload = value;
	}
	
	public LinkedList<Item> getEquipmentList(){
		return bh.getEquipmentList();
	}
	
	public void setBoss(boolean value){
		isBoss = value;
	}
	
	public boolean getBoss(){
		return isBoss;
	}
	
	public void setBossName(String name){
		BossName = name;
	}
	
	public String getBossName(){
		return BossName;
	}
	
	public String getTime(){
		return saveTime;
	}
	
	public void setTime(String time){
		saveTime = time;
	}
	
}