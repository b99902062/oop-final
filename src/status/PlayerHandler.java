package rpg.status;

import java.lang.*;
import java.util.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;

import rpg.resource.*;
import rpg.character.*;
/**
 * A class that handles all role's that player can control and their status
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class PlayerHandler implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private LinkedList<Role> roleList = new LinkedList<Role>();
	
	public PlayerHandler(String name, int type){
		ImageIcon[] iconList = readIconList("role0"+type);
		ImageIcon portrait = ResourceHandler.getIconByName("role", "role0"+type+"Face");
		int roleId = type;
		Role role = new Role(name, iconList, portrait, roleId);
		addRole(role);
		
		//Add another one
		if(type == 0){
			type = 1;
			name = name + "'s girlfriend";
		}
		else{
			type = 0;
			name = name + "'s boyfriend";
		}
		ImageIcon[] iconList2 = readIconList("role0"+type);
		ImageIcon portrait2 = ResourceHandler.getIconByName("role", "role0"+type+"Face");
		Role role2 = new Role(name, iconList2, portrait2, type);
		addRole(role2);
	}
	
	private ImageIcon[] readIconList(String name){
		ImageIcon[] iconList = new ImageIcon[12];
		BufferedImage[] roleBuffer = ResourceHandler.getHumanAnimationFrames(name);
		for(int i=0 ;i<roleBuffer.length ;i++)
			iconList[i] = new ImageIcon(roleBuffer[i]);
		return iconList;
	}
	
	public LinkedList<Role> getRoleList(){
		return roleList;
	}
	
	public void addRole(Role c){
        roleList.add(c);
    }
    
    public Role getRole(){
		return roleList.get(0);
    }
	
	public Role getRole(int index){
		return roleList.get(index);
    }
	
	public Role getRole(String name){
		for(int i=0; i<roleList.size(); i++){
			if(roleList.get(i).getName().equals(name))
				return roleList.get(i);
		}
		return null;
	}
	
	public boolean isRole(String name){
		for(int i=0; i<roleList.size(); i++){
			if(roleList.get(i).getName().equals(name))
				return true;
		}
		return false;
	}
	
}