package rpg.status;

import java.lang.*;
import java.util.ArrayList;
import java.io.*;

import rpg.game.*;
import rpg.character.*;
import rpg.resource.*;

/**
 * A class that handles all NPC's status
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class NPCHandler implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private NPC[][] NPCList;
	private int[] mapIdList;
	private int mapAmount;
	
	public NPCHandler(){
		mapAmount = ResourceHandler.getResourceAmount("map");
		setNPCList();
	}
	
	private void setNPCList(){
		NPCList = new NPC[mapAmount][];
		mapIdList = ResourceHandler.getMapIdList();
		for(int i=0; i<mapAmount; i++){
			int npcAmount = ResourceHandler.getNPCAmountByMapId(mapIdList[i] + "");
			if(npcAmount > 0){
				NPCList[i] = new NPC[npcAmount];
				for(int j=0; j<npcAmount; j++){
					NPCList[i][j] = ResourceHandler.getNPC(mapIdList[i] + "", j + "");
				}
			}
			else
				NPCList[i] = new NPC[0];
		}
	}
	
	public NPC[] getMapNPC(String mapId){
		for(int i=0; i<mapAmount; i++){
			if(Integer.parseInt(mapId) == mapIdList[i]){
				// NPC[] returnList = new NPC[NPCList[i].length];
				// for(int j=0; j<NPCList[i].length; j++)
					// returnList[j] = new NPC(NPCList[i][j]);
				// return returnList;
				return NPCList[i];
			}
		}
		return null;
	}
	
	public NPC[] getMapNPC(int mapId){
		return getMapNPC(mapId + "");
	}
	
	public NPC getNPC(String mapId, int id){
		for(int i=0; i<mapAmount; i++){
			if(Integer.parseInt(mapId) == mapIdList[i]){
				if(id < NPCList[i].length)
					return NPCList[i][id];
				else
					return null;
			}
		}
		return null;
	}
	
	public NPC getNPC(int mapId, int id){
		return getNPC(mapId + "", id);
	}
	
	public NPC getNPCByName(String name){
		for(int i=0; i<NPCList.length; i++)
			for(int j=0; j<NPCList[i].length; j++)
				if(NPCList[i][j].getName().equals(name))
					return NPCList[i][j];
		return null;
	}
}