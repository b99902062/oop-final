package rpg.map;

import java.lang.*;
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import java.util.*;

import rpg.draw.*;

/**
 * A class that represents a piece of map
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class MapElement implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private ArrayList<ImageIcon> imageList = new ArrayList<ImageIcon>();
	private boolean isObstacle;
	private boolean isExit;
	private int nextMapId;
	private int nextMapPosRow;
	private int nextMapPosCol;
	private int curIndex;
	
	public MapElement(ImageIcon _image, boolean _isObstacle, boolean _isExit, int _nextMapId, int _row, int _col, int _curIndex){
		imageList.add(_image);
		isObstacle = _isObstacle;
		isExit = _isExit;
		nextMapId = _nextMapId;
		curIndex = _curIndex;
		nextMapPosRow = _row;
		nextMapPosCol = _col;
	}
	
	public MapElement(MapElement element){
		isObstacle = element.isObstacle();
		isExit = element.isExit();
		nextMapId = element.getNextMapId();
		nextMapPosRow = element.getNextPosRow();
		nextMapPosCol = element.getNextPosCol();
		curIndex = element.getCurIndex();
		for(int i=0; i<element.imageList.size(); i++)
			imageList.add(element.imageList.get(i));
	}
	
	public ImageIcon getNextFrame(){
		ImageIcon img = getImage(curIndex);
		curIndex = (curIndex + 1) % imageList.size();
		return img;
	}
	
	public void setImage(ImageIcon img){
		imageList.set(0, img);
	}
	
	public void setImage(int index, ImageIcon img){
		imageList.set(index, img);
	}
	
	public ImageIcon getImage(){
		return imageList.get(0); 
	}
	
	public ImageIcon getNextScaledFrame(int rowSize, int colSize){
		ImageIcon img = getScaledImage(curIndex, rowSize, colSize);
		curIndex = (curIndex + 1) % imageList.size();
		return img;
	}
	
	public ImageIcon getScaledImage(int rowSize, int colSize){
		return new ImageIcon(Drawer.getResizedImg(imageList.get(0).getImage(), rowSize, colSize));
	}
	
	public ImageIcon getScaledImage(int index, int rowSize, int colSize){
		return new ImageIcon(Drawer.getResizedImg(imageList.get(index).getImage(), rowSize, colSize));
	}
	
	public ImageIcon getImage(int index){
		return imageList.get(index); 
	}
	
	public ImageIcon getImageList(){
		return imageList.get(0); 
	}
	
	public int getListLength(){
		return imageList.size();
	}
	
	public void addImage(ImageIcon img){
		imageList.add(img);
	}
	
	public void setObstacle(boolean value){
		isObstacle = value;
	}
	
	public boolean isObstacle(){
		return isObstacle;
	}
	
	public void setExit(boolean value){
		isExit = value;
	}
	
	public boolean isExit(){
		return isExit;
	}
	
	public void setNextMapId(int id){
		nextMapId = id;
	}
	
	public int getNextMapId(){
		return nextMapId;
	}
	
	public void setCurIndex(int index){
		curIndex = index;
	}
	
	public int getCurIndex(){
		return curIndex;
	}
	
	public int getNextPosCol(){
		return nextMapPosCol;
	}
	
	public int getNextPosRow(){
		return nextMapPosRow;
	}
	
	public void setNextPosRow(int row){
		nextMapPosRow = row;
	}
	
	public void setNextPosCol(int col){
		nextMapPosCol = col;
	}
}