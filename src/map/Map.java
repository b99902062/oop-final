package rpg.map;

import java.lang.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;

import rpg.character.*;
import rpg.resource.*;

/**
 * A class that represents a map
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Map implements Serializable, Cloneable{
	
	private static final long serialVersionUID = 1L;
	
	private int width;
	private int height;
	private int id;
	private String name;
	private String type;
	private MapElement[][] mapElement;
	private Monster[] monsterList;
	private double[] meetRate;
	
	public Map(int _width, int _height, int _id, String _name, String _type){
		width = _width;
		height = _height;
		id = _id;
		name = new String(_name);
		type = new String(_type);
		initialElement();
	}
	
	public Map(Map map){
		width = map.width;
		height = map.height;
		id = map.id;
		name = new String(map.name);
		type = new String(map.type);
		initialElement();
		for(int row=0; row<height; row++)
			for(int col=0; col<width; col++)
				setElement(row, col, map.getElement(row, col));
	}
	
	private void initialElement(){
		mapElement = new MapElement[height][width];
		for(int row=0; row < height; row++)
			mapElement[row] = new MapElement[width];
	}
	
	public int getWidth(){
		return width;
	}
	
	public int getHeight(){
		return height;
	}
	
	public int getId(){
		return id;
	}
	
	public String getName(){
		return new String(name);
	}
	
	public String getType(){
		return type;
	}
	
	public Monster[] getMonsters(){
		Monster[] returnList = new Monster[monsterList.length];
		for(int i=0; i<monsterList.length; i++)
			returnList[i] = ResourceHandler.getMonster(monsterList[i].getName());
		return returnList;
	}
	
	public Monster getMonster(int index){
		return ResourceHandler.getMonster(monsterList[index].getName());
	}
	
	public double[] getRateList(){
		return meetRate;
	}
	
	public double getRate(int index){
		return meetRate[index];
	}
	
	public int getMonsterCount(){
		return monsterList.length;
	}
	
	public void setElement(int row, int col, MapElement element){
		if(element == null)
			mapElement[row][col] = null;
		else
			mapElement[row][col] = new MapElement(element);
	}
	
	public MapElement getElement(int row, int col){
		MapElement element;
		if(mapElement[row][col] == null)
			return null;
		element = new MapElement(mapElement[row][col]);
		return element;
	}
	
	public ImageIcon getNextFrame(int row, int col){
		return mapElement[row][col].getNextFrame();
	}
	
	public ImageIcon getNextScaledFrame(int row, int col, int rowSize, int colSize){
		return mapElement[row][col].getNextScaledFrame(rowSize, colSize);
	}
	
	public void setMonsters(Monster[] list, double[] rate){
		monsterList = new Monster[list.length];
		meetRate = new double[list.length];
		for(int i=0; i<list.length; i++){
			monsterList[i] = new Monster(list[i]);
			meetRate[i] = rate[i];
		}
	}
}