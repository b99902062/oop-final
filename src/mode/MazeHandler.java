package rpg.mode;

import java.lang.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.*;
import java.net.*;

import javax.swing.*;
import javax.imageio.*;

import rpg.draw.*;
import rpg.game.*;
import rpg.resource.*;
import rpg.map.*;
import rpg.audio.*;
import rpg.status.*;
import rpg.control.*;
import rpg.character.*;

/**
 * A class that handle the maze Handler
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class MazeHandler extends PanelHandler{
	private static final long serialVersionUID = 1L;
	
	private String bgImageName = ResourceHandler.getResourcePath("image", "defaultBGI");
	private AudioPlayer bgPlayer;
	private Controller controller;
	private Thread musicThread;
	private Thread mapHandleThread;
	private BufferedImage bgImg;
	private Map curMap;
	private int mapWidth;
	private int mapHeight;
	private JLabel charLabel;
	private NPC[] mapNPC;
	
	private boolean isOut;
	
	public int nextMode = -1;
	
	public static final double RISING_RATE = 0.988;
	
	public synchronized int execute(){
		while(!isOut){
			bgPlayer = new AudioPlayer(true, ResourceHandler.getResourcePath("music", "mazeBGM"));
			musicThread = new Thread(bgPlayer);
			musicThread.start();
			Game.gui.requestFocus();
			initialPanel();
			try{
				isOut = controller.waitForOut("Maze");
				System.out.println(isOut);
				bgPlayer.stop();
				for(int i=0; i<mapNPC.length; i++)
					mapNPC[i].stop();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		if(Game.gsh.getCurMap().getType().equals("Village"))
			return InterfaceHandler.VILLAGE_MODE;
		else if(Game.gsh.getCurMap().getType().equals("BigMap"))
			return InterfaceHandler.BIGMAP_MODE;
		if(Game.gsh.getBattle())
			return InterfaceHandler.BATTLE_MODE;
		return InterfaceHandler.LOGIN_MODE;
	}
	
	private void drawNPC(){
		mapNPC = Game.gsh.getMapNPC();
		for(int i=0; i<mapNPC.length; i++){
			JLabel label = new JLabel(mapNPC[i].getImage());
			label.setBounds(mapNPC[i].getCol() * Drawer.LARGE_BLOCK_SIZE, mapNPC[i].getRow() * Drawer.LARGE_BLOCK_SIZE, Drawer.LARGE_BLOCK_SIZE, Drawer.LARGE_BLOCK_SIZE);
			panel.add(label, 1);
			mapNPC[i].initialNPC(label, curMap, charLabel);
			Thread npcThread = new Thread(mapNPC[i]);
			npcThread.start();
		}
	}
	
	private void initialPanel(){
		drawBackGround();
		drawMap(Game.gsh.getCurMapId());
		drawRole();
		drawNPC();
		controller = new Controller(panel, block, curMap, charLabel, mapNPC);
		Game.gui.add(panel);
		panel.setLocation(Game.gui.getWidth() / 2 - Game.gsh.getCurPos()[1] * Drawer.LARGE_BLOCK_SIZE+25, Game.gui.getHeight() / 2 - Game.gsh.getCurPos()[0] * Drawer.LARGE_BLOCK_SIZE+40);
		mapHandleThread = new Thread(controller);
		mapHandleThread.start();
	}
	
	private void drawBackGround(){
		try{
			BufferedImage bgImg = ImageIO.read(new FileInputStream(new File(bgImageName)));
			bgImg = Drawer.getResizedImg(bgImg, Drawer.MAX_PANEL_SIZE, Drawer.MAX_PANEL_SIZE);
			Game.gui.setContentPane(Drawer.drawBackGround(bgImg));
			panel = Drawer.drawBackGround(bgImg);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void drawMap(int mapId){
		curMap = ResourceHandler.getMapById(mapId + "");
		mapWidth = curMap.getWidth();
		mapHeight = curMap.getHeight();
		intialBlock();
	}
	
	private void drawRole(){
		charLabel = new JLabel(Game.gsh.getCharImage()[1]);
		charLabel.setBounds(Game.gui.getWidth()/2, Game.gui.getHeight()/2, Drawer.LARGE_BLOCK_SIZE, Drawer.LARGE_BLOCK_SIZE);
		Game.gui.add(charLabel, 0);
	}
	
	private void intialBlock(){
		try{
			block = new JLabel[mapHeight][mapWidth];
			for(int row=0; row<mapHeight; row++){
				block[row] = new JLabel[mapWidth];
				for(int col=0; col<mapWidth; col++){
					MapElement element = curMap.getElement(row, col);
					if(element == null){
						block[row][col] = new JLabel();
					}
					else
						block[row][col] = new JLabel(element.getScaledImage(Drawer.LARGE_BLOCK_SIZE, Drawer.LARGE_BLOCK_SIZE));
					block[row][col].setBounds(Drawer.LARGE_BLOCK_SIZE * col, Drawer.LARGE_BLOCK_SIZE * row, Drawer.LARGE_BLOCK_SIZE, Drawer.LARGE_BLOCK_SIZE);
					panel.add(block[row][col]);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println(panel.getSize().getHeight() + ", " + panel.getSize().getWidth());
	}
}