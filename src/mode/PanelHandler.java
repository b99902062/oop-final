package rpg.mode;

import java.lang.*;
import javax.swing.*;

import rpg.draw.*;
import rpg.game.*;

/**
 * An abstract class that inherits by several different specific interface handler
 * @author  Chander, Moe, William
 * @version 1.0
 */
public abstract class PanelHandler{
	
	protected JPanel panel;
	protected JLabel[][] block;
	
	public abstract int execute();
}