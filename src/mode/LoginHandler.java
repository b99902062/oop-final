package rpg.mode;

import java.lang.*;
import java.util.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.*;
import java.net.*;

import javax.swing.*;
import javax.imageio.*;

import rpg.draw.*;
import rpg.game.*;
import rpg.resource.*;
import rpg.audio.*;
import rpg.status.*;
import rpg.control.*;

/**
 * A class that handle the login interface
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class LoginHandler extends PanelHandler implements KeyListener{
	private static final long serialVersionUID = 1L;

	private	BufferedImage bgImage = ResourceHandler.getImageByName("background", "loginBGI");
	private BufferedImage titleImage = ResourceHandler.getImageByName("other", "loginTitle");
	private AudioPlayer bgPlayer = new AudioPlayer(true, ResourceHandler.getResourcePath("music", "loginBGM"));
	private AudioPlayer switchPlayer = new AudioPlayer(false, ResourceHandler.getResourcePath("sound", "switchSound"));
	private Thread musicThread;
	
	private JLabel[] labelList;
	private int wordAmount = 3;
	private String[] wordList = { "LoadGame", "NewGame", "Exit" };
	private int currentWord = 0;
	
	private final int textItemWidth = 600;
	private final int textItemHeight = 100;
	
	private boolean isNewGame = false;
	private boolean isLoadGame = false;
	
	public synchronized int execute(){
		musicThread = new Thread(bgPlayer);
		musicThread.start();
		
		initialPanel();
		try{
			wait();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		if(isNewGame)
			return InterfaceHandler.CREATECHAR_MODE;
		else if(isLoadGame)
			return -1;
		else
			return -1;
		
	}
	
	private void initialPanel(){
		drawBackGround();
		addTitleImage();
		addInitialWord();
		Game.gui.setContentPane(panel);
		Game.gui.revalidate();
		Game.gui.requestFocus();
		Game.gui.addKeyListener(this);
	}
	
	private void drawBackGround(){
		try{
			bgImage = Drawer.getResizedImg(bgImage, Game.gui.getWidth(), Game.gui.getHeight());
			panel = Drawer.drawBackGround(bgImage);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void addTitleImage(){
		try{
			Drawer.drawImageOnScreen(panel, titleImage, (Game.gui.getWidth() - titleImage.getWidth())/2, 0);
			Game.gui.revalidate();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void addInitialWord(){
		labelList = new JLabel[wordAmount];
		for( int i=0;i<wordAmount;i++ ){
			labelList[i] = new JLabel( wordList[i], JLabel.CENTER );
			labelList[i].setFont( new Font("Trebuchet MS", Font.BOLD, 70) );
			if( i==0 ) labelList[i].setForeground(Color.blue);
			else labelList[i].setForeground(Color.darkGray);
			labelList[i].setBounds((Game.gui.getWidth() - textItemWidth)/2, Game.gui.getHeight()/2 + textItemHeight * i, textItemWidth, textItemHeight);
			panel.add( labelList[i] );
		}
	}
	
	public void keyTyped(KeyEvent e){
	}
	
	public synchronized void keyPressed(KeyEvent e){
		int keyCode = e.getKeyCode();
		if(keyCode == Game.UP){
			if(currentWord != 0){
				Thread soundThread = new Thread(switchPlayer);
				soundThread.start();
				labelList[currentWord].setForeground(Color.darkGray);
				currentWord--;
				labelList[currentWord].setForeground(Color.blue);
			}
		}
		else if(keyCode == Game.DOWN){
			if(currentWord != (wordAmount-1)){
				Thread soundThread = new Thread(switchPlayer);
				soundThread.start();
				labelList[currentWord].setForeground(Color.darkGray);
				currentWord++;
				labelList[currentWord].setForeground(Color.blue);
			}
		}
		else if(keyCode == Game.ENTER){
			if(labelList[currentWord].getText().equals("Exit"))
				System.exit(0);
			else if(labelList[currentWord].getText().equals("NewGame")){
				bgPlayer.stop();
				isNewGame = true;
				Game.gui.removeKeyListener(this);
				notify();
			}
			else{
				Game.gui.removeKeyListener(this);
				SaveMenu sm = new SaveMenu(false, this, false);
				JPanel glassMenu = (JPanel)Game.gui.getGlassPane();
				glassMenu.add(sm);
				return;
			}
		}
	}
	
	public void keyReleased(KeyEvent e){
	}
	
	public synchronized void doReload(){
		bgPlayer.stop();
		isNewGame = true;
		Game.gui.removeKeyListener(this);
		notify();
	}
}