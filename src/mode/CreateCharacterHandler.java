package rpg.mode;

import java.lang.*;
import java.util.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.*;
import java.net.*;

import javax.swing.*;
import javax.imageio.*;

import rpg.draw.*;
import rpg.game.*;
import rpg.resource.*;
import rpg.audio.*;
import rpg.status.*;

/**
 * A class that handle the Role creating interface
 * @author  Chander, Moe, William
 * @version 1.0
 */

public class CreateCharacterHandler extends PanelHandler implements KeyListener{
	private static final long serialVersionUID = 1L;
	
	private String bgImageName = ResourceHandler.getResourcePath("image", "createBG");
	private String titleImageName = ResourceHandler.getResourcePath("image", "loginTitle");
	private AudioPlayer bgPlayer = new AudioPlayer(true, ResourceHandler.getResourcePath("music", "createBGM"));
	private JTextField NameField;
	private JLabel[] GenderButton;
	private Animation[] RoleAnimation;
	private Thread[] animationThread;
	private int CharType;
	private int nowKeyPos;
	private String name;
	private boolean GoToNextMode = false;
	private Thread musicThread;
	String[] wordList = { "Male", "Female"}; 
	
	public synchronized int execute(){
		musicThread = new Thread(bgPlayer);
		musicThread.start();
		nowKeyPos = 0;
		initialPanel();
		NameField.requestFocus();
		try{
			wait();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		for(int i=0; i<RoleAnimation.length; i++)
			RoleAnimation[i].stop();
		return InterfaceHandler.VILLAGE_MODE;
	}
	
	private void initialPanel(){
		drawBackGround();
		addGenderChooseButton();
		addTitleImage();
		addNameInput();
		Game.gui.setContentPane(panel);
		Game.gui.revalidate();
	}
	
	private void addTitleImage(){
		try{
			BufferedImage img = ImageIO.read(new FileInputStream(new File(titleImageName)));
			Drawer.drawImageOnScreen(panel, img, (Game.gui.getWidth() - img.getWidth())/2, 0);
			Game.gui.revalidate();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void drawBackGround(){
		try{
			BufferedImage img = ImageIO.read(new FileInputStream(new File(bgImageName)));
			img = Drawer.getResizedImg(img, Game.gui.getWidth(), Game.gui.getHeight());
			panel = Drawer.drawBackGround(img);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void addNameInput(){
		JLabel label = new JLabel("Name:");
		label.setFont( new Font("Trebuchet MS", Font.BOLD, 50) );
		label.setBounds(400,400,200,50);
		NameField = new JTextField();
		NameField.setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
		NameField.setHorizontalAlignment(JTextField.CENTER); 
		NameField.setBounds(600,400,400,50);
		NameField.setOpaque(false);
		NameField.setBorder(BorderFactory.createLineBorder(Color.black));
		NameField.addKeyListener(this);
		panel.add(label);
		panel.add(NameField);
	}
	
	private void addGenderChooseButton(){
		GenderButton = new JLabel[2];
		RoleAnimation = new Animation[2];
		animationThread = new Thread[2];
		
		for(int i=0 ;i<2 ;i++)
		{
			GenderButton[i] = new JLabel(wordList[i]);
			GenderButton[i].setBounds(400*(i+1),500,200,100);
			GenderButton[i].setOpaque(false);
			GenderButton[i].setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
			GenderButton[i].setForeground(Color.gray);
			panel.add(GenderButton[i]);
			JLabel label = new JLabel(new ImageIcon());
			label.setBounds(400*(i+1), 600, 100, 100);
			panel.add(label);
			RoleAnimation[i] = new Animation("Human", "testAnimation"+i, label);
			animationThread[i] = new Thread(RoleAnimation[i]);
			animationThread[i].start();
		}
	}
	
	public void keyTyped(KeyEvent e){
	}
	
	public synchronized void keyPressed(KeyEvent e){
		int keyCode = e.getKeyCode();
		if(keyCode == Game.LEFT)
		{
			if(nowKeyPos == 2)
			{
				GenderButton[nowKeyPos-1].setForeground(Color.gray);
				nowKeyPos--;
				GenderButton[nowKeyPos-1].setForeground(Color.blue);
			}
		}
		else if(keyCode == Game.RIGHT)
		{
			if(nowKeyPos == 1)
			{
				GenderButton[nowKeyPos-1].setForeground(Color.gray);
				nowKeyPos++;
				GenderButton[nowKeyPos-1].setForeground(Color.red);
			}
		}
		else if(keyCode == Game.ENTER)
		{
			if(nowKeyPos == 0)
			{
				name = NameField.getText();
				if(name.isEmpty() == false)
				{
					nowKeyPos = 1;
					GenderButton[nowKeyPos-1].setForeground(Color.blue);
					NameField.removeKeyListener(this);
					Game.gui.requestFocus();
					Game.gui.addKeyListener(this);
				}
			}
			else
			{
				CharType = nowKeyPos-1;
				GoToNextMode = true;
				Game.gsh = new GameStatusHandler(name,CharType);
				bgPlayer.stop();
				Game.gui.removeKeyListener(this);
				notify();
			}
		}
	}
	public void keyReleased(KeyEvent e){
	}
}