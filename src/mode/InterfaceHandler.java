package rpg.mode;

import java.lang.*;

import rpg.mode.*;
import rpg.game.*;
import rpg.status.*;
/**
 * A class that handle which interface should be showed to user
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class InterfaceHandler{
	private int mode;
	public static final int LOGIN_MODE 		= 0;
	public static final int VILLAGE_MODE 	= 1;
	public static final int MAZE_MODE 		= 2;
	public static final int BIGMAP_MODE 	= 3;
	public static final int BATTLE_MODE 	= 4;
	public static final int MENU_MODE 		= 5;
	public static final int CREATECHAR_MODE = 6;
	private static final String handlerList[] = {"rpg.mode.LoginHandler", "rpg.mode.VillageHandler", 
		"rpg.mode.MazeHandler", "rpg.mode.BigMapHandler", "rpg.mode.BattleHandler", 
		"rpg.mode.MenuHandler", "rpg.mode.CreateCharacterHandler"};
	
	public InterfaceHandler(){
		this(LOGIN_MODE);
	}
	
	public InterfaceHandler(int mode){
		Game.gsh = new GameStatusHandler("Unknown" ,0);
		this.mode = mode;
		runLoop();
	}
	
	private void runLoop(){
		while(mode >= 0){
			try{
				PanelHandler ph = (PanelHandler)Class.forName(handlerList[mode]).newInstance();
				mode = ph.execute();
				
				if(Game.gsh.isReload()){
					Game.gsh.setReload(false);
					if(Game.gsh.getCurMap().getType().equals("Village"))
						mode = VILLAGE_MODE;
					else if(Game.gsh.getCurMap().getType().equals("Maze"))
						mode = MAZE_MODE;
					else if(Game.gsh.getCurMap().getType().equals("BigMap"))
						mode = BIGMAP_MODE;
				}
			}catch(Exception e){
				System.err.println(e);
				mode = -1;
			}
		}
	}
	
}