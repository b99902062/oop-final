package rpg.mode;

import java.lang.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.*;
import java.net.*;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.*;
import javax.imageio.*;

import rpg.draw.*;
import rpg.game.*;
import rpg.resource.*;
import rpg.map.*;
import rpg.audio.*;
import rpg.status.*;
import rpg.control.*;
import rpg.character.*;
import rpg.item.*;

/**
 * A class that handle the battle Handler
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class BattleHandler extends PanelHandler implements KeyListener{
	private static final long serialVersionUID = 1L;
	
	private AudioPlayer bgPlayer;
	private Thread musicThread;
	private Animation[] monsterAnimation;
	private JLabel[] monsterLabel;
	private boolean[] monsterDead;
	private boolean[] roleDead;
	private BufferedImage bgImg;
	private int monsterNum;
	private Monster[] nowMonster;
	private LinkedList<Role> nowRole;
	private LinkedList<Item> Items;
	private JLabel[] charLabel;
	private JLabel[] charHp,rectangleHp;
	private JLabel[] charMp,rectangleMp;
	private JLabel[] charPortrait;
	private int userMode;
	private String[] wordList = { "Attack", "Skill", "Defense","Backpack","Run" };
	private JLabel[] labelList;
	private JLabel[] skillLabel;
	private JLabel[] itemTypeLabel = new JLabel[2];
	private JLabel[] itemLabel;
	private int wordAmount = 5;
	private int nowCommand,target,currentRole,nowSkill,barSkill,barSize,nowType,nowItem,barItem;
	private int[] attackTarget;
	private int[] useSkill;
	private boolean[] useWhich;
	private int[] itemChoose;
	private JLabel skillDescrip,skillTitle,skillcost,skillAmount;
	private JLabel arrow;
	private ArrayList<Skill> roleSkill;
	private JPanel skillPanel = new JPanel();
	private JPanel backpackPanel = new JPanel();
	private static int overNum;
	private Thread preThread;
	private boolean battleOver;
	private BufferedImage attackArrow = ResourceHandler.getImageByName("other", "arrowImage");
	private BufferedImage healArrow = ResourceHandler.getImageByName("other", "arrow2Image");
	private Random random = new Random();
	private int allobject;
	private boolean win_lose = true;
	
	private boolean isOut;
	
	public int nextMode = -1;
	
	public static final double RISING_RATE = 0.988;
	
	public synchronized int execute(){
	
		if(Game.gsh.getBoss())
		{
			bgPlayer = new AudioPlayer(true, ResourceHandler.getResourcePath("music", "Boss"));
			bgImg = ResourceHandler.getImageByName("background", "battleBG02");
		}
		else
		{
			bgPlayer = new AudioPlayer(true, ResourceHandler.getResourcePath("music", "enemy"));
			bgImg = ResourceHandler.getImageByName("background", "battleBG01");
		}
		musicThread = new Thread(bgPlayer);
		musicThread.start();
		preThread = new Thread();
		warpToBattle();
		drawBackGround();
		if(Game.gsh.getBoss())
			setBoss();
		else
			setMonster();
		drawMenu();
		drawRole();
		drawArrow();
		battleOver = false;
		Game.gui.addKeyListener(this);
		try{
			wait();
			Game.gui.removeKeyListener(this);
			preThread.join();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		if(battleOver)
		{
			if(win_lose)
				drawWin();
			else
				drawLose();
			panel.repaint();
			try{
				Thread.sleep(5000);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		bgPlayer.stop();
		for(int i=0; i<monsterAnimation.length; i++)
			monsterAnimation[i].stop();
		Game.gsh.setBattle(false);
		Game.gsh.setBoss(false);
		if(win_lose)
			return InterfaceHandler.MAZE_MODE;
		else
			return InterfaceHandler.LOGIN_MODE;
	}
	
	/*Draw Game Over*/
	private void drawLose(){
		JPanel losePanel = new JPanel();
		BufferedImage tmpImg = ResourceHandler.getImageByName("background", "GameOver");
		tmpImg = Drawer.getResizedImg(tmpImg, Game.gui.getWidth(), Game.gui.getHeight());
		losePanel = Drawer.drawBackGround(tmpImg);
		losePanel.setBounds(0, 0, Game.gui.getWidth(), Game.gui.getHeight());
		losePanel.setLayout(null);
		System.out.println("lose");
		panel.add(losePanel,0);
	}
	/*Draw a Win Scene*/
	private void drawWin(){
		JPanel winPanel = new JPanel();
		panel.add(winPanel,0);
		winPanel.setBounds(250, 50, 800, 400);
		winPanel.setLayout(null);
		winPanel.setBackground( new Color(0,0,0,64) );
		winPanel.repaint();
		JLabel[] portrait = new JLabel[nowRole.size()];
		JLabel[] expLabel = new JLabel[nowRole.size()];
		
		JLabel victory = new JLabel("Victory");
		victory.setForeground(Color.RED);
		victory.setBounds(250,0,500,100);
		victory.setFont( new Font("Trebuchet MS", Font.BOLD, 80) );
		winPanel.add(victory,0);
		
		JLabel get = new JLabel("Get");
		get.setForeground(Color.green);
		get.setBounds(300,300,500,100);
		get.setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
		winPanel.add(get,0);
		
		int addexp = 0;
		int addMoney = 0;
		int itemNum = 0;
		for(int i=0 ;i<nowMonster.length ;i++)
		{
			addexp +=nowMonster[i].getExp();
			addMoney +=nowMonster[i].getMoney();
			if(random.nextInt(2) == 0)
			{
				Item dropItem = nowMonster[i].getItem();
				JLabel dropLabel = new JLabel(dropItem.getImage());
				dropLabel.setBounds(350+itemNum*50,300,50,100);
				winPanel.add(dropLabel,0);
				Game.gsh.addItem(dropItem);
				itemNum++;
			}
		}
		
		for(int i=0 ;i<nowRole.size() ;i++)
		{
			portrait[i] = new JLabel(nowRole.get(i).getPortrait());
			portrait[i].setBounds(0,i*100+100,100,100);
			winPanel.add(portrait[i],0);
			
			int exp = nowRole.get(i).getCurExp();
			exp = exp+addexp;
			nowRole.get(i).setCurExp(exp);
			boolean levelup = nowRole.get(i).checkUpgrade();
			expLabel[i]  =new JLabel("+"+addexp+" exp");
			expLabel[i].setForeground(Color.GREEN);
			expLabel[i].setBounds(150,i*100+100,300,100);
			expLabel[i].setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
			winPanel.add(expLabel[i],0);
			
			if(levelup)
			{
				JLabel levelupLabel = new JLabel("Level Up");
				levelupLabel.setForeground(Color.RED);
				levelupLabel.setBounds(400,i*100+100,300,100);
				levelupLabel.setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
				winPanel.add(levelupLabel,0);
			}
		}
		
		BufferedImage tmpImg = ResourceHandler.getImageByName("item", "money");
		JLabel moneyPic = new JLabel(new ImageIcon(tmpImg));
		moneyPic.setBounds(0,300,100,100);
		winPanel.add(moneyPic,0);
		
		int money = Game.gsh.getMoney();
		money = money+addMoney;
		Game.gsh.setMoney(money);
		JLabel moneyLabel = new JLabel("$ "+addMoney);
		moneyLabel.setForeground(Color.YELLOW);
		moneyLabel.setBounds(100,300,300,100);
		moneyLabel.setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
		winPanel.add(moneyLabel,0);
	}
	/*Warp Scene from Maze to Battle*/
	private void warpToBattle(){
		JPanel warpPanel = new JPanel();
		Game.gui.getContentPane().add(warpPanel,0);
		warpPanel.setLayout(null);
		warpPanel.setBounds(0, 0, Game.gui.getWidth(), Game.gui.getHeight());
		warpPanel.setOpaque(false);
		int whichWarp = random.nextInt(4)+1;
		BufferedImage[] warpImage = ResourceHandler.getSkillAnimationFrames("warp"+whichWarp);
		JLabel effect = new JLabel("");
		effect.setBounds(0,0,Game.gui.getWidth(), Game.gui.getHeight());
		warpPanel.add(effect,0);
		for(int i=0 ;i<6 ;i++)
		{
			BufferedImage tmpImg = Drawer.getResizedImg(warpImage[i%6],Game.gui.getWidth(), Game.gui.getHeight());
			try{
				effect.setIcon(new ImageIcon(tmpImg));
				warpPanel.repaint();
				Game.gui.getContentPane().repaint();
				Thread.sleep(100);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		Game.gui.getContentPane().remove(warpPanel);
	}
	/*Draw Skill scroll bar*/
	private void drawSkillbar(int topSkill){
		for(int i=0 ;i<barSize ;i++)
		{
			
			skillLabel[i].setText(roleSkill.get(topSkill+i).getName());
		}
	}
	/*Draw Backpack scroll bar*/
	private void drawItembar(int topItem){
		for(int i=0 ;i<barSize ;i++)
		{
			int itemCount = Game.gsh.countItem(Items.get(i));
			itemLabel[i].setText(Items.get(topItem+i).getName()+" x"+itemCount);
		}
	}
	/*Draw Backpack*/
	private void drawBackpack(){
		panel.add(backpackPanel,0);
		backpackPanel.setLayout(null);
		backpackPanel.setBounds((Game.gui.getWidth())/2-600, Game.gui.getHeight()/2+115, 800, 500);
		
		BufferedImage tmpImg = ResourceHandler.getImageByName("background", "menuBG");
		tmpImg = Drawer.getResizedImg(tmpImg,300,270);
		JLabel backpack = new JLabel(new ImageIcon(tmpImg));
		backpack.setBounds(300,0,300,270);
		backpackPanel.add(backpack,0);
		
		itemTypeLabel[0] = new JLabel("Potion");
		itemTypeLabel[0].setFont( new Font("Trebuchet MS", Font.BOLD, 40) );
		itemTypeLabel[0].setForeground(Color.blue);
		itemTypeLabel[0].setBounds(330,0, 300, 100);
		backpackPanel.add(itemTypeLabel[0],0);
		
		itemTypeLabel[1] = new JLabel("Toss");
		itemTypeLabel[1].setFont( new Font("Trebuchet MS", Font.BOLD, 40) );
		itemTypeLabel[1].setForeground(Color.white);
		itemTypeLabel[1].setBounds(480,0, 300, 100);
		backpackPanel.add(itemTypeLabel[1],0);
		drawItem(0);
		
		backpackPanel.setOpaque(false);
		backpackPanel.repaint();
		panel.repaint();
		nowType = 0;
	}
	/*Draw Items in Backpack*/
	private void drawItem(int type){
		if(type == 0)
		{
			Items = Game.gsh.getPotionList();
		}
		else
		{
			Items = Game.gsh.getTossList();
		}
		System.out.println(Items.size());
		if(Items.size() >0)
		{
			if(Items.size() > 4)
				barSize = 4;
			else
				barSize = Items.size();
			itemLabel = new JLabel[barSize];
			for(int i=0 ;i<barSize;i++)
			{
				int itemCount = Game.gsh.countItem(Items.get(i));
				itemLabel[i] = new JLabel(Items.get(i).getName()+" x"+itemCount,JLabel.CENTER);
				itemLabel[i].setFont( new Font("Trebuchet MS", Font.BOLD, 20) );
				if( i==0 ) itemLabel[i].setForeground(Color.yellow);
				else itemLabel[i].setForeground(Color.darkGray);
				itemLabel[i].setBounds(350,i*50+50, 200, 100);
				backpackPanel.add(itemLabel[i],0);
			}
		}
		nowItem = 0;
		barItem = 0;
	}
	/*Draw Skill list*/
	private void drawSkill(){
		panel.add(skillPanel,0);
		skillPanel.setLayout(null);
		skillPanel.setBounds((Game.gui.getWidth())/2-600, Game.gui.getHeight()/2+115, 800, 500);
		
		BufferedImage tmpImg = ResourceHandler.getImageByName("other", "shellImage");
		tmpImg = Drawer.getResizedImg(tmpImg,300,270);
		
		JLabel shell = new JLabel(new ImageIcon(tmpImg));
		shell.setBounds(300,0,300,270);
		skillPanel.add(shell,0);
		
		tmpImg = ResourceHandler.getImageByName("other", "skillDisplay");
		tmpImg = Drawer.getResizedImg(tmpImg,300,270);
		JLabel display = new JLabel(new ImageIcon(tmpImg));
		display.setBounds(0,0,300,270);
		skillPanel.add(display,0);
		
		roleSkill = nowRole.get(currentRole).getSkills();
		if(roleSkill.size() > 4)
			barSize = 4;
		else
			barSize = roleSkill.size();
		skillLabel = new JLabel[barSize];
		for(int i=0 ;i<barSize;i++)
		{
			skillLabel[i] = new JLabel(roleSkill.get(i).getName(),JLabel.CENTER);
			skillLabel[i].setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
			if( i==0 ) skillLabel[i].setForeground(Color.yellow);
			else skillLabel[i].setForeground(Color.darkGray);
			skillLabel[i].setBounds(300,i*50, 300, 100);
			skillPanel.add(skillLabel[i],0);
		}
		skillTitle = new JLabel(roleSkill.get(0).getName(),JLabel.CENTER);
		skillTitle.setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
		skillTitle.setForeground(Color.white);
		skillTitle.setBounds(0,0, 300, 100);
		skillPanel.add(skillTitle,0);
		
		skillDescrip = new JLabel(roleSkill.get(0).getDescription(),JLabel.CENTER);
		skillDescrip.setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
		skillDescrip.setForeground(Color.yellow);
		skillDescrip.setBounds(0,50, 300, 100);
		skillPanel.add(skillDescrip,0);
		
		skillAmount = new JLabel("Amount: "+roleSkill.get(0).getRealAmount(),JLabel.CENTER);
		skillAmount.setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
		skillAmount.setForeground(Color.red);
		skillAmount.setBounds(0,100, 300, 100);
		skillPanel.add(skillAmount,0);
		
		skillcost = new JLabel("Cost: "+roleSkill.get(0).getMpCost()+" MP",JLabel.CENTER);
		skillcost.setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
		skillcost.setForeground(Color.blue);
		skillcost.setBounds(0,150, 300, 100);
		skillPanel.add(skillcost,0);
		
		skillPanel.setOpaque(false);
		skillPanel.repaint();
		panel.repaint();
		nowSkill = 0;
		barSkill = 0;
	}
	/*Draw Skill Description which you select*/
	private void drawSkillDescription(){
		skillTitle.setText(roleSkill.get(nowSkill).getName());
		skillDescrip.setText(roleSkill.get(nowSkill).getDescription());
		skillAmount.setText("Amount: "+roleSkill.get(nowSkill).getRealAmount());
		skillcost.setText("Cost: "+roleSkill.get(nowSkill).getMpCost()+" MP");
	}
	/*Draw a arrow to choose target*/
	private void drawArrow(){
		arrow = new JLabel(new ImageIcon(attackArrow));
		arrow.setBounds(240, 30, 100, 67);
		panel.add(arrow,0);
		arrow.setVisible(false);
		target = 0;
	}
	/*Draw a battle menu*/
	private void drawMenu(){
		BufferedImage tmpImg = ResourceHandler.getImageByName("other", "fightMenuImage");
		tmpImg = Drawer.getResizedImg(tmpImg,Game.gui.getWidth(),400);
		JLabel menu = new JLabel(new ImageIcon(tmpImg));
		menu.setBounds(0, 500,Game.gui.getWidth(),400);
		panel.add( menu,0);
		menu.repaint();
		labelList = new JLabel[wordAmount];
		for( int i=0;i<wordAmount;i++ ){
			labelList[i] = new JLabel( wordList[i],JLabel.CENTER);
			labelList[i].setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
			if( i==0 ) labelList[i].setForeground(Color.blue);
			else labelList[i].setForeground(Color.darkGray);
			labelList[i].setBounds((Game.gui.getWidth())/2-100, Game.gui.getHeight()/2+100 + 50 * i, 300, 100);
			panel.add( labelList[i],0);
			labelList[i].repaint();
		}
		nowCommand = 0;
	}
	/*Draw Role's HP and MP*/
	private void drawHpMp(){
		for(int i=0 ;i<nowRole.size() ;i++)
		{
			charHp[i].setText(nowRole.get(i).getCurHP()+"/"+nowRole.get(i).getHP());
			charMp[i].setText(nowRole.get(i).getCurMP()+"/"+nowRole.get(i).getMP());
			rectangleHp[i].setSize((100*nowRole.get(i).getCurHP()/nowRole.get(i).getHP()),20);
			rectangleMp[i].setSize((100*nowRole.get(i).getCurMP()/nowRole.get(i).getMP()),20);
		}
		panel.repaint();
	}
	/*Draw a dead Role*/
	private void drawRoleDead(int index){
		BufferedImage tmpImg = Drawer.getResizedImg(nowRole.get(index).getIconList()[4].getImage(),Drawer.LARGE_BLOCK_SIZE*2,Drawer.LARGE_BLOCK_SIZE*2);
		int width = tmpImg .getWidth();
        int height = tmpImg .getHeight();
		
		for (int i = 0; i < height; i++) 
		{
			for (int j = 0; j < width; j++) 
			{
				int color = tmpImg.getRGB(j, i);
				Color c = new Color(color);
				int red = c.getRed();
				int green = c.getGreen();
				int blue = c.getBlue();
				
				int new_color =  (int)(red * 0.299 + green * 0.587 + blue + 0.114);
				if(new_color > 255)
					new_color = 255;
				if(color !=0)
				{
					c = new Color(new_color,new_color,new_color);
					tmpImg.setRGB(j, i,c.getRGB());
				}
			}
		}
		charLabel[index].setIcon(new ImageIcon(tmpImg));
		charLabel[index].repaint();
	}
	/*Draw role's every imformation*/
	private void drawRole(){
		BufferedImage hpImg = ResourceHandler.getImageByName("other", "hpImage");
		BufferedImage mpImg = ResourceHandler.getImageByName("other", "mpImage");
		currentRole = 0;
		userMode = 0;
		nowRole = Game.gsh.getRoleList();
		roleDead = new boolean[nowRole.size()];
		itemChoose = new int[nowRole.size()];
		useWhich = new boolean[nowRole.size()];
		attackTarget = new int[nowRole.size()];
		useSkill = new int[nowRole.size()];
		charLabel = new JLabel[nowRole.size()];
		charHp = new JLabel[nowRole.size()];
		charMp = new JLabel[nowRole.size()];
		charPortrait = new JLabel[nowRole.size()];
		rectangleHp = new JLabel[nowRole.size()];
		rectangleMp = new JLabel[nowRole.size()];
		overNum = nowRole.size() + nowMonster.length;
		allobject = overNum;
		for(int i=0 ;i<nowRole.size() ;i++)
		{
			roleDead[i] = false;
			BufferedImage tmpImg = Drawer.getResizedImg(nowRole.get(i).getIconList()[4].getImage(),Drawer.LARGE_BLOCK_SIZE*2,Drawer.LARGE_BLOCK_SIZE*2);
			charLabel[i] = new JLabel(new ImageIcon(tmpImg));
			charLabel[i].setBounds(Game.gui.getWidth()/2+200, Game.gui.getHeight()/2-200+i*200, Drawer.LARGE_BLOCK_SIZE*2, Drawer.LARGE_BLOCK_SIZE*2);
			panel.add(charLabel[i], 0);
			
			charHp[i] = new JLabel(nowRole.get(i).getCurHP()+"/"+nowRole.get(i).getHP());
			charHp[i].setFont( new Font("Trebuchet MS", Font.BOLD, 20) );
			charHp[i].setForeground(Color.red);
			charHp[i].setBounds((Game.gui.getWidth())/2+250+(i%2)*200, Game.gui.getHeight()/2+130 + 200 * (i/2),200,100);
			panel.add(charHp[i], 0);
			
			rectangleHp[i]  = new JLabel(new ImageIcon(hpImg));
			rectangleHp[i].setBounds((Game.gui.getWidth())/2+250+(i%2)*200, Game.gui.getHeight()/2+150 + 200 * (i/2),(100*nowRole.get(i).getCurHP()/nowRole.get(i).getHP()),20);
			panel.add(rectangleHp[i], 0);
			
			charMp[i] = new JLabel(nowRole.get(i).getCurMP()+"/"+nowRole.get(i).getMP());
			charMp[i].setFont( new Font("Trebuchet MS", Font.BOLD, 20) );
			charMp[i].setForeground(Color.blue);
			charMp[i].setBounds((Game.gui.getWidth())/2+250+(i%2)*200, Game.gui.getHeight()/2+180 + 200 * (i/2),200,100);
			panel.add(charMp[i], 0);
			
			rectangleMp[i]  = new JLabel(new ImageIcon(mpImg));
			rectangleMp[i].setBounds((Game.gui.getWidth())/2+250+(i%2)*200, Game.gui.getHeight()/2+200 + 200 * (i/2),(100*nowRole.get(i).getCurMP()/nowRole.get(i).getMP()),20);
			panel.add(rectangleMp[i], 0);
			
			charPortrait[i] = new JLabel(nowRole.get(i).getPortrait());
			charPortrait[i].setBounds((Game.gui.getWidth())/2+150+(i%2)*200, Game.gui.getHeight()/2+150 + 200 * (i/2),100,100);
			panel.add(charPortrait[i], 0);
		}
		panel.repaint();
	}
	/*Draw a background of battle field*/
	private void drawBackGround(){
		try{
			bgImg = Drawer.getResizedImg(bgImg, Game.gui.getWidth(), Game.gui.getHeight());
			panel = Drawer.drawBackGround(bgImg);
			Game.gui.setContentPane(panel);
			panel.setLayout(null);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	/*Set monsters in this battle field*/
	private void setMonster(){
		try{
			Map map = Game.gsh.getCurMap();
			monsterNum = (int)(Math.random() * 5) + 2;
			System.out.println("Num: " + monsterNum);
			monsterDead = new boolean[monsterNum];
			monsterLabel = new JLabel[monsterNum];
			monsterAnimation = new Animation[monsterNum];
			nowMonster = new Monster[monsterNum];
			for(int i=0; i<monsterNum; i++){
				monsterDead[i] = false;
				double tmpRand = Math.random();
				double curTotal = 0;
				for(int j=0; j<map.getMonsters().length; j++){
					curTotal += map.getRateList()[j];
					if(curTotal >= tmpRand){
						createMonster(i, map.getMonsters()[j]);
						break;
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		System.out.println("CUR THREAD: " + Thread.activeCount());
	}
	/*Creat Monser*/
	private void createMonster(int index, Monster monster){
		nowMonster[index] = new Monster(monster);
		monsterLabel[index] = new JLabel(new ImageIcon(Drawer.getResizedImg(monster.getImage().getImage(), 80, 80)));
		if(index < 3)
			monsterLabel[index].setBounds(210 - 80 * index, 30 + 80 * index, 80, 80);
		else
			monsterLabel[index].setBounds(350 - 80 * (index-3), 30 + 80 * (index-3), 80, 80);
		panel.add(monsterLabel[index], 0);
		monsterLabel[index].repaint();
		
		double scaleRate = 2;
		monsterAnimation[index] = new Animation("monster", monster.getNormalAnimationName(), monsterLabel[index], scaleRate);
		Thread thread = new Thread(monsterAnimation[index]);
		thread.start();
		System.out.println(monster.getName());
	}
	
	private void setBoss(){
		nowMonster = new Monster[1];
		nowMonster[0] = ResourceHandler.getMonster(Game.gsh.getBossName());
		monsterLabel = new JLabel[1];
		monsterLabel[0] = new JLabel(new ImageIcon(Drawer.getResizedImg(nowMonster[0].getImage().getImage(), 480, 480)));
		monsterLabel[0].setBounds(0, 0, 480, 480);
		panel.add(monsterLabel[0],0);
		
		monsterDead = new boolean[1];
		monsterDead[0] = false;
		monsterNum = 1;
		
		monsterAnimation = new Animation[1];
		monsterAnimation[0] = new Animation("monster", nowMonster[0].getNormalAnimationName(), monsterLabel[0], 1);
		Thread thread = new Thread(monsterAnimation[0]);
		thread.start();
	}
	
	/*Fight animation and action*/
	private boolean fightMode(){
		int win = 0;
		boolean start = true;
		int Atk;
		JLabel damage;
		
		overNum = 0;
		allobject = nowRole.size()+nowMonster.length;
		preThread = new Thread();
		for(int i=0 ;i<nowRole.size() ;i++)
		{
			if(roleDead[i])
			{
				attackOver();
				continue;
			}
			if(useWhich[i])
			{
				roleSkill = nowRole.get(i).getSkills();
				if(useSkill[i] == -1)
				{
					Atk = nowRole.get(i).getAtk();
				}
				else
				{
					int mp = nowRole.get(i).getCurMP();
					if(roleSkill.get(useSkill[i]).getMpCost() > mp)
					{
						attackOver();
						continue;
					}
					nowRole.get(i).setCurMP(mp - roleSkill.get(useSkill[i]).getMpCost());
					if(roleSkill.get(useSkill[i]).isHit())
					{
						Atk = roleSkill.get(useSkill[i]).getAmount();
					}
					else
						Atk = 0;
				}
				
				boolean single;
				if(useSkill[i] == -1)
					single = true;
				else
					single = roleSkill.get(useSkill[i]).getIsSingle();
				
				if(single)
				{
					if(Atk !=0)
					{
						damage = new JLabel(""+Atk);
						damage.setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
						damage.setForeground(Color.red);
					}
					else
					{
						damage = new JLabel("miss");
						damage.setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
						damage.setForeground(Color.yellow);
					}
					
					if(useSkill[i] == -1 || roleSkill.get(useSkill[i]).getType().equals("ATTACK"))
					{	
						int damageX,damageY;
						
						if(monsterDead[attackTarget[i]])
						{
							int newTarget = 0;
							while(monsterDead[newTarget])
							{
								newTarget++;
								if(newTarget == monsterNum)
									break;
							}
							attackTarget[i] = newTarget;
						}
						
						if(attackTarget[i] != monsterNum)
						{
							int hp = nowMonster[attackTarget[i]].getHp();
							nowMonster[attackTarget[i]].setHp(hp-Atk);
							if(attackTarget[i] < 3)
							{
								damageX = 210 - 80 * attackTarget[i]+50;
								damageY = 80 * attackTarget[i]+50;
							}
							else
							{
								damageX = 350 - 80 * (attackTarget[i]-3)+50;
								damageY = 30 + 80 * (attackTarget[i]-3)+20;
							}
							damage.setBounds(damageX,damageY,100,100);
							
							if(nowMonster[attackTarget[i]].getHp() <= 0)
								monsterDead[attackTarget[i]] = true;
							
							String skillname,soundname;
							if(useSkill[i] == -1)
							{
								skillname = "attack";
								soundname = "slash";
							}
							else
							{
								skillname = roleSkill.get(useSkill[i]).getName();
								soundname = roleSkill.get(useSkill[i]).getSound();
							}
							
							attackAnimation damageAnimation = new attackAnimation(damage,panel,nowRole.get(i),charLabel[i],monsterLabel[attackTarget[i]],monsterDead[attackTarget[i]],preThread,skillname,true,0,attackTarget[i],soundname);
							Thread damageThread = new Thread(damageAnimation);
							damageThread.start();
							preThread = damageThread;
							drawHpMp();
						}
					}
					else
					{
						String skillname = roleSkill.get(useSkill[i]).getName();
						String soundname = roleSkill.get(useSkill[i]).getSound();
						damage.setForeground(Color.GREEN);
						damage.setBounds(Game.gui.getWidth()/2+300, Game.gui.getHeight()/2-200+attackTarget[i]*200-50,100,100);
						int hp = nowRole.get(attackTarget[i]).getCurHP();
						nowRole.get(attackTarget[i]).setCurHP(hp + Atk);
						attackAnimation damageAnimation = new attackAnimation(damage,panel,nowRole.get(i),charLabel[i],preThread,skillname,true,soundname);
						Thread damageThread = new Thread(damageAnimation);
						damageThread.start();
						preThread = damageThread;
					}
					drawHpMp();
				}
				else
				{
					boolean[] beforeAtk = new boolean[nowMonster.length];
					boolean[] afterAtk = new boolean[nowMonster.length];
					JLabel[] damages = new JLabel[nowMonster.length];
					String skillname = roleSkill.get(useSkill[i]).getName();
					String soundname = roleSkill.get(useSkill[i]).getSound();
					for(int j=0 ;j<nowMonster.length ;j++)
					{
						beforeAtk[j] = monsterDead[j];
						if(roleSkill.get(useSkill[i]).isHit())
						{
							Atk = roleSkill.get(useSkill[i]).getAmount();
							damages[j] = new JLabel(""+Atk);
							damages[j].setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
							damages[j].setForeground(Color.red);
							if(monsterDead[j])
								damages[j].setVisible(false);
							else
							{
								int hp = nowMonster[j].getHp();
								nowMonster[j].setHp(hp-Atk);
								if(nowMonster[j].getHp() <=0)
									monsterDead[j] = true;
							}
						}
						else
						{
							damages[j] = new JLabel("miss");
							damages[j].setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
							damages[j].setForeground(Color.yellow);
							if(monsterDead[j])
								damages[j].setVisible(false);
						}
						afterAtk[j] = monsterDead[j];
						if(j < 3)
							damages[j].setBounds(210 - 80 * j+50,80 * j+50,100,100);
						else
							damages[j].setBounds(350 - 80 * (j-3)+50,30 + 80 * (j-3)+20,100,100);
					}
					attackAnimation damageAnimation = new attackAnimation(damages,panel,nowRole.get(i),charLabel[i],monsterLabel[0],beforeAtk,afterAtk,preThread,skillname,true,0,soundname);
					Thread damageThread = new Thread(damageAnimation);
					damageThread.start();
					preThread = damageThread;
				}
			}
			else
			{
				if(itemChoose[i] == 0)
				{
					Items = Game.gsh.getPotionList();
					Potion potion = (Potion)Items.get(useSkill[i]);
					potion.doEffect(nowRole.get(attackTarget[i]));
					attackOver();
				}			
			}
		}
		
		for(int i=0 ;i<monsterNum ;i++)
		{
			if(monsterDead[i])
			{
				win++;
				allobject--;
			}
			else
			{
				Skill[] monsterSkill = nowMonster[i].getSkillList();
				if(nowMonster[i].getStrategy() == 1)
				{
					int monsterUse = random.nextInt(monsterSkill.length);
					int monsterTarget = random.nextInt(nowRole.size());
					Atk = monsterSkill[monsterUse].getAmount();
					if(monsterSkill[monsterUse].getIsSingle())
					{
						if(roleDead[monsterTarget])
						{
							attackOver();
							continue;
						}
						damage = new JLabel(""+Atk);
						damage.setForeground(Color.RED);
						damage.setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
						damage.setBounds(Game.gui.getWidth()/2+300, Game.gui.getHeight()/2-200+monsterTarget*200-50,100,100);
						if(Atk >= nowRole.get(monsterTarget).getCurHP())
							roleDead[monsterTarget] = true;
						attackAnimation damageAnimation = new attackAnimation(damage,panel,nowRole.get(monsterTarget),charLabel[monsterTarget],monsterLabel[i],false,preThread,monsterSkill[monsterUse].getName(),false,Atk,monsterTarget,monsterSkill[monsterUse].getSound());
						Thread damageThread = new Thread(damageAnimation);
						damageThread.start();
						preThread = damageThread;
					}
					else
					{
						JLabel[] damages = new JLabel[nowRole.size()];
						boolean[] beforeAtk = new boolean[nowRole.size()]; 
						boolean[] afterAtk = new boolean[nowRole.size()]; 
						for(int j=0 ;j<nowRole.size() ;j++)
						{
							beforeAtk[j] = roleDead[j];
							damages[j] = new JLabel(""+Atk);
							damages[j].setFont( new Font("Trebuchet MS", Font.BOLD, 30) );
							damages[j].setForeground(Color.red);
							damages[j].setBounds(Game.gui.getWidth()/2+300, Game.gui.getHeight()/2-200+j*200-50,100,100);
							if(Atk >= nowRole.get(j).getCurHP())
								roleDead[j] = true;
							afterAtk[j] = roleDead[j];
						}
						attackAnimation damageAnimation = new attackAnimation(damages,panel,nowRole.get(monsterTarget),charLabel[monsterTarget],monsterLabel[i],beforeAtk,afterAtk,preThread,monsterSkill[monsterUse].getName(),false,Atk,monsterSkill[monsterUse].getSound());
						Thread damageThread = new Thread(damageAnimation);
						damageThread.start();
						preThread = damageThread;
						
					}
				}
			}
			if(start == true && monsterDead[i] == false)
			{
				target = i;
				start = false;
			}
		}
		int lose = 0;
		for(int i=0 ;i<nowRole.size();i++)
		{
			if(roleDead[i])
				lose++;
		}
		if(lose == nowRole.size())
			win_lose = false;
			
		nowSkill = 0;
		barSkill = 0;
		if(win == monsterNum || lose == nowRole.size())
			return true;
		else
			return false;
	}
	/*attack animation or not */
	public static void attackOver(){
		overNum++;
		System.out.println("overNum: "+overNum);
	}
	
	public void keyTyped(KeyEvent e){
	}
	/*Control*/
	public synchronized void keyPressed(KeyEvent e){
		int keyCode = e.getKeyCode();
		if(overNum == allobject && battleOver == false)
		{
			if(keyCode == Game.UP){
				if(userMode == 0)
				{
					if(nowCommand != 0){
						labelList[nowCommand].setForeground(Color.darkGray);
						nowCommand--;
						labelList[nowCommand].setForeground(Color.blue);
					}
				}
				else if(userMode == 1)
				{
					if(target !=0)
					{
						int newTarget = target-1;
						while(monsterDead[newTarget])
						{
							newTarget--;
							if(newTarget == -1)
								newTarget = target;
						}
						target = newTarget;
						if(target < 3)
							arrow.setLocation(210 - 80 * target+50,80 * target+30+20);
						else
							arrow.setLocation(350 - 80 * (target-3)+50, 30 + 80 * (target-3)+20);
					}
				}
				else if(userMode == 2)
				{
					if(target !=0)
					{
						target--;
						arrow.setLocation(Game.gui.getWidth()/2+300, Game.gui.getHeight()/2-200+target*200);
					}
				}
				else if(userMode == 3)
				{
					if(nowSkill != 0){
						skillLabel[barSkill].setForeground(Color.darkGray);
						nowSkill--;
						if(barSkill == 0)
						{
							drawSkillbar(nowSkill);
						}
						else
							barSkill--;
						skillLabel[barSkill].setForeground(Color.yellow);
					}
					drawSkillDescription();
				}
				else if(userMode == 4)
				{
					if(nowItem != 0){
						itemLabel[barItem].setForeground(Color.darkGray);
						nowItem--;
						if(barItem == 3)
						{
							drawItembar(nowItem);
						}
						else
							barItem--;
						itemLabel[barItem].setForeground(Color.yellow);
					}
				}
			}
			else if(keyCode == Game.DOWN){
				if(userMode == 0)
				{
					if(nowCommand != (wordAmount-1)){
						labelList[nowCommand].setForeground(Color.darkGray);
						nowCommand++;
						labelList[nowCommand].setForeground(Color.blue);
					}
				}
				else if(userMode == 1)
				{
					if(target !=(monsterNum-1))
					{
						int newTarget = target+1;
						while(monsterDead[newTarget])
						{
							newTarget++;
							if(newTarget == monsterNum)
								newTarget = target;
						}
						target = newTarget;
						if(target < 3)
							arrow.setLocation(210 - 80 * target+50,80 * target+30+20);
						else
							arrow.setLocation(350 - 80 * (target-3)+50, 30 + 80 * (target-3)+20);
					}
				}
				else if(userMode == 2)
				{
					if(target !=(monsterNum-1))
					{
						target++;
						arrow.setLocation(Game.gui.getWidth()/2+300, Game.gui.getHeight()/2-200+target*200);
					}
				}
				else if(userMode == 3)
				{
					if(nowSkill != roleSkill.size()-1){
						skillLabel[barSkill].setForeground(Color.darkGray);
						nowSkill++;
						if(barSkill == 3)
						{
							drawSkillbar(nowSkill-3);
						}
						else
							barSkill++;
						skillLabel[barSkill].setForeground(Color.yellow);
					}
					drawSkillDescription();
				}
				else if(userMode == 4)
				{
					if(nowItem != Items.size()-1){
						itemLabel[barItem].setForeground(Color.darkGray);
						nowItem++;
						if(barItem == 3)
						{
							drawItembar(nowItem-3);
						}
						else
							barItem++;
						itemLabel[barItem].setForeground(Color.yellow);
					}
				}
			}
			else if(keyCode == Game.LEFT){
				if(nowType!=0)
				{
					itemTypeLabel[nowType].setForeground(Color.white);
					nowType--;
					itemTypeLabel[nowType].setForeground(Color.blue);
					for(int i=0 ;i<Items.size();i++)
						backpackPanel.remove(itemLabel[i]);
					panel.repaint();
					drawItem(0);
				}
			}
			else if(keyCode == Game.RIGHT){
				if(nowType!=1)
				{
					itemTypeLabel[nowType].setForeground(Color.white);
					nowType++;
					itemTypeLabel[nowType].setForeground(Color.blue);
					for(int i=0 ;i<Items.size();i++)
						backpackPanel.remove(itemLabel[i]);
					panel.repaint();
					drawItem(1);
				}
			}
			else if(keyCode == Game.ENTER){
				if(userMode == 0)
				{
					if(nowCommand == 0)
					{
						userMode = 1;
						useSkill[currentRole] = -1;
						useWhich[currentRole] = true;
						arrow.setVisible(true);
						int newTarget = target;
						while(monsterDead[newTarget])
						{
							newTarget++;
							if(newTarget == monsterNum)
								newTarget = target;
						}
						target = newTarget;
						if(target < 3)
							arrow.setLocation(210 - 80 * target+50,80 * target+30+20);
						else
							arrow.setLocation(350 - 80 * (target-3)+50, 30 + 80 * (target-3)+20);
						arrow.setIcon(new ImageIcon(attackArrow));
					}
					else if(nowCommand == 1)
					{
						drawSkill();
						useWhich[currentRole] = true;
						userMode = 3;
					}
					else if(nowCommand == 2)
					{
						if(currentRole == nowRole.size()-1)
						{
							battleOver = fightMode();
							if(battleOver)
								notify();
							currentRole = 0;
							while(roleDead[currentRole])
							{
								if(currentRole == nowRole.size()-1)
									break;
								currentRole++;
							}
						}
						else
						{
							currentRole++;
							while(roleDead[currentRole])
							{
								if(currentRole == nowRole.size()-1)
									break;
								currentRole++;
							}
						}
					}
					else if(nowCommand == 3)
					{
						drawBackpack();
						useWhich[currentRole] = false;
						userMode = 4;
					}
					else if(nowCommand == 4)
					{
						overNum = nowRole.size();
						notify();
					}
				}
				else if(userMode == 1)
				{
					attackTarget[currentRole] = target;
					target = 0;
					userMode = 0;
					arrow.setVisible(false);
					if(currentRole == nowRole.size()-1)
					{
						battleOver = fightMode();
						if(battleOver)
							notify();
						currentRole = 0;
						while(roleDead[currentRole])
						{
							if(currentRole == nowRole.size()-1)
								break;
							currentRole++;
						}
					}
					else
					{
						currentRole++;
						while(roleDead[currentRole])
						{
							if(currentRole == nowRole.size()-1)
								break;
							currentRole++;
						}
					}
				}
				else if(userMode == 2)
				{
					attackTarget[currentRole] = target;
					target = 0;
					userMode = 0;
					arrow.setVisible(false);
					if(currentRole == nowRole.size()-1)
					{
						battleOver = fightMode();
						if(battleOver)
							notify();
						currentRole = 0;
						while(roleDead[currentRole])
						{
							if(currentRole == nowRole.size()-1)
								break;
							currentRole++;
						}
					}
					else
					{
						currentRole++;
						while(roleDead[currentRole])
						{
							if(currentRole == nowRole.size()-1)
								break;
							currentRole++;
						}
					}
				}
				else if(userMode == 3)
				{
					skillPanel.removeAll();
					useSkill[currentRole] = nowSkill;
					itemChoose[currentRole] = nowType;
					panel.repaint();
					
					if(roleSkill.get(nowSkill).getIsSingle())
					{
						if(roleSkill.get(nowSkill).getType().equals("ATTACK"))
						{
							userMode = 1;
							arrow.setVisible(true);
							int newTarget = target;
							while(monsterDead[newTarget])
							{
								newTarget++;
								if(newTarget == monsterNum)
									newTarget = target;
							}
							target = newTarget;
							if(target < 3)
								arrow.setLocation(210 - 80 * target+50,80 * target+30+20);
							else
								arrow.setLocation(350 - 80 * (target-3)+50, 30 + 80 * (target-3)+20);
							arrow.setIcon(new ImageIcon(attackArrow));
						}
						else
						{
							userMode = 2;
							arrow.setVisible(true);
							arrow.setLocation(Game.gui.getWidth()/2+300, Game.gui.getHeight()/2-200);
							target = 0;
							arrow.setIcon(new ImageIcon(healArrow));
						}
					}
					else
					{
						target = 0;
						userMode = 0;
						arrow.setVisible(false);
						if(currentRole == nowRole.size()-1)
						{
							battleOver = fightMode();
							if(battleOver)
								notify();
							currentRole = 0;
							while(roleDead[currentRole])
							{
								if(currentRole == nowRole.size()-1)
									break;
								currentRole++;
							}
						}
						else
						{
							currentRole++;
							while(roleDead[currentRole])
							{
								if(currentRole == nowRole.size()-1)
									break;
								currentRole++;
							}
						}
					}
				}
				else if(userMode == 4)
				{
					backpackPanel.removeAll();
					useSkill[currentRole] = nowItem;
					Game.gsh.removeItem(Items.get(nowItem));
					panel.repaint();
					if(nowType == 0)
					{
						userMode = 2;
						arrow.setVisible(true);
						arrow.setLocation(Game.gui.getWidth()/2+300, Game.gui.getHeight()/2-200);
						target = 0;
						arrow.setIcon(new ImageIcon(healArrow));
					}
					else
					{
						userMode = 1;
						arrow.setVisible(true);
						int newTarget = target;
						while(monsterDead[newTarget])
						{
							newTarget++;
							if(newTarget == monsterNum)
								newTarget = target;
						}
						target = newTarget;
						if(target < 3)
							arrow.setLocation(210 - 80 * target+50,80 * target+30+20);
						else
							arrow.setLocation(350 - 80 * (target-3)+50, 30 + 80 * (target-3)+20);
						arrow.setIcon(new ImageIcon(attackArrow));
					}
				}
			}
			else if(keyCode == Game.SPACE){
				
			}
			else if(keyCode == Game.ESC){
				if(userMode == 1)
				{
					userMode = 0;
					arrow.setVisible(false);
				}
				else if(userMode == 2)
				{
					userMode = 0;
					arrow.setVisible(false);
				}
				else if(userMode == 3)
				{
					skillPanel.removeAll();
					userMode = 0;
					panel.repaint();
				}
				else if(userMode == 4)
				{
					backpackPanel.removeAll();
					userMode = 0;
					panel.repaint();
				}
			}
			
		}
	}
	public void keyReleased(KeyEvent e){
	}



class attackAnimation implements Runnable{
	private JLabel damage;
	private JPanel panel;
	private int x,y,charx,chary,monx,mony;
	private JLabel charLabels;
	private Role role;
	private JLabel monster;
	private boolean dead = false;
	private Thread preThread;
	private int mode;
	private skillAnimation skill;
	private JLabel skillLabel;
	private boolean who;
	private int Atk;
	private int scale;
	private JLabel[] damages;
	private boolean single = true;
	private boolean[] before;
	private boolean[] after;
	private int index;
	private String sound;
		
	public attackAnimation(JLabel _damage,JPanel _panel,Role _role,JLabel _charLabel,JLabel _monster,boolean _dead,Thread _preThread,String _skillname,boolean _who,int _Atk,int _index,String _sound){
		preThread = _preThread;
		damage = _damage;
		panel = _panel;
		charLabels =_charLabel;
		role = _role;
		monster = _monster;
		dead = _dead;
		x = damage.getLocation().x;
		y = damage.getLocation().y;
		monx = _monster.getLocation().x;
		mony = _monster.getLocation().y;
		charx = charLabels.getLocation().x;
		chary = charLabels.getLocation().y;
		mode  = 0;
		who = _who;
		Atk = _Atk;
		index = _index;
		sound = _sound;
		
		if(who)
		{
			skill = new skillAnimation(_skillname,x-100,y-50,x-100,y-50);
			skillLabel = new JLabel(new ImageIcon(skill.getImg(0)));
			skillLabel.setVisible(false);
			skill.addLabel(skillLabel);
			int tmpx = skill.getImg(0).getWidth();
			int tmpy = skill.getImg(0).getHeight();
			skillLabel.setBounds(charx-100,chary-50,tmpx,tmpy);
			panel.add(skillLabel,0);
		}
		else
		{
			if(Game.gsh.getBoss())
				skill = new skillAnimation(_skillname,400,0,400,0);
			else
				skill = new skillAnimation(_skillname,charx-100,chary-50,charx-100,chary-50);
			skillLabel = new JLabel(new ImageIcon(skill.getImg(0)));
			skillLabel.setVisible(false);
			skill.addLabel(skillLabel);
			int tmpx = skill.getImg(0).getWidth();
			int tmpy = skill.getImg(0).getHeight();
			if(Game.gsh.getBoss())
				skillLabel.setBounds(400,0,tmpx,tmpy);
			else
				skillLabel.setBounds(charx-100,chary-50,tmpx,tmpy);
			panel.add(skillLabel,0);
		}
	}
		
	public attackAnimation(JLabel _damage,JPanel _panel,Role _role,JLabel _charLabel,Thread _preThread,String _skillname,boolean _who,String _sound){
		preThread = _preThread;
		damage = _damage;
		panel = _panel;
		charLabels =_charLabel;
		role = _role;
		x = damage.getLocation().x;
		y = damage.getLocation().y;
		charx = charLabels.getLocation().x;
		chary = charLabels.getLocation().y;
		mode  = 1;
		who = _who;
		sound = _sound;
		
		if(who)
		{
			skill = new skillAnimation(_skillname,x-100,y-50,x-100,y-50);
			skillLabel = new JLabel(new ImageIcon(skill.getImg(0)));
			skillLabel.setVisible(false);
			skill.addLabel(skillLabel);
			int tmpx = skill.getImg(0).getWidth();
			int tmpy = skill.getImg(0).getHeight();
			skillLabel.setBounds(charx-100,chary-50,tmpx,tmpy);
			panel.add(skillLabel,0);
		}
		else
		{
			skill = new skillAnimation(_skillname,charx-100,chary-50,charx-100,chary-50);
			skillLabel = new JLabel(new ImageIcon(skill.getImg(0)));
			skillLabel.setVisible(false);
			skill.addLabel(skillLabel);
			int tmpx = skill.getImg(0).getWidth();
			int tmpy = skill.getImg(0).getHeight();
			skillLabel.setBounds(charx-100,chary-50,tmpx,tmpy);
			panel.add(skillLabel,0);
		}
	}
	
	public attackAnimation(JLabel[] _damages,JPanel _panel,Role _role,JLabel _charLabel,JLabel _monster,boolean[] _before,boolean[] _after,Thread _preThread,String _skillname,boolean _who,int _Atk,String _sound){
		single = false;
		preThread = _preThread;
		damages = _damages;
		panel = _panel;
		charLabels =_charLabel;
		charx = charLabels.getLocation().x;
		chary = charLabels.getLocation().y;
		role = _role;
		monster = _monster;
		before = _before;
		after = _after;
		mode  = 0;
		who = _who;
		Atk = _Atk;
		sound = _sound;
		
		if(who)
		{
			skill = new skillAnimation(_skillname,300,50,300,50);
			skillLabel = new JLabel(new ImageIcon(skill.getImg(0)));
			skillLabel.setVisible(false);
			skill.addLabel(skillLabel);
			int tmpx = skill.getImg(0).getWidth();
			int tmpy = skill.getImg(0).getHeight();
			System.out.println(""+tmpx+" "+tmpy);
			skillLabel.setBounds(300,50,tmpx,tmpy);
			panel.add(skillLabel,0);
		}
		else
		{
			skill = new skillAnimation(_skillname,300,50,300,50);
			skillLabel = new JLabel(new ImageIcon(skill.getImg(0)));
			skillLabel.setVisible(false);
			skill.addLabel(skillLabel);
			int tmpx = skill.getImg(0).getWidth();
			int tmpy = skill.getImg(0).getHeight();
			skillLabel.setBounds(300,50,tmpx,tmpy);
			panel.add(skillLabel,0);
		}
	}

	public void run(){
		try{
			preThread.join();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		AudioPlayer switchPlayer = new AudioPlayer(false, ResourceHandler.getResourcePath("sound", sound));
		Thread soundThread = new Thread(switchPlayer);
		
		if(single)
		{
			if(who)
			{
				for(int i=0 ;i<7 ;i++)
					attack(i,0);
				panel.add(damage,0);
				
				Thread skillThread = new Thread(skill);
				skillLabel.setVisible(true);
				skillThread.start();
				soundThread.start();
				int frameNum = skill.getFrameNum();
				for(int i=0 ;i<frameNum ;i++)
				{
					up();
					if(mode == 0)
					{
						if(i%2 == 0)
							monsterDamage(false);
						else
							monsterDamage(true);
					}
					else
						heal();
				}
				if(mode == 0)
					monsterDamage(true);
				if(dead)
					monster.setVisible(false);
				panel.remove(damage);
				panel.repaint();
				for(int i=0 ;i<7 ;i++)
					attack(i,1);
				BattleHandler.attackOver();
				panel.remove(skillLabel);
			}
			else
			{
				for(int i=0 ;i<7 ;i++)
					attackMonster(i,0);
				panel.add(damage,0);
				
				Thread skillThread = new Thread(skill);
				skillLabel.setVisible(true);
				skillThread.start();
				soundThread.start();
				int frameNum = skill.getFrameNum();
				for(int i=0 ;i<frameNum ;i++)
				{
					up();
					if(mode == 0)
					{
						if(i%2 == 0)
							charDamage(false);
						else
							charDamage(true);
					}
					else
						heal();
				}
				charDamage(true);
				if(dead)
					charLabels.setVisible(false);
				panel.remove(damage);
				panel.repaint();
				for(int i=0 ;i<7 ;i++)
					attackMonster(i,1);
				BattleHandler.attackOver();
				panel.remove(skillLabel);
				int hp = role.getCurHP();
				role.setCurHP(hp-Atk);
				if(role.getCurHP() <=0)
				{
					drawRoleDead(index);
					roleDead[index] = true;
				}
				drawHpMp();
			}
		}
		else
		{
			if(who)
			{
				for(int i=0 ;i<7 ;i++)
					attack(i,0);
				for(int i=0 ;i<nowMonster.length ;i++)
					panel.add(damages[i],0);
				Thread skillThread = new Thread(skill);
				skillLabel.setVisible(true);
				skillThread.start();
				soundThread.start();
				int frameNum = skill.getFrameNum();
				for(int i=0 ;i<frameNum ;i++)
				{
					allup();
					if(mode == 0)
					{
						if(i%2 == 0)
							allmonsterDamage(false);
						else
							allmonsterDamage(true);
					}
					else
						heal();
				}
				if(mode == 0)
					allmonsterDamage(true);
				for(int i=0 ;i<nowMonster.length ;i++)
				{
					if(after[i] == true)
						monsterLabel[i].setVisible(false);
				}
				for(int i=0 ;i<nowMonster.length ;i++)
					panel.remove(damages[i]);
				panel.repaint();
				for(int i=0 ;i<7 ;i++)
					attack(i,1);
				BattleHandler.attackOver();
				skillLabel.setVisible(false);
				panel.remove(skillLabel);
			}
			else
			{
				for(int i=0 ;i<7 ;i++)
					attackMonster(i,0);
				for(int i=0 ;i<nowRole.size() ;i++)
					panel.add(damages[i],0);
				Thread skillThread = new Thread(skill);
				skillLabel.setVisible(true);
				skillThread.start();
				soundThread.start();
				int frameNum = skill.getFrameNum();
				for(int i=0 ;i<frameNum ;i++)
				{
					allup();
					if(mode == 0)
					{
						if(i%2 == 0)
							allcharDamage(false);
						else
							allcharDamage(true);
					}
					else
						heal();
				}
				if(mode == 0)
					allcharDamage(true);
				for(int i=0 ;i<nowRole.size() ;i++)
					panel.remove(damages[i]);
				panel.repaint();
				for(int i=0 ;i<7 ;i++)
					attackMonster(i,1);
				BattleHandler.attackOver();
				skillLabel.setVisible(false);
				panel.remove(skillLabel);
				for(int i=0 ;i<nowRole.size() ;i++)
				{
					int hp = nowRole.get(i).getCurHP();
					nowRole.get(i).setCurHP(hp-Atk);
					if(nowRole.get(i).getCurHP() <=0)
						drawRoleDead(i);
				}
				drawHpMp();
			}
		}
	}
	private void monsterDamage(boolean vis)
	{
		try{
			Thread.sleep(50);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		monster.setVisible(vis);
	}
	private void allmonsterDamage(boolean vis)
	{
		try{
			Thread.sleep(50);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		for(int i=0 ;i<nowMonster.length ;i++)
		{
			if(before[i] == false)
				monsterLabel[i].setVisible(vis);
		}
	}
	private void allcharDamage(boolean vis)
	{
		try{
			Thread.sleep(50);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		for(int i=0 ;i<nowRole.size() ;i++)
		{
			if(before[i] == false)
				charLabel[i].setVisible(vis);
		}
	}
	private void charDamage(boolean vis)
	{
		try{
			Thread.sleep(50);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		charLabels.setVisible(vis);
	}
	private void heal()
	{
		try{
			Thread.sleep(50);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	private void up(){
		try{
			Thread.sleep(50);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		y = y-2;
		damage.setLocation(x,y);
	}
	private void allup(){
		try{
			Thread.sleep(50);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		if(who)
		{
			for(int i=0 ;i<nowMonster.length ;i++)
			{
				x = damages[i].getLocation().x;
				y = damages[i].getLocation().y;
				y = y-2;
				damages[i].setLocation(x,y);
			}
		}
		else
		{
			for(int i=0 ;i<nowRole.size() ;i++)
			{
				x = damages[i].getLocation().x;
				y = damages[i].getLocation().y;
				y = y-2;
				damages[i].setLocation(x,y);
			}
		}
	}
	
	private void attack(int i,int direct)
	{
		try{
			Thread.sleep(50);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		if(direct == 0)
			charx = charx-5;
		else
			charx = charx+5;
		BufferedImage tmpImg = Drawer.getResizedImg(role.getIconList()[i%3+3].getImage(),Drawer.LARGE_BLOCK_SIZE*2,Drawer.LARGE_BLOCK_SIZE*2);
		charLabels.setIcon(new ImageIcon(tmpImg));
		charLabels.setLocation(charx,chary);
	}
	private void attackMonster(int i,int direct)
	{
		try{
			Thread.sleep(50);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		if(direct == 1)
			monx = monx-5;
		else
			monx = monx+5;
		monster.setLocation(monx,mony);
	}
}


class skillAnimation implements Runnable{
	private BufferedImage[] skillFrame;
	private int frameNum;
	private JLabel targetLabel;
	private int fromX,fromY,toX,toY;
	
	public skillAnimation(String animationName,int _fromX,int _fromY, int _toX, int _toY)
	{
		System.out.println(animationName);
		skillFrame = ResourceHandler.getSkillAnimationFrames(animationName);
		frameNum = skillFrame.length;
		fromX = _fromX;
		fromY = _fromY;
		toX = _toX;
		toY = _toY;
	}
	
	public void run()
	{
		for(int i=0 ; i<frameNum ;i++)
		{
			try{
				targetLabel.setIcon(new ImageIcon(skillFrame[i]));
				Thread.sleep(100);
				targetLabel.setLocation((fromX*(frameNum-i))/frameNum+(toX*i)/frameNum,(fromY*(frameNum-i))/frameNum+(toY*i)/frameNum);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		targetLabel.setVisible(false);
	}
	public BufferedImage getImg(int index)
	{
		return skillFrame[index];
	}
	public void addLabel(JLabel label)
	{
		targetLabel = label;
	}
	public int getFrameNum()
	{
		return frameNum;
	}
}

}