package rpg.game;

import java.lang.*;

import rpg.draw.*;
import rpg.mode.*;
import rpg.status.*;

/**
 * A class that contains the main
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Game{
	public static GUIhandler gui = new GUIhandler();
	
	public static final int UP 		= 38;
	public static final int DOWN 	= 40;
	public static final int LEFT 	= 37;
	public static final int RIGHT 	= 39;
	public static final int ENTER 	= 10;
	public static final int SPACE 	= 32;
	public static final int ESC		= 27;
	public static final int R		= 82;
	public static final int S		= 83;
	
	public static GameStatusHandler gsh;
	
	public static void main(String [] args){
		InterfaceHandler interfaceHandler = new InterfaceHandler();
	}
}