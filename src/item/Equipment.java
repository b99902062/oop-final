package rpg.item;

import java.lang.*;
import javax.swing.*;

/**
 * A class that handles the information of a equipment
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Equipment extends Item{
	
	private static final long serialVersionUID = 1L;
	private int position;
	private int hpValue;
	private int mpValue;
	private int atkValue;
	private int defValue;
	private int spdValue;
	
	public static final int HEAD_POS 		= 0;
	public static final int CHEST_POS 		= 1;
	public static final int RIGHTHAND_POS 	= 2;
	public static final int LEFTHAND_POS 	= 3;
	public static final int FOOT_POS 		= 4;
	
	public Equipment(int _id, int _amount, int _type, int _price, String _name, String _utility, ImageIcon _image){
		super(_id, _amount, _type, _price, _name, _utility, _image);
	}
	
	public void setProperty(int pos, int hp, int mp, int atk, int def, int spd){
		position = pos;
		hpValue = hp;
		mpValue = mp;
		atkValue = atk;
		defValue = def;
		spdValue = spd;
	}
	
	public int getPosition(){
		return position;
	}
	
	public int getHpValue(){
		return hpValue;
	}
	
	public int getMpValue(){
		return mpValue;
	}
	
	public int getAtkValue(){
		return atkValue;
	}
	
	public int getDefValue(){
		return defValue;
	}
	
	public int getSpdValue(){
		return spdValue;
	}
}