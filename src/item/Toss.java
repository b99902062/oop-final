package rpg.item;

import java.lang.*;
import javax.swing.*;

/**
 * A class that handles the information of a tossed item
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Toss extends Item{
	
	private static final long serialVersionUID = 1L;
	private int harmValue;
	private int statusValue;
	private double statusRate;
	
	public Toss(int _id, int _amount, int _type, int _price, String _name, String _utility, ImageIcon _image){
		super(_id, _amount, _type, _price, _name, _utility, _image);
	}
	
	public void setProperty(int harm, int status, double rate){
		harmValue = harm;
		statusValue = status;
		statusRate = rate;
	}
}