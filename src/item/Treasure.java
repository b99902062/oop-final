package rpg.item;

import java.lang.*;
import java.io.*;

/**
 * A class that handles the information of a treasure
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Treasure implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int treasureId;
	private Item[] itemInBox;
	private boolean isOpened;
	
	public Treasure(int id){
		treasureId = id;
		//setItemInBox(itemInBox, id);
	}
	
	public boolean isOpened(){
		return isOpened;
	}
	
	public void setToClosed(){
		isOpened = false;
	}
	
	public void setToOpen(){
		isOpened = true;
	}
	
	public Item[] getItemInBox(){
		return itemInBox;
	}
}