package rpg.item;

import java.lang.*;
import java.util.*;
import javax.swing.*;


/**
 * A class that save a role's initial status
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class SkillInit{
	
	public String description;
	public int mpCost;
	public boolean isSingle;
	public boolean isNear;
	public boolean isAttack;
	public boolean isHeal;
	public int amount;
	public double biasRate;
	public double hitRate;
	public boolean isStatusAttack;
	public boolean isStatusHeal;
	public int status;
	public double statusHitRate;
	public String sound;

	public SkillInit(){
	}
	
}