package rpg.item;

import java.lang.*;
import javax.swing.*;

/**
 * A class that handles the information of key items
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Key extends Item{

	private static final long serialVersionUID = 1L;
	public Key(int _id, int _amount, int _type, int _price, String _name, String _utility, ImageIcon _image){
		super(_id, _amount, _type, _price, _name, _utility, _image);
	}
}