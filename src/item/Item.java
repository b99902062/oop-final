package rpg.item;

import java.lang.*;
import javax.swing.*;
import java.awt.image.*;
import java.io.*;
import java.awt.*;

import rpg.resource.*;

/**
 * A class that handles the information of a item
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Item implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int id;
	private int amount;
	private int type;
	private int price;
	private String name;
	private String utility;
	private ImageIcon image;
	
	public static final int TYPE_EQUIP		= 1;
	public static final int TYPE_POTION		= 2;
	public static final int TYPE_TOSS		= 3;
	public static final int TYPE_OTHER		= 4;
	public static final int TYPE_KEY		= 5;
	
	private static String[] typeNameList = {"", "Equipment", "Potion", "Toss", "Other", "Key"};
	private static String[] typeImageName = {"", "typeEquipmentImage", "typePotionImage", "typeTossImage", "typeOtherImage", "typeKeyImage"};
	
	public Item(int _id, int _amount, int _type, int _price, String _name, String _utility, ImageIcon _image){
		id = _id;
		amount = _amount;
		type = _type;
		price = _price;
		name = _name;
		utility = _utility;
		image = _image;
	}
	
	public ImageIcon getImage(){
		return new ImageIcon(image.getImage());
	}
	
	public String getName(){
		return name;
	}
	
	public int getAmount(){
		return amount;
	}
	
	public void setAmount(int value){
		amount = value;
	}
	
	public int getType(){
		return type;
	}
	
	public int getPrice(){
		return price;
	}
	
	public int getId(){
		return id;
	}
	
	public String getUtility(){
		return new String(utility);
	}
	
	public BufferedImage getTypeImage(){
		return ResourceHandler.getImageByName("other", typeImageName[type]);
	}
	
	public Image getPureImage(){
		return image.getImage();
	}
	
	public String toString(){
		return new String("<" + typeNameList[type] + "> " + name);
	}
	
	//public Action onCharacter( Charactor target )
	//public Action onMoster( Monster target ) 
}