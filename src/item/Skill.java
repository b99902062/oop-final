package rpg.item;

import java.lang.*;
import java.awt.image.*;
import java.io.*;
import java.util.Random;

/**
 * A class that handles a skill
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Skill implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private String description;
	private String sound;
	private int mpCost;
	
	private boolean isSingle;
	private boolean isNear;
	private boolean isAttack;
	private boolean isHeal;
	private int amount;
	private double biasRate;
	private double hitRate;
	
	private boolean isStatusAttack;
	private boolean isStatusHeal;
	private int status;
	private double statusHitRate;
	
	private Random random = new Random();
	
	public Skill(String _name, SkillInit skillInit){
		name = _name;
		this.description = skillInit.description;
		this.mpCost = skillInit.mpCost;
		this.isSingle = skillInit.isSingle;
		this.isNear = skillInit.isNear;
		this.isAttack = skillInit.isAttack;
		this.isHeal = skillInit.isHeal;
		this.amount = skillInit.amount;
		this.biasRate = skillInit.biasRate;
		this.hitRate = skillInit.hitRate;
		this.isStatusAttack = skillInit.isStatusAttack;
		this.isStatusHeal = skillInit.isStatusHeal;
		this.status = skillInit.status;
		this.statusHitRate = skillInit.statusHitRate;
		this.sound = skillInit.sound;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getDescription(){
		return this.description;
	}
	
	public String getSound(){
		return this.sound;
	}
	
	public int getMpCost(){
		return this.mpCost;
	}
	
	public int getAmount(){
		int range = (int)(2*amount*biasRate*100);
        return (int)(amount + (random.nextInt(range)/100 - amount*biasRate));
	}
	
	public int getRealAmount(){
		return amount;
	}
	
	public boolean isHit(){
		if(random.nextDouble() <= hitRate) return true;
		else return false;
	}
	
	public double getHitRate(){
		return hitRate;
	}
	
	public String getStatus(){
		if(status == 0) return "NONE";
		else if(status == 1) return "POISONED";
		else if(status == 2) return "SILENCE";
		else if(status == 4) return "PARALYSIS";
		else if(status == 8) return "BLIND";
		else return "NONE";
	}
	
	public boolean isStatusHit(){
		if(random.nextDouble() <= statusHitRate) return true;
		else return false;
	}
	
	public double getStatusHitRate(){
		return statusHitRate;
	}
	
	public String getTarget(){
		if(isSingle == true) return "single";
		else return "multiple";
	}
	
	public boolean getIsSingle(){
		return this.isSingle;
	}
	
	public boolean getIsNear(){
		return this.isNear;
	}
	
	public String getType(){
		if(isAttack == true) return "ATTACK";
		else return "HEAL";
	}
	
	public String getStatusType(){
		if(isStatusAttack == true) return "ATTACK";
		else if(isStatusHeal == true) return "HEAL";
		else return "NONE";
	}
}