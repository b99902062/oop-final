package rpg.item;

import java.lang.*;
import java.util.LinkedList;
import javax.swing.*;

import rpg.character.*;
import rpg.status.*;
import rpg.game.*;
/**
 * A class that handles the information of a potion
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Potion extends Item{
	
	private static final long serialVersionUID = 1L;
	private int hpValue;
	private int mpValue;
	private int statusValue;
	private boolean isSingle;
	private String soundName;
	private String animationName;
	
	public Potion(int _id, int _amount, int _type, int _price, String _name, String _utility, ImageIcon _image){
		super(_id, _amount, _type, _price, _name, _utility, _image);
	}
	
	public void setProperty(int hp, int mp, int status, boolean _isSingle, String _soundName, String _animationName){
		hpValue = hp;
		mpValue = mp;
		statusValue = status;
		isSingle = _isSingle;
		soundName = _soundName;
		animationName = _animationName;
	}
	
	public boolean isSingle(){
		return isSingle;
	}
	
	public int getHpValue(){
		return hpValue;
	}
	
	public int getMpValue(){
		return mpValue;
	}
	
	public int getStatusValue(){
		return statusValue;
	}
	
	public void doEffect(Role target){
		if(isSingle){
			int hp = (target.getCurHP() + hpValue) < target.getHP() ? (target.getCurHP() + hpValue) : target.getHP();
			target.setCurHP(hp);
			int mp = (target.getCurMP() + mpValue) < target.getMP() ? (target.getCurMP() + mpValue) : target.getMP();
			target.setCurMP(mp);
			target.setStatus(statusValue & target.getStatus());
		}
		else{
			LinkedList<Role> roleList = Game.gsh.getRoles();
			for(int i=0; i<roleList.size(); i++){
				target = roleList.get(i);
				int hp = (target.getCurHP() + hpValue) < target.getHP() ? (target.getCurHP() + hpValue) : target.getHP();
				target.setCurHP(hp);
				int mp = (target.getCurMP() + mpValue) < target.getMP() ? (target.getCurMP() + mpValue) : target.getMP();
				target.setCurMP(mp);
				target.setStatus(statusValue & target.getStatus());
			}
		}
	}
}