package rpg.audio;

import java.io.*;
import javazoom.jl.player.Player;  

/**
 * A class that handle the audio playing
 * @author  Chander, Moe, William
 * @version 1.0
 */  
public class AudioPlayer implements Runnable{
    private String filename;  
    private Player player;
	private boolean loop;
	private boolean isKilled;
  
    // constructor that takes the name of an MP3 file  
    public AudioPlayer(boolean loop, String filename){
        this.filename = filename;
		this.loop = loop;
    }
	
	public void run(){
		if(loop){
			while(!isKilled)
				play();
		}
		else
			play();
	}
  
    public void play(){  
        try{  
            FileInputStream fis     = new FileInputStream(filename);
            BufferedInputStream bis = new BufferedInputStream(fis); 
            player = new Player(bis);
            player.play();
        }  
        catch(Exception e){  
			e.printStackTrace();
        }  
    }  
	
	public void stop(){
		isKilled = true;
		player.close();
	}
}  
