package rpg.draw;

import java.lang.*;
import java.awt.*;
import javax.swing.*;

/**
 * A class that handle a frame and all the features in a GUI
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class GUIhandler extends JFrame{
	private static final long serialVersionUID = 1L;
	
	public GUIhandler(){
		super();
		this.setUndecorated(true);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Rectangle bounds = new Rectangle(screenSize);
		this.setBounds(bounds);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setVisible(true);
	}
	
}