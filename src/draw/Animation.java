package rpg.draw;

import java.lang.*;
import java.awt.*;
import javax.swing.*;
import java.io.*;

import rpg.resource.*;
import rpg.game.*;

/**
 * A class that handles animations
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Animation implements Runnable, Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Image[] frameList;
	private int frameNum;
	private int curFrameIndex;
	private JLabel targetLabel;
	private double scaleRate = 1;
	private boolean isKilled;
	
	public Animation(String type, String animationName, JLabel label){
		if(type.equals("Human"))
			frameList = ResourceHandler.getHumanAnimationFrames(animationName);
		else
			frameList = ResourceHandler.getMonsterAnimationFrames(animationName);
		frameNum = frameList.length;
		targetLabel = label;
	}
	
	public Animation(String type, String animationName, JLabel label, double scaleRate){
		this.scaleRate = scaleRate;
		if(type.equals("Human"))
			frameList = ResourceHandler.getHumanAnimationFrames(animationName);
		else
			frameList = ResourceHandler.getMonsterAnimationFrames(animationName);
		frameNum = frameList.length;
		targetLabel = label;
	}
	
	public void setLabel(JLabel label){
		targetLabel = label;
	}
	
	public void run(){
		while(!isKilled){
			try{
				if(scaleRate != 1)
					targetLabel.setIcon(new ImageIcon(Drawer.getResizedImg(getNextFrame(), (int)(getNextFrame().getWidth(null) * scaleRate), (int)(getNextFrame().getHeight(null) * scaleRate))));
				else
					targetLabel.setIcon(new ImageIcon(getNextFrame()));
				Thread.sleep(300);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public void stop(){
		isKilled = true;
	}
	
	private Image getNextFrame(){
		Image img = frameList[curFrameIndex];
		curFrameIndex = (curFrameIndex + 1) % frameNum;
		return img;
	}

}