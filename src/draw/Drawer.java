package rpg.draw;

import java.lang.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.*;

import rpg.map.*;
import rpg.game.*;

/**
 * A class that draws all screens in the game
 * @author  Chander, Moe, William
 * @version 1.0
 */
public class Drawer{
	public static int rowBlockNum;
	public static int colBlockNum;
	public static final int SMALL_BLOCK_SIZE = 32;
	public static final int LARGE_BLOCK_SIZE = 50;
	public static final int PORTRAIT_BLOCK_SIZE = 96;
	public static final int MAX_PANEL_SIZE = 3000;
	private static double scaleRate = LARGE_BLOCK_SIZE / SMALL_BLOCK_SIZE;
	
	private JPanel panel;
	
	public Drawer(JPanel panel){
	}
	
	public static JPanel drawBackGround(BufferedImage img){
		JPanel panel = new BgPanel(img);
		return panel;
	}
	
	public static void drawImageOnScreen(JPanel panel, BufferedImage img, int x, int y){		
		JLabel label = new JLabel(new ImageIcon(img));
		label.setBounds(x, y, img.getWidth(), img.getHeight());
		panel.add(label);
	}
	
	public static void initialMap(JPanel panel, JLabel[][] block){
		rowBlockNum = Game.gui.getHeight() / LARGE_BLOCK_SIZE;
		colBlockNum = Game.gui.getWidth() / LARGE_BLOCK_SIZE;
		for(int row = 0; row < rowBlockNum; row++){
			block[row] = new JLabel[colBlockNum];
			for(int col = 0; col < colBlockNum; col++){
				block[row][col] = new JLabel();
				block[row][col].setBounds(LARGE_BLOCK_SIZE * col, LARGE_BLOCK_SIZE * row, LARGE_BLOCK_SIZE, LARGE_BLOCK_SIZE);
				panel.add(block[row][col]);
			}
		}
	}
	
	public static int[] drawMap(Map map, JLabel[][] block){
		try{
			int rowLimit = (map.getHeight() * LARGE_BLOCK_SIZE) > Game.gui.getHeight() ? rowBlockNum : map.getHeight();
			int colLimit = (map.getWidth() * LARGE_BLOCK_SIZE) > Game.gui.getWidth() ? colBlockNum : map.getWidth();
			int rowOffset = (rowBlockNum - rowLimit) / 2;
			int colOffset = (colBlockNum - colLimit) / 2;
		
			for(int row = 0; row < rowLimit; row++)
				for(int col = 0; col < colLimit; col++){
					MapElement element = map.getElement(row, col);
					if(element == null)
						continue;
					BufferedImage img = getResizedImg(element.getImage().getImage(), LARGE_BLOCK_SIZE, LARGE_BLOCK_SIZE);
					block[row + rowOffset][col + colOffset].setIcon(new ImageIcon(img));
				}
			int[] offset = {(-1) * rowOffset,(-1) * colOffset};
			return offset;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void drawBigMap(){
	}
	
	public void drawRole(){
	}
	
	public void drawItem(){
	}
	
	public void drawSkillPanel(){
	}
	
	public static BufferedImage getResizedImg(Image src, int width, int height){
		// ResampleOp resampleOp = new ResampleOp (width,height);
		// // resampleOp.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.Normal);
		// return resampleOp.filter(imageToBufferedImage(src), null);
		BufferedImage resizedImg = new BufferedImage(width, height, BufferedImage.TRANSLUCENT);
		Graphics2D g2 = resizedImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2.drawImage(src, 0, 0, width, height, null);
		g2.dispose();
		return resizedImg;
	}
	
	public static BufferedImage deepCopyBI(BufferedImage bi){
		ColorModel cm = bi.getColorModel();
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		WritableRaster raster = bi.copyData(null);
		return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}
	
	public static BufferedImage imageToBufferedImage(Image img){
		BufferedImage bi = new BufferedImage(img.getWidth(null), img.getHeight(null), 
								BufferedImage.TYPE_INT_ARGB);
		Graphics bg = bi.getGraphics();
		bg.drawImage(img, 0, 0, null);
		bg.dispose();
		return bi;
	}
}

class BgPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	
	private Image img;
	
	public BgPanel(String img){
		this(new ImageIcon(img).getImage());
	}

	public BgPanel(Image img){
		this.img = img;
		Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		setSize(size);
		setLayout(null);
	}
	
	public void paintComponent(Graphics g){
		g.drawImage(img, 0, 0, null);
	}	
}